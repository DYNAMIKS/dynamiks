Installation
===============

.. code-block:: bash
   
   pip install dynamiks


Newest development version can be installed by:

.. code-block:: bash

   pip install git+https://gitlab.windenergy.dtu.dk/DYNAMIKS/dynamiks.git


Optional dependencies can be installed by:

.. code-block:: bash

   pip install dynamiks[hawc2]


Developer installation
-----------------------------------

Clone the **dynamiks** repository:

.. code-block:: bash

   git clone https://gitlab.windenergy.dtu.dk/DYNAMIKS/dynamiks.git
   cd dynamiks
   pip install -e .
