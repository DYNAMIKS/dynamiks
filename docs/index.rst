.. Dynamiks documentation master file, created by
   sphinx-quickstart on Wed Feb 14 09:58:17 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dynamiks's documentation!
====================================


Dynamic wind system simulator


Source code repository and issue tracker:
    https://gitlab.windenergy.dtu.dk/DYNAMIKS/dynamiks


Contents
========



.. toctree::
    :maxdepth: 2

    notebooks/Overview
    installation   
    notebooks/Publications
    notebooks/QuickStart

.. toctree::
    :maxdepth: 2
    :caption: Main components

    notebooks/WindTurbines
    notebooks/Site
    notebooks/WindFarmFlowModel

.. toctree::
    :maxdepth: 2
    :caption: Wind farm flow models

    notebooks/DWMFlowSimulation

.. toctree::
    :maxdepth: 2
    :caption: Common

    notebooks/Visualization
    notebooks/Views
    notebooks/StepHandlers
    notebooks/MeanFlowVariations
    notebooks/WindFarmControlSimple
    notebooks/ImplementationDetails

