import os
import nbformat
from nbconvert.preprocessors.execute import ExecutePreprocessor
import pytest
import glob

folder = os.path.abspath(os.path.dirname(__file__) + '/notebooks/')


@pytest.mark.parametrize('f', glob.glob(os.path.join(folder, "*.ipynb")))
def test_notebooks(f):
    print(f)
    try:
        os.environ['DYNAMIKS_VISUALIZE'] = 'short'
        nb = nbformat.read(f, as_version=4)
        ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
        ep.preprocess(nb, {'metadata': {'path': folder}})
    finally:
        del os.environ['DYNAMIKS_VISUALIZE']
