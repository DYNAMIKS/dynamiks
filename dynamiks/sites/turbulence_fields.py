import hipersim
from py_wake.utils.grid_interpolator import GridInterpolator
import xarray as xr
import numpy as np
from pathlib import Path
from numpy import newaxis as na


class TurbulenceField():
    ""


class RandomTurbulence(TurbulenceField):
    def __init__(self, ti, ws, seed=12345, uvw_scaling=[1, .8, .5]):
        self.sigma = ti * ws * np.array(uvw_scaling)
        self.rng = np.random.default_rng(seed)
        self.ti = ti

    def get_turbulence_intensity(self, xyz, U):
        return self.ti

    def initialize(self, flowSimulation):
        pass

    def __call__(self, x, *_, **__):
        if np.any(self.sigma > 0):
            return (self.rng.standard_normal((3,) + np.shape(x)).T * self.sigma).T
        else:
            return np.zeros((3,) + np.shape(x))


class MannTurbulenceField(hipersim.MannTurbulenceField, TurbulenceField):
    def __init__(self, uvw, **kwargs):
        hipersim.MannTurbulenceField.__init__(self, uvw, **kwargs)

        self.x, self.y, self.z = [np.arange(N) * d for N, d in zip(self.Nxyz, self.dxyz)]

        self.time_offset = (0, [0., 0., 0.])  # time, offset_xyz
        self.ti = (np.nan, 0)

    def initialize(self, flowSimulation):
        self.site = flowSimulation.site
        self.time_offset = 0, np.array(flowSimulation.site.turbulence_offset, dtype=float)

    @property
    def transport_speed(self):
        if not hasattr(self, 'site'):
            return None
        return self.site.turbulence_transport_speed

    def get_turbulence_intensity(self, xyz, U):
        if self.transport_speed != self.ti[0]:
            self.ti = (self.transport_speed, self.spectrum_TI(U=self.transport_speed))
        return np.zeros_like(U) + self.ti[1]

    @staticmethod
    def from_netcdf(filename):
        da = xr.load_dataarray(filename)
        return MannTurbulenceField.from_xarray(da)

    @staticmethod
    def from_xarray(dataArray):
        da = dataArray
        return MannTurbulenceField(da.values,
                                   Nxyz=da.shape[1:],
                                   dxyz=[(v[1] - v[0]).item() for v in (da.x, da.y, da.z)],
                                   generator=da.attrs['Generator'],
                                   **{k: da.attrs[k] for k in da.attrs if k not in ['Generator', 'name']}
                                   )

    @staticmethod
    def from_hawc2(filenames, Nxyz, dxyz,
                   alphaepsilon, L, Gamma,  # used for scaling
                   seed=1, HighFreqComp=False, double_xyz=(False, True, True)  # only used to generate name
                   ):
        uvw = np.reshape([np.fromfile(f, np.float32, -1) for f in filenames], (3,) + tuple(Nxyz))
        return MannTurbulenceField(uvw, alphaepsilon=alphaepsilon, L=L, Gamma=Gamma, Nxyz=Nxyz, dxyz=dxyz,
                                   seed=seed, HighFreqComp=HighFreqComp, double_xyz=double_xyz)

    @staticmethod
    def generate(alphaepsilon=1, L=33.6, Gamma=3.9, Nxyz=(8192, 64, 64),
                 dxyz=(1, 1, 1), seed=1, HighFreqComp=0, double_xyz=(False, False, False),
                 n_cpu=1, verbose=0, random_generator=None, cache_spectral_tensor=False):
        mtf = hipersim.MannTurbulenceField.generate(alphaepsilon=alphaepsilon, L=L, Gamma=Gamma,
                                                    Nxyz=Nxyz, dxyz=dxyz, seed=seed, HighFreqComp=HighFreqComp,
                                                    double_xyz=double_xyz, n_cpu=n_cpu, verbose=verbose,
                                                    random_generator=random_generator,
                                                    cache_spectral_tensor=cache_spectral_tensor)
        return MannTurbulenceField.from_xarray(mtf.to_xarray())

    def get_offset(self, time):
        time_last, offset = self.time_offset
        if time > time_last:
            offset[0] += self.transport_speed * (time - time_last)
            self.time_offset = time, offset
        return offset

    def get_axes(self, time):
        offset = self.get_offset(time)
        return [xyz + o for xyz, o in zip([self.x, self.y, self.z], offset)]

    def get_slice(self, values, axis, time):
        values = np.atleast_1d(values)
        ax = self.get_axes(time)[axis]
        x0 = ax[0]
        dx = ax[1] - x0
        xi = int(np.floor((values.min() - x0) / dx))
        Nx = int((values.max() - values.min()) // dx) + 1
        N = len(ax)
        if xi < 0 or xi + Nx > N:
            # repeat box
            assert self.double_xyz[axis] == 0, "Not implement for mirrored axes"
            return np.arange(xi, xi + Nx + 1)  # % N
        else:
            return slice(xi, xi + Nx + 1)

    def get_coordinates(self, ax_slice, axis, time):
        if isinstance(ax_slice, slice):
            ax_slice = np.arange(ax_slice.start or 0, ax_slice.stop or self.Nxyz[axis], ax_slice.step)
        ax = self.get_axes(time)[axis]
        x0 = ax[0]
        dx = ax[1] - x0
        return x0 + dx * ax_slice

    def __call__(self, x, y, z, time):
        offset = self.get_offset(time)
        x = x - offset[0]
        y = y - offset[1]
        z = z - offset[2]
        return np.moveaxis(hipersim.MannTurbulenceField.__call__(self, x, y, z), -1, 0).astype(float)


class NoTurbulenceField(MannTurbulenceField):
    def __init__(self, ti):
        MannTurbulenceField.__init__(self, uvw=np.zeros((3, 0)), alphaepsilon=1, L=1, Gamma=1, Nxyz=(2, 2, 2),
                                     dxyz=(1e6, 1e6, 1e6), double_xyz=(0, 0, 0))
        self.ti = ti

    def __call__(self, x, y, z, time):
        return np.zeros((3,) + np.shape(x))

    def get_turbulence_intensity(self, xyz, U):
        return np.zeros_like(U) + self.ti
