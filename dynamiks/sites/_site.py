from abc import ABC, abstractmethod
import numpy as np
from numpy import newaxis as na
from dynamiks.views import GridSlice3D, Points, Grid, View
from dynamiks.sites.mean_wind import ConstantWindSpeed
from dynamiks.sites.turbulence_fields import TurbulenceField, MannTurbulenceField, NoTurbulenceField


class Site(ABC):
    def __init__(self, ws):
        if isinstance(ws, (int, float)):
            ws = ConstantWindSpeed(ws)
        self.add_mean_windspeed = ws

        self.turbulence_transport_speed = self.add_mean_windspeed.ws
        self.step_handlers = [self.step]
        self.time = 0

    def initialize(self, flowSimulation):
        self.time = 0

    @abstractmethod
    def get_windspeed(self, view, time=None):
        ""

    def step(self, flowSimulation):
        self.time = flowSimulation.time


class TurbulenceFieldSite(Site):
    def __init__(self, ws, turbulenceField, turbulence_offset=[0, 0, 0], turbulence_transport_speed=None):
        """Site with mean wind and turbulence field

        Parameters
        ----------
        ws : int, float or MeanWind-object
            mean wind speed or MeanWind-object, e.g. ConstantWindSpeed
        turbulenceField : TurbulenceField
            E.g. MannTurbulenceField
        turbulence_offset : tuple(x,y,z), optional
            turbulence (x,y,z)-offset. Default is no offset
        turbulence_transport_speed : int, float or None, optional
            Turbulence field advection speed.
            If None (default), the speed is obtained from the mean wind speed
        """
        Site.__init__(self, ws)
        assert isinstance(turbulenceField, TurbulenceField)
        self.turbulence_offset = turbulence_offset
        self.turbulenceField = turbulenceField
        self.turbulence_transport_speed = turbulence_transport_speed or self.turbulence_transport_speed

    def initialize(self, flowSimulation):
        Site.initialize(self, flowSimulation)
        self.turbulenceField.initialize(flowSimulation)

    def get_windspeed(self, view, time=None):
        time = time or self.time
        assert isinstance(view, View)
        xyz = view.XYZ
        if isinstance(view, Points):
            uvw = self.turbulenceField(*xyz, time)
        elif (isinstance(view, GridSlice3D) or view.adaptive) and hasattr(self.turbulenceField, 'get_axes'):
            def slice_repeat(s, N):
                if isinstance(s, np.ndarray):
                    s = np.atleast_1d(s % N)
                return s

            if isinstance(view, GridSlice3D):
                slices = view.slices
            else:

                slices = view.get_slices(*self.turbulenceField.get_axes(time),
                                         mirror_xyz=self.turbulenceField.double_xyz)
            shape = self.turbulenceField.uvw.shape[1:]
            x_slice, y_slice, z_slice = [slice_repeat(s, N) for s, N in zip(slices, shape)]
            xyz = view.XYZ  # XYZ updated in view.get_slices
            s = (3,) + view.shape
            uvw = self.turbulenceField.uvw[:, x_slice][:, :, y_slice][:, :, :, z_slice].reshape(s).copy(order='F')
            uvw = uvw.astype(float)
        elif isinstance(view, Grid):
            if any([v is None for v in view]):
                adap_err = ["", "or set adaptive to True "][hasattr(self.turbulenceField, 'x')]
                err = f'Grid undefined. Please specify the grid explicitly {adap_err}when instantiating {view.__class__.__name__}'
                raise TypeError(err)
            uvw = self.turbulenceField(*xyz, time=time)
        else:  # pragma: no cover
            raise NotImplementedError(view.__class)
        return self.add_mean_windspeed(xyz, uvw, time)

    def get_turbulence_intensity(self, xyz):
        U = self.add_mean_windspeed(xyz, [0, 0, 0], self.time)[0]
        return xyz.x * 0 + self.turbulenceField.get_turbulence_intensity(xyz, U)


class UniformSite(TurbulenceFieldSite):
    def __init__(self, ws, ti):
        """Uniform site without turbulence

        Parameters
        ----------
        ws : int, float or MeanWind-object
            mean wind speed or MeanWind-object, e.g. ConstantWindSpeed
        ti : int or float
            turbulence intensity (used as input to e.g. wake models with ti-dependent expansion factors or boundary conditions)
        """
        TurbulenceFieldSite.__init__(self, ws, NoTurbulenceField(ti=ti), turbulence_offset=[-5e5, -5e5, 0],
                                     turbulence_transport_speed=ws)

    def get_windspeed(self, view, time=None):
        assert isinstance(view, View)
        if time is None:
            time = self.time
        if isinstance(view, Grid) and any([v is None for v in view]):
            err = f'Grid undefined. Please specify the grid explicitly when instantiating {view.__class__.__name__}'
            raise TypeError(err)

        xyz = view.XYZ
        uvw = np.array([*xyz], dtype=float) * 0
        return self.add_mean_windspeed(xyz, uvw, time)


class MetmastSite(TurbulenceFieldSite):
    def __init__(self, ws, turbulenceField,
                 wd_lst, dt, max_wd_step, update_interval,
                 turbulence_offset=[0, 0, 0], turbulence_transport_speed=None):
        TurbulenceFieldSite.__init__(self, ws, turbulenceField, turbulence_offset=turbulence_offset,
                                     turbulence_transport_speed=turbulence_transport_speed)
        self.ws = ws
        self.wd = np.unwrap(wd_lst, period=360)
        self.time_lst = np.arange(len(wd_lst)) * dt
        self.dt = dt
        self.max_wd_step = max_wd_step
        self.update_interval = update_interval
        self.wd_slow = np.mean([self.get_wd_lim(self.wd),  # forward
                               self.get_wd_lim(self.wd[::-1])[::-1]],  # backward
                               axis=0) % 360
        self.wd_small = self.wd - self.wd_slow
        self.last_update = 0
        self.add_mean_windspeed = self.mean_wind

    def initialize(self, flowSimulation):
        TurbulenceFieldSite.initialize(self, flowSimulation)
        flowSimulation._wind_direction = self.wd_slow[0]

    def step(self, flowSimulation):
        TurbulenceFieldSite.step(self, flowSimulation)
        fs = flowSimulation
        if fs.time >= self.last_update + self.update_interval:
            fs.wind_direction = np.interp(fs.time, self.time_lst, self.wd_slow)
            self.last_update = fs.time

    def mean_wind(self, xyz, uvw, time):
        theta = np.deg2rad(np.interp(time, self.time_lst, self.wd_small))
        uvw[0] += np.cos(theta) * self.ws
        uvw[1] -= np.sin(theta) * self.ws
        return uvw

    def get_wd_lim(self, wd_lst):
        max_step = self.max_wd_step * (self.dt / self.update_interval)
        wd_lim = [wd_lst[0]]
        for _wd in wd_lst[1:]:
            step = np.clip(_wd - wd_lim[-1], -max_step, max_step)
            wd_lim.append(wd_lim[-1] + step)
        return wd_lim
