import numpy as np
from py_wake.site.shear import Shear as PyWakeShear
from abc import ABC


class MeanWind(ABC):
    def __init__(self, ws):
        self.ws = ws

    def __call__(self, xyz, uvw, time):
        """Add mean wind to uvw, at position xyz"""


class ConstantWindSpeed(MeanWind):
    def __init__(self, ws, shear=None):
        MeanWind.__init__(self, ws)
        assert isinstance(shear, PyWakeShear) or shear is None, "Shear must be PyWake shear or None"
        self.shear = shear

    def __call__(self, xyz, uvw, time):
        U = self.ws
        if isinstance(self.shear, PyWakeShear):
            class LocalWind():
                coords = {}
            U = self.shear(localWind=LocalWind(), WS_ilk=U, h=np.maximum(xyz[2], 0))[:, 0, 0]
        uvw[0] += U
        return uvw
