import time as ctime
import numpy as np
import os
from dynamiks.wind_turbines._windTurbines import WindTurbines, AeroSectionWindturbine
from numpy import newaxis as na
from dynamiks.views import GridSlice3D, EastNorthView
import atexit
import inspect
from dynamiks.dwm.dwm_flow_simulation import DWMFlowSimulation
from dynamiks.utils.geometry import get_east_north_height
from multiclass_interface import mpi_interface
import warnings


def hawc2gl_to_uvw(xyz_gl, axis=0):
    xyz_gl = np.asarray(xyz_gl)
    return np.array([xyz_gl.take(1, axis), xyz_gl.take(0, axis), -xyz_gl.take(2, axis)])


def uvw_to_hawc2_gl(uvw, axis=0):
    return hawc2gl_to_uvw(uvw, axis)


class HAWC2WindTurbines(WindTurbines, AeroSectionWindturbine):
    def __init__(self, x, y, htc_lst=[], types=0, case_name='', z=0, suppress_output=True, buffer=10000,
                 coupling=None, htc_filename_lst=None):
        if htc_filename_lst:
            warnings.warn("htc_filename_lst argument is deprecated use htc_lst instead", DeprecationWarning)
            htc_lst = htc_filename_lst

        htc_filename_lst = [os.path.basename(getattr(htc, 'filename', htc)) for htc in htc_lst]
        WindTurbines.__init__(self, htc_filename_lst, x, y, z, types)
        self.step_handlers.insert(0, self.step)
        self.time = 0

        from h2lib._h2lib import MultiH2Lib
        from wetb.hawc2.htc_file import HTCFile

        self.h2 = MultiH2Lib(self.N, suppress_output=suppress_output)
        atexit.register(self.h2.close)
        self.dist_wt = self.h2
        self.coupling = coupling

        def make_wt_htc(htc, wt_index):
            if isinstance(htc, str):
                htc = HTCFile(htc)
                basename = os.path.splitext(os.path.basename(htc.filename))[0]
            else:
                basename = os.path.splitext(os.path.basename(htc.filename))[0]
                htc = htc.copy()
                htc.set_name(basename)
            htc.set_name(f'{basename}_wt{wt_index}', case_name.replace(" ", "_"))
            if 'output_at_time' in htc:
                htc.output_at_time.filename = f'{htc.output_at_time.filename[0]}_wt{wt_index}'
            if buffer:
                htc.simulation.log_deltat = buffer
                for output in [v for k, v in htc.contents.items() if k.startswith('output') and k != 'output_at_time']:
                    output.buffer = buffer
            htc.save()
            filename, modelpath = htc.filename, htc.modelpath
            return htc, filename, modelpath

        if mpi_interface.mpi and not mpi_interface.COLLECTIVE_MPI:
            rank = mpi_interface.rank
            if rank in np.unique(self.types, return_index=True)[1].tolist():
                # first of each wt saves htc to create subfolders only from one mpi worker
                make_wt_htc(htc_lst[self.types[rank]], rank)
            if mpi_interface.comm:  # pragma: no cover
                # syncronize
                mpi_interface.comm.bcast(0)

            if self.h2.use_rank[rank]:
                self.htc_lst, htc_path_lst, model_path_lst = make_wt_htc(htc_lst[self.types[rank]], rank)
            else:
                return
        else:
            self.htc_lst, htc_path_lst, model_path_lst = [], [], []
            for i, t in enumerate(self.types):
                htc, filename, modelpath = make_wt_htc(htc_lst[t], i)
                model_path_lst.append(modelpath)
                htc_path_lst.append(filename)
                self.htc_lst.append(htc)

        self.h2.read_input(htc_path_lst, model_path_lst)
        self.sensor_id_lst = [[] for _ in range(self.N)]

        self.add_sensor('aero_power', 'aero power')
        self.add_sensor('aero_thrust', 'aero thrust')

        self.h2.init()
        self.init_rotor_variables()

    def initialize(self, flowSimulation):
        WindTurbines.initialize(self, flowSimulation)
        if self.coupling is None and isinstance(flowSimulation, DWMFlowSimulation):
            self.coupling = FreeWindCoupling()

        self.coupling.initialize(flowSimulation, self)

    def init_rotor_variables(self):
        self._rotor_position = self.h2.get_rotor_position()
        self._diameter = self.h2.get_diameter()

    def add_sensor(self, name, getter=None, setter=None, expose=False, ext_lst=None):
        """add a wind turbine sensor

        Sensor values available using: windTurbines.sensors.<name>

        Parameters
        ----------
        name : str
            Name of sensor. Cannot contain spaces
        getter : str, function
            if str: HAWC2 sensor string, e.g. "constraint bearing1 shaft_rot 2"
            if function: f(wt) -> sensor_value
        setter : int, function or None, optional
            if int, index of the HAWC2 'general variable' sensor to set
            if function, f(wt, value) -> None
        expose : boolean, optional
            if True, the getter/setter is exposed to the wind turbine enabling e.g. wt.yaw as a shorthand for wt.sensors.yaw
        ext_lst : list or None
            List of sensor name extensions in case the sensor returns more than one value,
            e.g. the u,v and w components of the wind. In this case "add_sensor('ws', ext_lst=['u','v','w'])" will add
            the three sensors: ws_u, ws_v, ws_w
        """
        if isinstance(getter, str):
            idx = self.h2.add_sensor(getter)

            def getter(wt):
                return np.array(wt.h2.get_sensor_values(idx))

        if setter != False and isinstance(setter, int):  # noqa # False is int-instance
            variable_sensor_index = setter

            def setter(wt, value):
                wt.h2.set_variable_sensor_value(variable_sensor_index, value)
        WindTurbines.add_sensor(self, name, getter, setter, expose=expose, ext_lst=ext_lst)

    @property
    def rotor_positions_east_north(self):
        xyz = [np.atleast_1d(v) for v in np.transpose(self._rotor_position)]
        xyz = hawc2gl_to_uvw(xyz, axis=0)
        if hasattr(self, "flowSimulation"):
            enh = get_east_north_height(xyz, self.flowSimulation.wind_direction, np.array([0, 0, 0]))
        else:
            enh = xyz
        return enh + self.positions_east_north

    def hub_height(self, idx=slice(None)):
        return -np.atleast_2d(self._rotor_position)[self.idx[idx], 2]

    def diameter(self, idx=slice(None)):
        return np.atleast_1d(self.h2.get_diameter())[idx]

    def step(self, flowSimulation):
        self.coupling.step(flowSimulation)
        try:
            self.time = np.atleast_1d(self.h2.run(flowSimulation.time))[0]
            self._rotor_position = self.h2.get_rotor_position()
        except ChildProcessError:
            pass

    def __getitem__(self, idx):
        class WindTurbine():
            def __init__(self, windTurbines, idx):
                self.windTurbines = windTurbines
                if isinstance(idx, (int, np.int_)):
                    idx = [idx]
                self.idx = idx

            @property
            def rotor_positions_east_north(self):
                return self.windTurbines.rotor_positions_east_north[:, idx]

            def __getattr__(self, name):
                attr = getattr(self.windTurbines, name)
                if isinstance(attr, (np.ndarray)):
                    return attr[self.idx[0]]
                argspec = inspect.getfullargspec(attr)
                if ((inspect.ismethod(attr) or inspect.isfunction(attr)) and
                        'idx' in argspec.args + argspec.kwonlyargs):
                    def wrap(*args, **kwargs):
                        return getattr(self.windTurbines, name)(*args, idx=self.idx, **kwargs)
                    return wrap
        return WindTurbine(self, idx)

    def get_rotor_avg_windspeed(self, include_wakes, idx=slice(None)):
        if include_wakes and self.time > 0:
            return np.array(self.h2[idx].get_rotor_avg_uvw()).T
        return WindTurbines.get_rotor_avg_windspeed(self, include_wakes, idx=idx)

    def ct(self, idx=slice(None), **_):
        # Tn = 1/2 rho ctn A (U cos theta)^2
        # ctx = ctn (cos theta)^2 = 2 Tn / (rho A U^2)
        T = self.sensors.aero_thrust[idx, 0] * 1000
        A = np.pi * (self.diameter(idx=idx) / 2)**2
        U = self.rotor_avg_windspeed[idx, 0]
        ct_x = 2 * T / (1.225 * A * U**2)
        return ct_x

    def power(self, include_wakes=True, idx=slice(None)):
        assert include_wakes, "HAWC2WindTurbines cannot calculate power without wakes"
        power = self.sensors.aero_power
        if mpi_interface.main:
            return power[idx, 0]
        else:  # pragma: no cover
            return 0

    def axisymetric_induction(self, r, idx=slice(None)):
        R_lst = np.array(self.diameter(idx)) / 2
        h2_r = self.h2[idx].get_bem_grid()[0][1]
        induc_lst = self.h2[idx].get_induction_axisymmetric()
        if h2_r[-1] == 0:
            return np.zeros((len(r), len(induc_lst)))
        for induc in induc_lst:
            induc[-1] = 0  # From HAWC2Farm, don't know why?
        return np.array([np.interp(r, h2_r / R, induc) for R, induc in zip(R_lst, induc_lst)]).T

    def rotor_avg_induction(self, idx=slice(None)):
        return np.array(self.h2[idx].get_induction_rotoravg())

    def yaw_tilt(self, idx=slice(None)):
        # hawc2 positive yaw is opposite (clockwise) while positive in Dynamiks is counter-clockwise
        return (np.array(self.h2[idx].get_rotor_orientation(deg=True))[:, :2] * [-1, 1]).T

    def get_windspeed(self, x, y, z):
        wt_x, wt_y, wt_z = self.positions_xyz
        return np.array(self.h2.get_uvw(uvw_to_hawc2_gl([x - wt_x, y - wt_y, z - wt_z]).T.tolist())).T

    def get_aerosection_positions(self):
        """
        Returns
        -------
        rotor_pos_x : array_like
            xyz position of rotor in dynamikcs coordinates, shape=(3, nwt)
        sec_rel_pos_xsb : array_like
            positions of aerodynamic sections relative to rotor position in dynamiks coordinates, shape=(3, nwt, nblades, nsections)

        """

        sec_pos_xibs = hawc2gl_to_uvw(self.h2.get_aerosections_position(), -1)
        rotor_pos_xi = hawc2gl_to_uvw(self.h2.get_rotor_position(), -1)

        # blade positions relative to rotor center
        sec_rel_pos_xibs = sec_pos_xibs - rotor_pos_xi[:, :, na, na]
        if mpi_interface.mpi and not mpi_interface.COLLECTIVE_MPI:  # pragma: no cover
            rotor_pos_xi += self.positions_xyz[:, [mpi_interface.rank]]
        else:
            rotor_pos_xi += self.positions_xyz
        return rotor_pos_xi, sec_rel_pos_xibs

    def get_aerosection_forces(self):
        """
        Returns
        -------
        f_xisb : array_like
            xyz forces at aerodynamic sections in dynamics coordinates, shape=(3, nwt, nblades, nsections)
        """
        f_xibs = hawc2gl_to_uvw(self.h2.get_aerosections_forces(), -1)
        return f_xibs

    def set_aerosection_velocities(self, uvw_uibs):
        """Set velocity at aerodynamic sections

        Parameters
        ----------
        uvw_usb : array_like
            uvw velocity at aerodynamic sections, shape=(3, nwt, nsections, nblades)
        """
        uvw_ibsu = np.moveaxis(uvw_uibs, [0], [-1])
        self.h2.set_aerosections_windspeed([np.asfortranarray(uvw_bsu) for uvw_bsu in uvw_ibsu])

    def plot_windTurbines(self, flowSimulation, view, ax):
        fs = flowSimulation
        if mpi_interface.mpi and not mpi_interface.COLLECTIVE_MPI:  # pragma: no cover
            try:
                rotor_pos_xi, sec_rel_pos_xibs = self.get_aerosection_positions()
                rotor_pos_xi, sec_rel_pos_xibs = rotor_pos_xi[:, 0], sec_rel_pos_xibs[:, 0]
            except ChildProcessError:
                rotor_pos_xi, sec_rel_pos_xibs = None, None
            rotor_pos_xi = mpi_interface.comm.gather(rotor_pos_xi, root=0)
            sec_rel_pos_xibs = mpi_interface.comm.gather(sec_rel_pos_xibs, root=0)
            if mpi_interface.main:
                rotor_pos_xi, sec_rel_pos_xibs = np.moveaxis(
                    rotor_pos_xi[:self.N], 0, 1), np.moveaxis(sec_rel_pos_xibs[:self.N], 0, 1)
        else:
            rotor_pos_xi, sec_rel_pos_xibs = self.get_aerosection_positions()

        if mpi_interface.main:
            sec_pos_xibs = rotor_pos_xi[:, :, na, na] + sec_rel_pos_xibs
            if isinstance(view, EastNorthView):
                pos_xi = self.positions_east_north
                rotor_pos_xi = get_east_north_height(rotor_pos_xi, fs.wind_direction, fs.center_offset)
                sec_pos_xibs = get_east_north_height(sec_pos_xibs, fs.wind_direction, fs.center_offset)
                xi, yi = 0, 1
            else:
                xi, yi = ['xyz'.index(v) for v in view.plane]
                pos_xi = self.positions_xyz

            tilt_i = np.deg2rad(self.yaw_tilt()[1])
            # horizontal distance from tower bottom to rotor center
            s = np.sqrt(np.sum((rotor_pos_xi[:2] - pos_xi[:2])**2, 0))
            tower_top = np.array([pos_xi[0], pos_xi[1], rotor_pos_xi[2] - s * np.tan(tilt_i)])

            for i in range(self.N):
                ax.plot([pos_xi[xi, i], tower_top[xi, i], rotor_pos_xi[xi, i]],
                        [pos_xi[yi, i], tower_top[yi, i], rotor_pos_xi[yi, i]], 'k')
                for b in range(sec_rel_pos_xibs.shape[2]):
                    ax.plot(sec_pos_xibs[xi, i, b],
                            sec_pos_xibs[yi, i, b], 'k')


class FreeWindCoupling():
    def __init__(self, windfield_update_interval=5):
        self.windfield_update_interval = windfield_update_interval

    def initialize(self, flowSimulation, windTurbine):
        self.windTurbine = windTurbine
        self.flowSimulation = flowSimulation
        self.init_windfield(flowSimulation)
        self.windTurbine.add_sensor('wind_speed', lambda wt: np.array(wt.h2.get_uvw(wt.h2.get_rotor_position())),
                                    ext_lst=['u', 'v', 'w'])

    def init_windfield(self, flowSimulation):
        update_interval = self.windfield_update_interval
        site = flowSimulation.site
        tf = site.turbulenceField
        Xs, Ys, Zs = tf.get_axes(0)
        dxyz = np.diff([Xs[:2], Ys[:2], Zs[:2]])
        D = self.windTurbine.diameter()
        # buffer size should be larger than rotor due to deflections and non-aligned grids
        Lx, Ly, Lz = (1.05 * D[na] + dxyz)
        Lz[:] = Zs[-1] - Zs[0]  # full box
        self.x_offset = update_interval * site.turbulence_transport_speed + Lx / 2
        Lx = update_interval * site.turbulence_transport_speed + Lx
        self.Lxyz = Lx, Ly, Lz
        self.Nxyz = Nxyz = np.ceil(np.round([Lx, Ly, Lz] / dxyz + 1, 6)).astype(int)
        # t = self.windTurbine.time

        grid_lst, h2_box_offset_xyz = self.get_grid(site)
        self.windTurbine.h2.init_windfield(Nxyz=Nxyz.T.tolist(), dxyz=dxyz[:, 0],
                                           box_offset_yz=np.transpose(h2_box_offset_xyz[1:]).tolist(),
                                           transport_speed=site.turbulence_transport_speed)

        uvw_lst = [np.asfortranarray(site.get_windspeed(grid)) for grid in grid_lst]
        self.windTurbine.h2.set_windfield(uvw_lst, box_offset_x=h2_box_offset_xyz[0])
        self.last_windfield_update = - update_interval
        self.current_wind_direction = flowSimulation.wind_direction
        self.current_transport_speed = site.turbulence_transport_speed

    def get_grid(self, site):
        tf = site.turbulenceField
        t = self.windTurbine.time
        x, y, z = xyz = self.windTurbine.positions_xyz
        Lx, Ly, Lz = self.Lxyz
        self.x_slice_lst = [tf.get_slice([start, start + L], 0, t) for start, L in zip(x - self.x_offset, Lx)]
        self.y_slice_lst = [tf.get_slice([start, start + L], 1, t) for start, L in zip(y - Ly / 2, Ly)]
        self.z_slice_lst = [slice(None)] * self.windTurbine.N
        axes = tf.get_axes(t)
        grid_lst = [GridSlice3D(x_s, y_s, z_s, axes)
                    for x_s, y_s, z_s in zip(self.x_slice_lst, self.y_slice_lst, self.z_slice_lst)]
        h2_box_offset_xyz = [[tf.get_coordinates(sl, i, t)[0] - p for sl, p in zip(slice_lst, pos)]
                             for i, (slice_lst, pos) in enumerate(zip([self.x_slice_lst, self.y_slice_lst, self.z_slice_lst],
                                                                      self.windTurbine.positions_xyz))]
        return grid_lst, h2_box_offset_xyz

    def step(self, flowSimulation):
        # import time
        # t = time.time()
        if (flowSimulation.wind_direction != self.current_wind_direction or
                flowSimulation.site.turbulence_transport_speed != self.current_transport_speed):
            self.init_windfield(flowSimulation)
        elif flowSimulation.time >= self.last_windfield_update + self.windfield_update_interval:
            site = flowSimulation.site
            # print('set new wind field')
            grid_lst, h2_box_offset_xyz_lst = self.get_grid(site)

            uvw_lst = [np.asfortranarray(flowSimulation.get_windspeed(grid, include_wakes=True, exclude_wake_from=[i]))
                       for i, grid in enumerate(grid_lst)]
            self.windTurbine.h2.set_windfield(uvw_lst, box_offset_x=h2_box_offset_xyz_lst[0])
            self.last_windfield_update = flowSimulation.time
        # print('set windfield', flowSimulation.time, time.time() - t)


class AeroSectionCoupling():

    def __init__(self, general_variable_uvw=[1, 2, 3]):
        """
        Parameters
        ----------
        general_variable_uvw : list, optional
            Indexes of the HAWC2 general variable sensors that will be updated with the rotor center uvw wind speed
            These wind speeds are typically needed by the controller.
        """
        self.general_variable_uvw = general_variable_uvw

    def initialize(self, flowSimulation, windTurbines):
        self.windTurbines = windTurbines
        self.h2 = self.windTurbines.h2
        self.h2_time = 0
        try:
            for i, uvw in zip(self.general_variable_uvw, 'uvw'):
                self.windTurbines.add_sensor(f'wind_speed_{uvw}', f'general variable {i} 0')
        except ChildProcessError:  # pragma: no cover
            pass

    def step(self, flowSimulation):
        wts = self.windTurbines

        while self.h2_time < flowSimulation.time:
            # Get wind speed from flowSimulation and pass to HAWC2.
            # Note, the distributed behaviuor:
            # Each mpi worker (rank<=n_wt) request only the wind speed at the aerosection positions of their own turbine.
            # I.e. there is no collective gathering of positions or wind speeds by rank 0.
            try:
                rotor_pos_xi, sec_rel_pos_xibs = wts.get_aerosection_positions()
                sec_pos_xibs = sec_rel_pos_xibs + rotor_pos_xi[:, :, na, na]
            except ChildProcessError:  # pragma: no cover
                rotor_pos_xi = sec_pos_xibs = np.zeros((3, 0))

            uvw_un = flowSimulation.get_windspeed(sec_pos_xibs.reshape((3, -1)), time=self.h2_time)
            uvw_hub_ui = flowSimulation.get_windspeed(rotor_pos_xi, time=self.h2_time)

            if sec_pos_xibs.shape[1]:
                uvw_uibs = uvw_un.reshape(sec_pos_xibs.shape)
                wts.set_aerosection_velocities(uvw_uibs)

                # print (h2_time, sec_pos_xibs[:,0,0,35], uvw_uibs[0,0,0,35])

                # set freestream hub velocity
                for i, uvw in enumerate(uvw_hub_ui, 1):
                    self.h2.set_variable_sensor_value(i, uvw.tolist())

            try:
                self.h2.step()
                self.h2_time = np.atleast_1d(self.h2.get_time())[0]
            except ChildProcessError:   # pragma: no cover
                pass

            if mpi_interface.mpi and not mpi_interface.COLLECTIVE_MPI:  # pragma: no cover
                # barrier to ensure all HAWC2 instances are finish
                self.h2_time = mpi_interface.comm.bcast(self.h2_time, root=0)


def _get_dummy_diameter(*_, **__):  # pragma: no cover  # run in separate process
    return 178


def _get_dummy_rotor_position(*_, **__):  # pragma: no cover  # run in separate process
    return [0, 0, -119]


def _get_dummy_rotor_orientation(*_, **__):  # pragma: no cover  # run in separate process
    return [0, 0, 0]


class HAWC2WindTurbinesDummy(HAWC2WindTurbines):
    """Dummy wind turbine with same Dynamiks interface and behaviour as HAWC2WindTurbines,
    but without HAWC2. Usefull to profiling the Dynamiks side of the HAWC2WindTurbines interface"""

    def __init__(self, x, y, htc_lst, types=0,
                 case_name='', suppress_output=True):
        from h2lib_tests import tfp as h2_tfp
        htc_lst = [h2_tfp + 'minimal/htc/minimal_nooutput.htc']
        HAWC2WindTurbines.__init__(self, x, y, htc_lst, types=0, case_name=case_name,
                                   suppress_output=suppress_output)
        from dynamiks.wind_turbines.axisymetric_induction import InductionMatch
        self.inductionModel = InductionMatch()

    def init_rotor_variables(self):
        self.h2.get_diameter = _get_dummy_diameter
        self.h2.get_rotor_position = _get_dummy_rotor_position
        self.h2.get_rotor_orientation = _get_dummy_rotor_orientation
        HAWC2WindTurbines.init_rotor_variables(self)

    def ct(self, idx=slice(None), **_):
        return np.full(self.N, 8 / 9)[idx]

    def get_rotor_avg_windspeed(self, include_wakes, idx=slice(None)):
        return WindTurbines.get_rotor_avg_windspeed(self, include_wakes, idx=idx)

    def axisymetric_induction(self, r, idx=slice(None)):
        tsr = 7
        from py_wake.deficit_models.utils import ct2a_madsen
        a_target_lst = ct2a_madsen(self.ct(idx=idx))
        return np.array([self.inductionModel(tsr=tsr, a_target=a_target, r=r)[1] for a_target in a_target_lst]).T

    def rotor_avg_induction(self, idx=slice(None)):
        from py_wake.deficit_models.utils import ct2a_madsen
        return ct2a_madsen(self.ct(idx=idx))

    def get_aerosection_positions(self):
        rotor_pos_xi = self.rotor_positions_xyz
        rotor_pos_xi[0] -= 8

        sec_rel_pos_xibs = np.zeros((3, self.N, 3, 2))

        for b in range(3):
            a = np.deg2rad(120 * b + 90)
            sec_rel_pos_xibs[[1, 2], :, b, 1] = np.array([np.cos(a) * 90, np.sin(a) * 90])[:, na]
        sec_rel_pos_xibs[0] = np.array([0, -8])[na, na]
        return rotor_pos_xi, sec_rel_pos_xibs
