from abc import ABC, abstractmethod
import numpy as np
from dynamiks.utils.geometry import get_xyz
from dynamiks.utils.data_dumper import DataDumper
from numpy import newaxis as na
from multiclass_interface import mpi_interface
from py_wake.rotor_avg_models.rotor_avg_model import CGIRotorAvg


class Sensors(DataDumper):
    windTurbines = None

    def __init__(self, windTurbines):
        self.dict = {}
        self.windTurbines = windTurbines
        self.sensor_name_lst = []
        DataDumper.__init__(self, data_dumper_function=self.get_all_sensor_values,
                            coords={'wt': np.arange(self.windTurbines.N)})

    def add_sensor(self, name, getter=None, setter=None, ext_lst=None):
        if name in self.dict:
            raise ValueError(f"'{name}' is already a sensor name. Please use another name")
        if ' ' in name:
            raise ValueError(f"Space not allowed in sensor name ('{name}')")
        if mpi_interface.main or mpi_interface.rank < self.windTurbines.N:
            if ext_lst:
                dims = (self.windTurbines.N, len(ext_lst))

                self.sensor_name_lst.append([f'{name}_{c}' for c in ext_lst])
            else:
                dims = self.windTurbines.N
                self.sensor_name_lst.append([name])

            if getter is None:
                object.__setattr__(self, f'_{name}', np.zeros(dims))

                def getter(wt):
                    return getattr(wt.sensors, f'_{name}')

                if setter is None:
                    def setter(wt, value):
                        getter(wt)[:] = value

            self.dict[name] = (getter, setter)

    def __getattr__(self, name):
        if mpi_interface.mpi and not mpi_interface.COLLECTIVE_MPI:  # pragma: no cover
            try:
                f = self.dict[name][0]
            except BaseException:
                if mpi_interface.rank < self.windTurbines.N:
                    raise

                def f(_):
                    return [None]

            res = np.atleast_2d(f(self.windTurbines))
            res = mpi_interface.comm.gather(res, root=0)
            if mpi_interface.rank == 0:
                res = np.array([[v for wt_sensor in wt_sensors for v in wt_sensor]
                               for wt_sensors in res[:self.windTurbines.N]])
            return res
        elif mpi_interface.rank < self.windTurbines.N:
            return self.dict[name][0](self.windTurbines)

    def __setattr__(self, name, value):
        if name in ['dict', 'windTurbines', 'data_dumper_function', 'sensor_name_lst',
                    'time_step_interval', 'coords', 'data', 'time', 'time_step']:
            return object.__setattr__(self, name, value)
        if mpi_interface.rank < self.windTurbines.N:
            setter = self.dict[name][1]
            setter(self.windTurbines, value)

    def get_all_sensor_values(self, flowSimulation):
        if mpi_interface.mpi and not mpi_interface.COLLECTIVE_MPI:  # pragma: no cover
            res = [v for g, _ in self.dict.values() for v in g(self.windTurbines)]
            res = mpi_interface.comm.gather(res, root=0)
            if mpi_interface.rank == 0:
                res = np.array([[v for wt_sensor in wt_sensors for v in np.atleast_2d(wt_sensor)]
                               for wt_sensors in res[:self.windTurbines.N]])
                if 'sensor' not in self.coords:
                    # res = [np.atleast_2d(np.transpose(g(self.windTurbines))) for g, _ in self.dict.values()]
                    # sensor_name_lst = [[s, [f'{s[0]}_{i}' for i in range(n)]][np.shape(s)[0] != n]
                    #                    for s, n in zip(self.sensor_name_lst, [r.shape[0] for r in res])]
                    self.coords['sensor'] = [s for s in self.sensor_name_lst for s in s]

            return res
        else:

            if 'sensor' not in self.coords:
                res = [np.atleast_2d(np.transpose(g(self.windTurbines))) for g, _ in self.dict.values()]
                sensor_name_lst = [[s, [f'{s[0]}_{i}' for i in range(n)]][np.shape(s)[0] != n]
                                   for s, n in zip(self.sensor_name_lst, [r.shape[0] for r in res])]
                self.coords['sensor'] = [s for s in sensor_name_lst for s in s]
                return np.concatenate(res).T
            return np.array([v for g, _ in self.dict.values()
                            for v in np.atleast_2d(np.transpose(g(self.windTurbines)))]).T

    def to_xarray(self, dataset=False):
        da = DataDumper.to_xarray(self)
        if dataset:
            return da.to_dataset('sensor')
        return da


class WindTurbines(ABC):
    def __init__(self, names, x, y, z=0, types=0, rotorAvgModel=CGIRotorAvg(7)):
        assert len(x) == len(y)
        self.N = len(x)
        z = np.zeros_like(x) + z
        self.positions_east_north = np.array([x, y, z], dtype=float)
        self.types = types = np.zeros(len(x), dtype=int) + types
        self.rotorAvgModel = rotorAvgModel
        self.idx = np.arange(self.N)
        self._names = names
        self.sensors = Sensors(self)
        self.step_handlers = [self.sensors]

    def initialize(self, flowSimulation):
        self.flowSimulation = flowSimulation

    @abstractmethod
    def diameter(self, idx=slice(None)):
        ""

    @abstractmethod
    def hub_height(self, idx=slice(None)):
        ""

    @property
    def positions_xyz(self):
        return get_xyz(self.positions_east_north, self.flowSimulation.wind_direction,
                       self.flowSimulation.center_offset)

    @property
    def rotor_positions_xyz(self):
        return get_xyz(self.rotor_positions_east_north, self.flowSimulation.wind_direction,
                       self.flowSimulation.center_offset)

    def get_rotor_avg_windspeed(self, include_wakes, idx=slice(None)):

        if not isinstance(idx, (int, np.integer)):
            return np.array([WindTurbines.get_rotor_avg_windspeed(self, include_wakes, i)
                            for i in np.atleast_1d(self.idx)[idx]]).T
        # only one wt as we need to exclude self induction
        rp_x = self.rotor_positions_xyz[:, idx]
        ram = self.rotorAvgModel
        if ram:
            y, z, w = ram.nodes_x, ram.nodes_y, ram.nodes_weight
            nodes_position_xn = rp_x[:, na] + np.array([y * 0, y, z])[na, :] * self.diameter(idx) / 2
            uvw_un = self.flowSimulation.get_windspeed(*nodes_position_xn, include_wakes=include_wakes,
                                                       exclude_wake_from=[idx])
            if w is None:
                return np.mean(uvw_un, -1)
            return np.sum(w * uvw_un, -1)
        else:
            return self.flowSimulation.get_windspeed(rp_x, include_wakes=include_wakes, exclude_wake_from=[idx])[:, 0]

    def rotor_avg_ti(self, idx=slice(None)):
        # TODO: Change to effective TI instead of ambient???
        if not isinstance(idx, (int, np.integer)):
            return np.array([self.rotor_avg_ti(i)
                            for i in np.atleast_1d(self.idx)[idx]]).T

        rp = self.rotor_positions_xyz[:, idx]
        # if len(np.shape(rp)) == 2:
        #     rp = rp[:, list(np.atleast_1d(self.idx)).index(idx)]
        return self.flowSimulation.get_turbulence_intensity(rp, False)[0]

    @abstractmethod
    def ct(self, idx=slice(None)):
        ""

    @abstractmethod
    def power(self, include_wakes=True, idx=slice(None),):
        ""

    @abstractmethod
    def rotor_avg_induction(self, idx=slice(None)):
        ""

    @abstractmethod
    def axisymetric_induction(self, idx=slice(None)):
        ""

    @abstractmethod
    def yaw_tilt(self):
        ""

    def add_sensor(self, name, getter=None, setter=None, expose=False, ext_lst=None):
        """add a wind turbine sensor

        Sensor values available using: windTurbines.sensors.<name>

        Parameters
        ----------
        name : str
            Name of sensor. Cannot contain spaces
        getter : function
            function, f(wt) -> sensor_value
        setter : function, optional
            function, f(wt, value) -> None,
        expose : boolean, optional
            if True, the getter/setter is exposed to the wind turbine enabling e.g. wt.yaw as a shorthand for wt.sensors.yaw
        ext_lst : list or None
            List of sensor name extensions in case the sensor returns more than one value,
            e.g. the u,v and w components of the wind, see examples
        Examples
        --------
        windTurbines.add_sensor('power', lambda wt: wt.power())
        windTurbines.add_sensor('ws', ext_lst=['u','v','w'])" # adds the three sensors: ws_u, ws_v, ws_w
        """
        self.sensors.add_sensor(name, getter, setter, ext_lst)
        if expose:
            setattr(self.__class__, name, property(lambda self, name=name: self.sensors.dict[name][0](self),
                                                   lambda self, value, name=name: self.sensors.dict[name][1](self, value)))

    @abstractmethod
    def plot_windTurbines(self, view):
        ""


class AeroSectionWindturbine(ABC):

    @abstractmethod
    def get_aerosection_positions(self):
        """
        Returns
        -------
        rotor_pos_x : array_like
            xyz position of rotor in dynamikcs coordinates, shape=(3,)
        sec_rel_pos_xsb : array_like
            positions of aerodynamic sections relative to rotor position in dynamiks coordinates, shape=(3, nblades, nsections)

        """

    @abstractmethod
    def get_aerosection_forces(self):
        """
        Returns
        -------
        f_xsb : array_like
            xyz forces at aerodynamic sections in dynamics coordinates, shape=(3, nblades, nsections)
        """

    @abstractmethod
    def set_aerosection_velocities(self, uvw_ubs):
        """Set velocity at aerodynamic sections

        Parameters
        ----------
        uvw_usb : array_like
            uvw velocity at aerodynamic sections, shape=(3,nsections, nblades)
        """
