import atexit
import inspect

from numpy import newaxis as na

from dynamiks.views import EastNorthView
from dynamiks.wind_turbines._windTurbines import WindTurbines
from dynamiks.wind_turbines.axisymetric_induction import InductionMatch
import matplotlib.pyplot as plt
from multiclass_interface.multiprocess_interface import MultiProcessClassInterface
import numpy as np
from py_wake.deficit_models.utils import ct2a_madsen
from py_wake.wind_turbines._wind_turbines import WindTurbines as WindTurbinesPW
from py_wake.rotor_avg_models.rotor_avg_model import CGIRotorAvg


class PyWakeWindTurbines(WindTurbines):
    def __init__(self, x, y, windTurbine, types=0, z=0, rotorAvgModel=CGIRotorAvg(7), inductionModel=None):
        if isinstance(windTurbine, (list, tuple)):
            windTurbine = WindTurbinesPW.from_WindTurbine_lst(windTurbine)

        WindTurbines.__init__(self, windTurbine._names, x, y, z, types, rotorAvgModel)
        assert isinstance(windTurbine, WindTurbinesPW)

        self.rotor_positions_east_north = self.positions_east_north.copy()
        self.rotor_positions_east_north[2] += windTurbine.hub_height(types)

        self.inductionModel = inductionModel or InductionMatch()
        self.windTurbine = windTurbine

        self.add_sensor('power', lambda wt: wt.power())
        self.add_sensor('ct', lambda wt: wt.ct())
        self.add_sensor('yaw', expose=True)
        self.add_sensor('tilt', expose=True)

    def __getitem__(self, idx):
        class WindTurbine():
            def __init__(self, windTurbines, idx):
                self.windTurbines = windTurbines
                self.idx = idx

            @property
            def rotor_positions_east_north(self):
                return self.windTurbines.rotor_positions_east_north[:, idx]

            def __getattr__(self, name):
                attr = getattr(self.windTurbines, name)
                if isinstance(attr, np.ndarray) and attr.shape[0] == self.N:
                    return attr[self.idx]
                if inspect.ismethod(attr) and 'idx' in inspect.getfullargspec(attr).args:
                    def wrap(*args, **kwargs):
                        res = getattr(self.windTurbines, name)(*args, idx=self.idx, **kwargs)
                        if isinstance(self.idx, int):
                            res = res[..., na]
                        return res
                    return wrap
                else:
                    return attr
        return WindTurbine(self, idx)

    def diameter(self, idx=slice(None)):
        return self.windTurbine.diameter(self.types[idx])

    def hub_height(self, idx=slice(None)):
        return self.windTurbine.hub_height(self.types[idx])

    def get_kwargs(self, idx=slice(None)):
        options = {'type': self.types}
        r, o = self.windTurbine.powerCtFunction.required_inputs, self.windTurbine.powerCtFunction.optional_inputs
        options.update({k: getattr(self.sensors, k) for k in r if k not in options})
        options.update({k: getattr(self.sensors, k) for k in o if k in self.sensors.dict and k not in options})

        return {k: options[k][idx] for k in set(r) | (set(o) & set(options))}

    def ct(self, idx=slice(None)):
        kwargs = self.get_kwargs(idx)

        ws = self.rotor_avg_windspeed[idx, 0]
        return self.windTurbine.ct(ws, **kwargs)

    def power(self, include_wakes=True, idx=slice(None)):
        kwargs = self.get_kwargs(idx)
        if include_wakes:
            ws = self.rotor_avg_windspeed[idx, 0]
        else:
            ws = self.rotor_avg_freestream[idx, 0]
        return self.windTurbine.power(ws, **kwargs)

    def rotor_avg_induction(self, idx=slice(None)):
        return ct2a_madsen(self.ct(idx=idx))

    def axisymetric_induction(self, r, idx=slice(None)):
        tsr = 7
        a_target_lst = self.rotor_avg_induction(idx)
        a = np.moveaxis([self.inductionModel(tsr=tsr, a_target=a_target, r=r)[1]
                         for a_target in np.atleast_1d(a_target_lst)], 0, -1)
        if isinstance(idx, int):
            a = a[..., 0]
        return a

    def yaw_tilt(self, idx=slice(None)):
        return np.array([self.sensors.yaw[idx], self.sensors.tilt[idx]])

    def plot_windTurbines(self, flowSimulation, view, ax):
        yaw, tilt = self.yaw_tilt()

        if isinstance(view, EastNorthView):
            x, y = self.positions_east_north[:2]
            f, wd = WindTurbinesPW.plot_xy, flowSimulation.wind_direction
        else:
            xi, yi = ['xyz'.index(v) for v in view.plane]
            x, y = self.positions_xyz[[xi, yi]]

            f, wd = {'xy': (WindTurbinesPW.plot_xy, 270),
                     'xz': (WindTurbinesPW.plot_yz, 180),
                     'yz': (WindTurbinesPW.plot_yz, 270)}[view.plane]
        f(self, x, y, types=self.types, wd=wd, yaw=yaw, tilt=tilt, ax=ax)

    @property
    def dist_wt(self):
        if not hasattr(self, '_dist_wt'):
            x, y, _ = self.positions_east_north
            self._dist_wt = MultiProcessClassInterface(DummyWT, [(x, y) for x, y in zip(x, y)])
            atexit.register(self._dist_wt.close)
        return self._dist_wt

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dist_wt.close()


class DummyWT():
    def __init__(self, x, y):
        self.x = x
        self.y = y
