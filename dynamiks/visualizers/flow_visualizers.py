import matplotlib.pyplot as plt
import numpy as np
from dynamiks.visualizers._visualizers import Visualizer
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from multiclass_interface import mpi_interface


class Flow():
    def __init__(self, uvw='u', include_wakes=True):
        if len(uvw) == 1:
            self.comp = 'uvw'.index(uvw)
        else:
            self.comp = ['uvw'.index(uvw) for uvw in uvw]
        self.include_wakes = include_wakes

    def get_flow(self, flowSimulation, view):
        flow = flowSimulation.get_windspeed(view, include_wakes=self.include_wakes)[self.comp]
        if isinstance(self.comp, int):
            return flow
        else:
            return np.sqrt(np.sum(flow**2, 0))

    def __call__(self, flowSimulation, view):
        return self.get_flow(flowSimulation, view)


class Flow2DVisualizer(Visualizer, Flow):
    def __init__(self, uvw='u', include_wakes=True, levels=55, color_bar=True, cmap='viridis'):
        Flow.__init__(self, uvw, include_wakes)
        self.levels = levels
        self.color_bar = color_bar
        self.value_lim = [1e100, -1e100]
        self.cmap = cmap

    def __call__(self, flowSimulation):
        flow = self.get_flow(flowSimulation, self.view).T
        if mpi_interface.main:
            if isinstance(self.levels, int):
                self.value_lim = [np.floor(np.minimum(self.value_lim[0], flow.min())),
                                  np.ceil(np.maximum(self.value_lim[1], flow.max()))]
                levels = np.linspace(self.value_lim[0],
                                     np.maximum(self.value_lim[1], self.value_lim[0] + 1),
                                     self.levels)
            else:
                levels = self.levels

            X, Y = self.view.XY(flowSimulation.wind_direction, flowSimulation.center_offset)
            c = self.ax.contourf(X, Y, flow, levels=levels, cmap=self.cmap)
            if self.color_bar:
                if hasattr(self, 'cax'):
                    self.cax.clear()
                else:
                    self.cax = make_axes_locatable(self.ax).append_axes("right", size="5%", pad="5%")
                self.ax.cb = self.ax.figure.colorbar(c, cax=self.cax)


class Flow1DVisualizer(Flow2DVisualizer):
    def __init__(self, uvw='u', include_wakes=True):
        Flow.__init__(self, uvw, include_wakes)

    def __call__(self, flowSimulation):
        if mpi_interface.main:
            flow = self.get_flow(flowSimulation, self.view)
            if self.view.plane[0] == 'z':
                self.view.ax.plot(flow, self.view.X)
            else:
                self.view.ax.plot(self.view.X, flow)


class MPIDummyFlowVisualizer(Flow2DVisualizer):
    def __call__(self, flowSimulation):  # pragma: no cover
        from dynamiks.views import Points
        self.flow2d(flowSimulation, Points(x=[], y=[], z=[]))
