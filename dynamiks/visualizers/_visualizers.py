from abc import abstractmethod, ABC
import numpy as np
from dynamiks.visualizers.visualizer_utils import color_lst
from dynamiks.utils.geometry import get_east_north_height, get_xyz
from itertools import product
from multiclass_interface import mpi_interface


class Visualizer(ABC):

    def initialize(self, ax, view=None):
        self.ax = ax
        if view:
            self.xy_index = ['xyz'.index(c) for c in view.plane]
            self.view = view

    @abstractmethod
    def __call__(self, flowSimulation):
        ""


class WindTurbineVisualizer(Visualizer):
    def __call__(self, flowSimulation):
        flowSimulation.windTurbines.plot_windTurbines(
            flowSimulation, view=self.view, ax=self.ax)


class WindDirectionVisualizer(Visualizer):
    def __init__(self, x, y, z, scale=1):
        x, y = [np.atleast_1d(xy) for xy in [x, y]]
        assert len(x) == len(y)
        self.x = x
        self.y = y
        self.z = np.zeros_like(x) + z
        self.scale = scale

    def __call__(self, flowSimulation):
        if mpi_interface.main:
            x, y, z = xyz = get_xyz(
                [self.x, self.y, self.z], flowSimulation.wind_direction, flowSimulation.center_offset)
            if self.view.view_plane == 'en':
                u, v, w = flowSimulation.get_windspeed(xyz, include_wakes=False)
                theta = np.deg2rad(270 - flowSimulation.wind_direction)
                uvw = np.array([np.cos(theta) * u - np.sin(theta) * v, np.sin(theta) * u + np.cos(theta) * v, w])
                x, y, z = xyz = [self.x, self.y, self.z]
            else:
                uvw = flowSimulation.get_windspeed(xyz, include_wakes=False)
            for x, y, uvw in zip(x, y, uvw.T):
                self.ax.arrow(
                    x, y, *[uvw[i] * self.scale for i in self.xy_index], width=10, color='k', zorder=32)


class ParticleVisualizer(Visualizer):
    def __init__(self, deficit_profile=False, deficit_scale=10, deficit_extend=200, velocity=False, color=None):
        self.color = color
        self.deficit_scale = deficit_scale
        self.deficit_extend = deficit_extend
        Visualizer.__init__(self)
        self.deficit_profile = deficit_profile
        self.velocity = velocity

    def __call__(self, flowSimulation):
        if mpi_interface.main:
            from dynamiks.views import EastNorthView
            fs = flowSimulation

            # index of first, x, and second, y, axis
            ax = self.ax
            for i in range(fs.n_wt):
                c = self.color or color_lst[i % len(color_lst)]

                p_use_idx = flowSimulation.get_active_particles_idx(i, x=flowSimulation.particle_position_xip[0, i])
                xy = fs.particle_position_xip[:, i, p_use_idx][self.xy_index]

                x, y = xy

                if isinstance(self.view, EastNorthView):
                    x, y = xy = get_east_north_height(
                        [xy[0], xy[1], xy[0] * 0], fs.wind_direction, fs.center_offset)[:2]

                ax.plot(x, y, '.', color=c, zorder=32)

                if self.deficit_profile:
                    for j, (x, y) in enumerate(xy.T):
                        assert self.view.plane == 'xy', f'requested plane {self.view.plane} and only xy is implemented'
                        hcw = np.linspace(-self.deficit_extend, self.deficit_extend)
                        deficit = fs.windTurbinesParticles[i].particles[0][j].get_profile(x, hcw, 0, fs.wind_direction)[
                            self.xy_index]

                        ax.plot(x + deficit[0] * self.deficit_scale, y + hcw + deficit[1], color=c, zorder=32)

                if self.velocity:
                    u, v = fs.particle_velocity_uip[:, i, p_use_idx][self.xy_index]
                    if isinstance(self.view, EastNorthView):
                        u, v = get_east_north_height(
                            [u, v, u * 0], fs.wind_direction, np.array([0, 0, 0]))[:2]
                    ax.quiver(*xy, u, v, color=c, zorder=32)

                if len(p_use_idx) > 1:
                    xyz = fs.get_particle_path(i)
                    xy = xyz[self.xy_index]
                    if isinstance(self.view, EastNorthView):
                        xy = get_east_north_height(
                            [xy[0], xy[1], xy[0] * 0], fs.wind_direction, fs.center_offset)[:2]

                    ax.plot(*xy, color=c, zorder=32)


class DataDumperVisualizer(Visualizer):
    def __init__(self, dataDumper):
        self.dataDumper = dataDumper

    def __call__(self, flowSimulation):
        if mpi_interface.main:
            labels = [", ".join(l) for l in product(*[[f'{v}' for v in v_lst]
                                                    for k, v_lst in self.dataDumper.coords.items()])]
            if len(self.dataDumper.time) > 1:
                data = np.reshape(self.dataDumper.data, (len(self.dataDumper.time), -1))
                for d, l in zip(data.T, labels):
                    self.lines = self.ax.plot(self.dataDumper.time, d, label=l)
                if any(labels):
                    self.ax.legend(loc='upper left', bbox_to_anchor=(1, 1))
