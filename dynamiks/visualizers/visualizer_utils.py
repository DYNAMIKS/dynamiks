import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation, PillowWriter
import os
from tqdm.auto import tqdm
from multiclass_interface import mpi_interface


# [plt.plot([])[0].get_color() for _ in range(10)]
color_lst = [
    '#1f77b4',
    '#ff7f0e',
    '#2ca02c',
    '#d62728',
    '#9467bd',
    '#8c564b',
    '#e377c2',
    '#7f7f7f',
    '#bcbd22',
    '#17becf']


class AnimationFigure():
    def __init__(self, fig=None, **fig_kw):
        plt.ioff()
        self.fig = fig or plt.figure(**fig_kw)
        self.gca = self.fig.gca
        self.subplots = self.fig.subplots
        self.clf = self.fig.clf

    def animate(self, flowSimulation, frames=100, dt=1, interval=10, repeat=False, verbose=True):
        pbar = tqdm(total=frames, disable=not (verbose and mpi_interface.main))
        time_start = flowSimulation.time

        def step(index):
            if pbar.n <= index:
                pbar.update()
            flowSimulation.run(time_start + dt * (index + 1))

        self.animation = FuncAnimation(self.fig, step, frames=frames,
                                       interval=interval, blit=False, repeat=repeat)
        self.interval = interval
        return self.animation

    def __call__(self, fs):
        ""

    def save(self, filename):
        ext = os.path.splitext(filename)[1].lower()
        if ext == '.gif':
            writergif = PillowWriter(fps=1000 / self.interval)
            self.animation.save(filename, writer=writergif)
        else:
            self.animation.save(filename, fps=1000 / self.interval, dpi=300)

    def show(self):
        plt.show()  # pragma: no cover


def update_figure(fig):
    if mpi_interface.main:
        try:  # pragma: no cover
            from IPython import get_ipython
            assert 'IPKernelApp' in get_ipython().config
            from IPython import display
            display.display(fig)
            display.clear_output(wait=True)
        except (ModuleNotFoundError, AttributeError, AssertionError):
            fig.canvas.draw()
            fig.canvas.flush_events()
            plt.pause(0.01)


class AxesInitializer():
    def __init__(self, ax, xlabel=None, ylabel=None, title=None, xlim=None, ylim=None, axis='auto', clear=True):
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.title = title
        self.ax = ax
        self.xlim = xlim
        self.ylim = ylim
        self.clear = clear
        self.axis = axis

    def set_lim(self, fs):
        self.ax.axis(self.axis)
        if self.xlim is not None and self.xlim is not False:
            self.ax.set_xlim(self.xlim)
        if self.ylim is not None and self.ylim is not False:
            self.ax.set_ylim(self.ylim)

    def __call__(self, fs):
        # try:  # pragma: no cover
        #     from IPython import get_ipython
        #     # assert 'IPKernelApp' in get_ipython().config
        #     # from IPython import display
        #     # display.clear_output(wait=True)
        #     # display.display(self.ax.figure)
        # except (ModuleNotFoundError, AttributeError, AssertionError):
        #     pass
        from multiclass_interface import mpi_interface
        if mpi_interface.main:
            if self.clear:
                self.ax.cla()

            if self.xlabel:
                self.ax.set_xlabel(self.xlabel)
            if self.ylabel:
                self.ax.set_ylabel(self.ylabel)
            if self.title:
                self.ax.set_title(self.title)
            else:
                self.ax.set_title(np.round(fs.time, 3))

            self.set_lim(fs)
