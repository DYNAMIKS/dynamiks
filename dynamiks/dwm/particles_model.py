import warnings

from dynamiks.dwm.particle_deficit_profiles.particle_deficit_profile import ParticleNoWakeProfile, \
    ParticleDeficitProfile
import numpy as np
from py_wake.utils.grid_interpolator import GridInterpolator
from multiclass_interface.multi_object_list import MultiObjectList
from dynamiks.views import Grid, Points
from scipy.interpolate._cubic import pchip_interpolate
from numpy import newaxis as na
from wetb.utils.timing import print_time


class WindTurbinesParticles(MultiObjectList):
    def __init__(self, windTurbines, n_particles, wakeDeficitModel, addedTurbulenceModel):
        R_lst = windTurbines.diameter() / 2
        MultiObjectList.__init__(self, [WindTurbineParticles(n_particles, R, wakeDeficitModel, addedTurbulenceModel, wt_index)
                                        for wt_index, R in enumerate(R_lst)])


def DistributedWindTurbinesParticles(windTurbines, n_particles, wakeDeficitModel, addedTurbulenceModel):
    dist_wt = windTurbines.dist_wt
    R_lst = windTurbines.diameter() / 2
    dist_wt.windTurbineParticles = [WindTurbineParticles(n_particles, R, wakeDeficitModel, addedTurbulenceModel, wt_index)
                                    for wt_index, R in enumerate(R_lst)]

    dist_wt.get_deficit = get_deficit
    dist_wt.reset_particle = reset_particle
    dist_wt.emit_particle = emit_particle
    dist_wt.set_position = set_position
    return dist_wt


def get_deficit(self, *args, **kwargs):  # pragma: no cover  # run in separate process
    return self.windTurbineParticles.get_deficit(*args, **kwargs)


def reset_particle(self, *args, **kwargs):  # pragma: no cover  # run in separate process
    return self.windTurbineParticles.reset_particle(*args, **kwargs)


def emit_particle(self, *args, **kwargs):  # pragma: no cover  # run in separate process
    self.windTurbineParticles.emit_particle(*args, **kwargs)


def set_position(self, position_xp):
    self.windTurbineParticles.set_position(position_xp)  # pragma: no cover  # run in separate process


class WindTurbineParticles():
    def __init__(self, n_particles, R, wakeDeficitModel, addedTurbulenceModel, wt_index):
        self.n_particles = n_particles
        self.R = R
        self.wakeDeficitModel = wakeDeficitModel
        self.addedTurbulenceModel = addedTurbulenceModel
        self.particles = np.array([ParticleNoWakeProfile(generator=self, ip=(wt_index, p))
                                  for p in range(self.n_particles)])
        self.iwt = wt_index

    def set_position(self, position_xp):
        for p, pos_x in zip(self.particles, position_xp.T):
            p.position = pos_x

    def reset_particle(self, p, particle_positions_x, kwargs):
        self.particles[p] = self.wakeDeficitModel.new_particle_deficit(
            particle_positions_x, ip=(self.iwt, p),
            ** kwargs)

    def emit_particle(self, p, particle_positions_x, yaw, tilt):
        self.particles[p].emit(particle_positions_x, yaw, tilt)

    def get_deficit(self, xyz, particle_positions_xp, mask_p, t, rotor_position, wind_direction):
        if isinstance(xyz, Points):
            x, y, z = [np.atleast_1d(v) for v in xyz]
            YZ = [y, z]
            assert len(np.unique(x)) == 1
            x = np.unique(x)
            s = np.shape(np.atleast_1d(y))
        else:
            s = xyz.shape

            if isinstance(xyz, Grid):
                x, y, z = [np.atleast_1d(v) for v in [xyz.x, xyz.y, xyz.z]]
                YZ = np.meshgrid(y, z, indexing='ij')
        s = (3,) + s
        if self.addedTurbulenceModel:
            u_deficit_norm_y = self.get(ParticleDeficitProfile.get_profile_norm, x, *YZ,
                                        particle_positions_xp, mask_p, wind_direction).reshape(s)
            deficit_norm_gradient_y = self.get(ParticleDeficitProfile.get_profile_norm_gradient, x, *YZ,
                                               particle_positions_xp, mask_p, wind_direction).reshape(s)
            deficit_norm_gradient_norm_y = deficit_norm_gradient_y * self.R
            deficit_uy = self.addedTurbulenceModel(
                xyz, t, self.iwt, rotor_position, u_deficit_norm_y, deficit_norm_gradient_norm_y).reshape(s)
        else:
            deficit_uy = np.zeros(s)
        deficit_uy += self.get(ParticleDeficitProfile.get_profile, x, *YZ, particle_positions_xp, mask_p,
                               wind_direction).reshape(s)
        return deficit_uy

    def get_wake_radius(self, x):
        return np.array([ParticleDeficitProfile.get_wake_radius(p, x_)[0] for x_, p in zip(x, self.particles)])

    def get(self, cls_method, x, y_j, z_j, particle_positions_xp, mask_p, wind_direction):
        x0s, y0s, z0s = particle_positions_xp[:, mask_p]
        res = np.zeros((3, len(x),) + y_j.shape)
        if len(x0s) <= 1:
            return res
        dy = pchip_interpolate(x0s, y0s, x)
        dz = pchip_interpolate(x0s, z0s, x)

        particles = self.particles[mask_p]
        for p_i in range(len(particles) - 1):
            p0 = particles[p_i]
            p1 = particles[p_i + 1]
            x0, x1 = x0s[p_i:p_i + 2]
            dx = x1 - x0
            for i in np.where((x >= x0s[p_i, na]) & (x < x0s[p_i + 1]))[0]:
                v0 = cls_method(p0, x[i], y_j - dy[i], z_j - dz[i], wind_direction)
                v1 = cls_method(p1, x[i], y_j - dy[i], z_j - dz[i], wind_direction)
                w0 = np.where(dx != 0, ((x1 - x[i]) / dx), 1)
                res[:, i] = v0 * w0 + v1 * (1 - w0)
        return res
