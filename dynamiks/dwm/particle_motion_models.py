import numpy as np
from enum import Enum, auto
from py_wake.rotor_avg_models.rotor_avg_model import NodeRotorAvgModel
from scipy import signal
from scipy.signal._signaltools import _validate_sos
from scipy.signal._sosfilt import _sosfilt
from numpy import newaxis as na
from py_wake.deflection_models.deflection_model import DeflectionIntegrator
from py_wake.utils.model_utils import check_model


class CutOffFrq():
    def __init__(self, d):
        self.d = d

    def __call__(self, U, D):
        """Return the low pass filter cut-off frequency

        Parameters
        ----------
        U : int or float
            Wind speed [m/s]
        D : int or float
            Wind turbine rotor diameter  [m]
        """
        return U / (self.d * D)


"""Larsen, G. C., Madsen, H. A., Thomsen, K., and Larsen, T. J.
Wake meandering: a pragmatic approach, Wind Energy, 11, 377–395,
https://doi.org/10.1002/we.267, 2008."""
CutOffFrqLarsen2008 = CutOffFrq(2)

"""Lio, W. H., Larsen, G. C., and Thorsen, G. R.:
Dynamic wake tracking using a cost-effective LiDAR and Kalman filtering: Design, simulation and full-scale validation,
Renew Energ., 172, 1073–1086, 2021."""
"""The 4 is derived from fig. 3, where the full-scale measurements time series amplitude reaches the half peak
amplitude at 0.06 Hz. In this case the U=13m/s and D=52, which corresponds to CutOffFrq(4)"""
CutOffFrqLio2021 = CutOffFrq(4)


class XSpeed(Enum):
    """Particle advection speed along x axis (downstream)"""
    Global = auto()  # advection speed = turbulence transport speed
    Rotor = auto()  # advection speed = wind speed (U) at rotor at the moment when the particle was emitted
    Particle = auto()  # advection speed = wind speed (U) at the current particle position (implicit for y and z speed)


class FirstOrderLowPass():
    def __init__(self, cut_off_frequency, dt):
        if cut_off_frequency > .5 / dt:
            raise ValueError(f"""The cut-off frequency ({cut_off_frequency}Hz) is too high for the time step ({dt}s).
Please reduce to at most {.5 / dt}Hz or discard the temporal filter.""")
        wc = cut_off_frequency * dt * 2 * np.pi
        # alpha for low pass filter with <wc> cut-off frequency, see https://dsp.stackexchange.com/a/40465
        self.alpha = np.cos(wc) - 1 + np.sqrt(np.cos(wc) ** 2 - 4 * np.cos(wc) + 3)
        self.one_minus_alpha = 1.0 - self.alpha

    def __call__(self, x, y):
        # x is input, y is old last output
        return self.one_minus_alpha * y + self.alpha * x

    def reset(self, idx_tuple):
        pass


class SOSFilter():
    def __init__(self, sos, shape):
        # Check that sos is a valid filter in cascaded second-order sections format.
        _validate_sos(sos)
        # Copy input arguments.
        self.sos = sos
        self.shape = shape
        # Initialize state to 0.
        self.zi = np.zeros((np.prod(shape), sos.shape[0], 2))

    def __call__(self, x, y=None):
        # scipy.signal.sosfilt has a significant overhead,
        # Therefore we directly call the cython version.
        # The arguments of _sosfilt() have shape:
        #   - sos: (n_sos, 6)
        #   - out: (n_outputs, 1)
        #   - zi: (n_outputs, n_sos, 2)
        out = x.reshape(-1, 1)
        _sosfilt(self.sos, out, self.zi)
        return out.reshape(self.shape)

    def reset(self, idx):
        # The indices specified by idx will be reset.

        # Build a Boolean mask with a shape that matches the output.
        # By default, we do not reset.
        mask = np.full(self.shape, False)
        # Set the specified indices to True.
        mask[idx] = True
        # Reshape the mask to a 1D array and apply it.
        self.zi[mask.ravel(), :, :] = 0.0


class ParticleMotionModel():
    def __init__(self, x_speed=XSpeed.Global, temporal_filter=CutOffFrqLio2021,
                 spatial_filter=None, include_wakes=False):
        """Models the motion (position and velocity) of the particles

        The velocity in the y and z direction are implicitly set to 'Particle'
        (i.e. the v and w speed at the current particle position)

        Note
        - The time ('current' or 'moment when the particle was released') may reflect
        a low pass filtered average (see the temporal_filter argument)
        - The position ('Rotor' or 'Particle') may represent a spatial average (see the spatial_filter argument)
        - The temporal and spatial filter models the same effect in different ways and probably only one of them should be used

        parameters
        ---------
        x_speed : SpeedType
            Particle advection speed along x axis (downstream)
            - Global: advection speed = turbulence transport speed
            - Rotor: advection speed = wind speed (U) at the rotor position at the moment
            when the particle was emitted
            - Particle: advection speed = current wind speed (U) at the current particle position
        temporal_filter : float, function or ndarray, optional
            The arguments for the temporal filter that is applied to the wind speed to obtain the particle velocity.
            if float: cut-off frequency for a temporal first order low pass filter
            if function: function, f(U,D)->float, returning the cut-off freqency for a temporal first order low pass filter
            (e.g. CutOffFrqLarsen2008 or CutOffFrqLio2021(default)), where U is the turbulence transport speed and
            D is the largest rotor diameter.
            If ndarray: Array of second-order filter coefficients, must have shape (n_sections, 6).
            Each row corresponds to a second-order section, with the first three columns providing the numerator
            coefficients and the last three providing the denominator coefficients.
        spatial_filter : NodeRotorAvgModel (from py_wake.rotor_avg_models.rotor_avg_model) or None, optional
            If NodeRotorAvgModel: The particle speed is set to a spatial (weighted) average of the wind speed at
            a set of nodes (scaled with  before applying the temporal low-pass filter.
            Examples are PyWake NodeRotorAvgModel, e.g. CGIRotorAvg, see
            https://topfarm.pages.windenergy.dtu.dk/PyWake/notebooks/RotorAverageModels.html)
            If None (default), the wind speed at the rotor center or current particle position
            is used without spatial average
        include_wakes : bool, optional
            Include wake from upstream wind turbines in the wind speed used to obtain the particle velocity.

        """
        self._alpha = None
        self.x_speed = x_speed
        self.temporal_filter = temporal_filter
        self.spatial_filter = spatial_filter
        self.include_wakes = include_wakes

    def initialize(self, flowSimulation):
        if isinstance(self.temporal_filter, CutOffFrq):
            self.temporal_filter = self.temporal_filter(U=flowSimulation.site.turbulence_transport_speed,
                                                        D=np.max(flowSimulation.windTurbines.diameter()))
        elif isinstance(self.temporal_filter, np.ndarray):
            self.temporal_filter = SOSFilter(sos=self.temporal_filter,
                                             shape=flowSimulation.particle_position_xip.shape)

        self.dt = flowSimulation.dt
        if isinstance(self.temporal_filter, (int, float)):
            self.temporal_filter = FirstOrderLowPass(self.temporal_filter, self.dt)
        self.flowSimulation = flowSimulation
        self.wt_diameters = flowSimulation.windTurbines.diameter()

    def get_uvw(self, position, wt_index, spatial_filter):
        fs = self.flowSimulation
        if spatial_filter is None:
            if self.include_wakes:
                uvw = fs.get_windspeed(position, include_wakes=self.include_wakes, exclude_wake_from=[wt_index])
            else:
                uvw = fs.get_windspeed(position, include_wakes=False)
        elif isinstance(spatial_filter, NodeRotorAvgModel):
            y, z, w = self.spatial_filter.nodes_x, self.spatial_filter.nodes_y, self.spatial_filter.nodes_weight

            wake_radius = fs.windTurbinesParticles[wt_index].get_wake_radius(fs.particle_position_xip[0, wt_index])[0]
            nodes_position_xpn = position[:, :, na] + \
                np.array([y * 0, y, z])[:, na, :] * wake_radius[na, :, na]
            uvw_upn = self.get_uvw(nodes_position_xpn.reshape((3, -1)), wt_index, None).reshape(
                nodes_position_xpn.shape)
            if w is None:
                return np.mean(uvw_upn, -1)
            return np.sum(w * uvw_upn, -1)

        else:
            raise NotImplementedError()
        if self.x_speed == XSpeed.Global:
            uvw[0] = self.flowSimulation.site.turbulence_transport_speed
        return uvw

    def reset_particles(self, position_xip, wt_idx=None):
        if wt_idx is None:
            assert position_xip.shape[1] == self.flowSimulation.n_wt
            wt_idx = np.arange(self.flowSimulation.n_wt)
        new_velocity_uip = np.moveaxis([self.get_uvw(position_xip[:, i], wt_index=j,
                                                     spatial_filter=self.spatial_filter)
                                        for i, j in enumerate(wt_idx)], 0, 1)
        if self.temporal_filter:
            for i, p in zip(wt_idx, self.flowSimulation.boundary_particle_index[wt_idx]):
                self.temporal_filter.reset((slice(None), i, p))

        new_velocity_uip[0] = np.maximum(new_velocity_uip[0], 1)  # set minimum speed to 1 m/s
        return new_velocity_uip

    def reset_particle(self, position_x, wt_idx):
        return self.reset_particles(position_x[:, na, na], [wt_idx])[:, 0, 0]

    def __call__(self, position_xip, velocity_uip, wt_idx=None):
        assert velocity_uip is None or position_xip.shape == velocity_uip.shape
        if wt_idx is None:
            assert position_xip.shape[1] == self.flowSimulation.n_wt
            wt_idx = np.arange(self.flowSimulation.n_wt)
        new_velocity_uip = np.moveaxis([self.get_uvw(position_xip[:, i], wt_index=j,
                                                     spatial_filter=self.spatial_filter)
                                        for i, j in enumerate(wt_idx)], 0, 1)

        bpi = self.flowSimulation.boundary_particle_index[wt_idx]

        if self.temporal_filter:
            new_velocity_uip = self.temporal_filter(new_velocity_uip, velocity_uip)
        if self.x_speed in [XSpeed.Global, XSpeed.Rotor]:
            new_velocity_uip[0] = velocity_uip[0]

        new_velocity_uip[0] = np.maximum(new_velocity_uip[0], 1)  # set minimum speed to 1 m/s
        position = position_xip + new_velocity_uip * self.dt

        # do not move boundary particles
        position[:, wt_idx, bpi] = position_xip[:, wt_idx, bpi]

        return position, new_velocity_uip


class HillVortexParticleMotion(ParticleMotionModel):
    def __call__(self, position_xip, velocity_uip, wt_idx=None):
        position, new_velocity_uip = ParticleMotionModel.__call__(self, position_xip, velocity_uip, wt_idx=wt_idx)

        if wt_idx is None:
            wt_idx = np.arange(self.flowSimulation.n_wt)

        fs = self.flowSimulation
        for i in wt_idx:
            x_p = position_xip[0, i]
            m = [p.yaw is not None for p in fs.windTurbinesParticles[i].particles[0]]
            delta_U_w = np.array([p.deficit_norm_magnitude([x - p.initial_position[0]])[0]
                                  for p, x in zip(fs.windTurbinesParticles[i].particles[0][m], x_p[m])])
            yaw = [p.yaw for p in fs.windTurbinesParticles[i].particles[0][m]]
            tilt = [p.tilt for p in fs.windTurbinesParticles[i].particles[0][m]]

            # for 0<theta_yaw<180, self induc point in positive y direction
            theta_yaw = np.deg2rad(np.array(yaw) - self.flowSimulation.wind_direction)
            theta_tilt = np.deg2rad(tilt)
            self_induc_um = delta_U_w * .4 * [-np.cos(theta_yaw) * np.cos(theta_tilt),  # induc opposite x
                                              np.cos(theta_tilt) * np.sin(theta_yaw),
                                              np.sin(theta_tilt)]

            position[:, i, m] = position_xip[:, i, m] + (new_velocity_uip[:, i, m] + self_induc_um) * self.dt
        # boundary particles are not affected as they have yaw=None, so no need to reset boundary particle positions
        return position, new_velocity_uip


class PyWakeDeflectionModel(ParticleMotionModel):
    def __init__(self, pyWakeDeflectionModel, x_speed=XSpeed.Global, temporal_filter=CutOffFrqLio2021,
                 spatial_filter=None, include_wakes=False):
        check_model(model=pyWakeDeflectionModel, cls=DeflectionIntegrator,
                    arg_name=pyWakeDeflectionModel, accept_None=False)
        self.pywakeDeflectionModel = pyWakeDeflectionModel
        ParticleMotionModel.__init__(self, x_speed=x_speed, temporal_filter=temporal_filter,
                                     spatial_filter=spatial_filter, include_wakes=include_wakes)

    def __call__(self, position_xip, velocity_uip, wt_idx=None):
        position, new_velocity_uip = ParticleMotionModel.__call__(self, position_xip, velocity_uip, wt_idx=wt_idx)

        if wt_idx is None:
            wt_idx = np.arange(self.flowSimulation.n_wt)

        fs = self.flowSimulation
        for i in wt_idx:
            x_p = position_xip[0, i]
            m = [p.yaw is not None for p in fs.windTurbinesParticles[i].particles[0]]
            yaw = [p.yaw for p in fs.windTurbinesParticles[i].particles[0][m]]
            tilt = [p.tilt for p in fs.windTurbinesParticles[i].particles[0][m]]
            theta_yaw, theta_tilt = np.deg2rad(np.array(yaw) - self.flowSimulation.wind_direction), np.deg2rad(tilt)
            # total angle
            w = np.tan(theta_yaw)
            L = np.hypot(1, w)
            h = np.tan(theta_tilt) * L
            theta_total = np.arctan(np.hypot(w, h))
            theta_total_direction = np.arctan2(h, w)

            deflection_rate = [
                self.pywakeDeflectionModel.get_deflection_rate(
                    theta_ilk=total[na, na, na], **p.kwargs, dw_ijlkx=x, IJLK=(1, 1, 1, 1))[0, 0, 0, 0, 0]
                for p, total, x in zip(fs.windTurbinesParticles[i].particles[0][m], theta_total, x_p[m])]
            deflection_angle = np.arcsin(deflection_rate)

            # def polar2cart(r, theta, phi):
            #     return np.array([np.cos(theta), np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi)])
            # M = np.array([polar2cart(1, deflection_angle, theta_total_direction),
            #               polar2cart(1, deflection_angle, theta_total_direction + np.pi / 2),
            #               polar2cart(1, deflection_angle + np.pi / 2, theta_total_direction),
            #               ]).T

            # axes of rotated coordinate system (same as above)
            ca, cb = np.cos((deflection_angle, theta_total_direction))
            sa, sb = np.sin((deflection_angle, theta_total_direction))
            # M has shape (deflection_angle.size, 3, 3).
            M = np.array([[ca, sa * cb, sa * sb],
                          [ca, -sa * sb, sa * cb],
                          [-sa, ca * cb, ca * sb]]).T

            position[:, i, m] = position_xip[:, i, m] + \
                np.array([m @ uvw for m, uvw in zip(M, new_velocity_uip[:, i, m].T)]).T * self.dt

        # boundary particles are not affected as they have yaw=None, so no need to reset boundary particle positions

        return position, new_velocity_uip
