from abc import ABC, abstractmethod
import numpy as np
from numpy import newaxis as na


class ParticleDeficitGenerator(ABC):
    @abstractmethod
    def new_particle_deficit(self, initial_position, u0, u_eff):
        ""


class ParticleDeficitProfile(ABC):

    def __init__(self, generator, initial_position, u_scale, ip):
        self.generator = generator
        self._initial_position = initial_position
        self._u_scale = u_scale
        self.ip = ip
        self.emitted = False
        self.position = initial_position
        self.yaw = None
        self.tilt = None

    def emit(self, position, yaw, tilt):
        self.emitted = True
        self._initial_position = position
        self.yaw = yaw
        self.tilt = tilt

    @property
    def initial_position(self):
        return self._initial_position

    def u_scale(self, rel_y=0, rel_z=0):
        return np.atleast_1d(self._u_scale)

    def get_rel_xyz(self, x, rel_y, rel_z):
        shape = [np.shape(np.atleast_1d(v)) for v in [rel_y, rel_z]][isinstance(rel_y, (int, float))]

        if self.emitted:
            rel_x = self.position[0] - self.initial_position[0]
            return [(np.zeros(shape) + xyz) for xyz in [rel_x, rel_y, rel_z]]
        else:
            return [(np.zeros(shape) + xyz) for xyz in [0, rel_y, rel_z]]

    def get_profile_norm(self, x, rel_y, rel_z, wind_direction):
        if self.initial_position is None:
            return np.zeros((3,) + rel_y.shape)
        xyz = self.get_rel_xyz(x, rel_y, rel_z)
        profile = self._get_profile_norm(*xyz)
        if self.yaw is not None and self.yaw != wind_direction:
            w = np.tan(np.deg2rad(wind_direction - self.yaw))
            L = np.hypot(1, w)
            h = np.tan(np.deg2rad(-self.tilt)) * L
            theta_total = np.arctan(np.hypot(w, h))
            theta_total_direction = np.arctan2(h, w)

            projection = np.array([np.cos(theta_total), np.sin(theta_total) * np.cos(theta_total_direction),
                                   np.sin(theta_total) * np.sin(theta_total_direction)])
        else:
            projection = np.array([1, 0, 0])
        return np.array([p * profile for p in projection])

    def get_profile(self, x, rel_y, rel_z, wind_direction):
        return self.get_profile_norm(x, rel_y, rel_z, wind_direction) * self.u_scale(rel_y, rel_z)

    def get_profile_norm_gradient(self, x, rel_y, rel_z, wind_direction):
        xyz = self.get_rel_xyz(x, rel_y, rel_z)
        return self._get_profile_norm_gradient(*xyz)

    def get_wake_radius(self, x):
        rel_x = self.get_rel_xyz(x, 0, 0)[0]
        return self._get_wake_radius(rel_x)

    @abstractmethod
    def _get_profile_norm(self, rel_x, rel_y, rel_z):
        ""

    @abstractmethod
    def deficit_norm_magnitude(self, rel_x):
        ""

    @abstractmethod
    def _get_profile_norm_gradient(self, rel_x, rel_y, rel_z):
        ""

    @abstractmethod
    def _get_wake_radius(self, rel_x):
        ""


class ParticleNoWakeProfile(ParticleDeficitProfile):
    def __init__(self, generator, ip):
        ParticleDeficitProfile.__init__(self, generator, initial_position=None, u_scale=0, ip=ip)

    def _get_profile_norm(self, rel_x, rel_y, rel_z):
        "return np.zeros_like(rel_y)"

    def deficit_norm_magnitude(self, rel_x):
        "return np.zeros_like(rel_x)"

    def _get_profile_norm_gradient(self, rel_x, rel_y, rel_z):
        "return np.zeros_like(rel_y)"

    def _get_wake_radius(self, rel_x):
        return np.zeros_like(rel_x)
