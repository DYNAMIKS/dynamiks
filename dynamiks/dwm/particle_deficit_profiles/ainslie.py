import numpy as np
from dynamiks.dwm.particle_deficit_profiles.particle_deficit_profile import ParticleDeficitGenerator, \
    ParticleDeficitProfile
from jDWM import BoundaryCondition
from jDWM import EddyViscosityModel
from jDWM.Solvers import implicit
from jDWM.EddyViscosityModel import madsen, larsen, IEC, keck
import inspect
import warnings


class jDWMAinslieGenerator(ParticleDeficitGenerator):
    def __init__(self, boundaryConditionModel=BoundaryCondition.madsen,
                 viscosity_model=EddyViscosityModel.madsen, solver=implicit(),
                 scale_with_freestream=False, r_max=3, n_r=51, dx=0.1):
        """
        Parameters
        ----------
        boundaryConditionModel : jDWM.BoundaryConditionModel, optional
            boundary condition
        viscosity_model : jDWM.EddyViscosityModel, optional
            Eddy viscosity model, class or object (TI will be overriden, so it can be set to TI=1)
        solver : jDWM.Solver, optional
            solver
        scale_with_freestream : bool, optional
            Specify if normalized ainlie deficit should be scaled with freestream (i.e. without wakes) or
            effective wind speed including wakes (default)
        r_max : int or float
            Width of axisymmetric control volume [R] (normalized by rotor radius)
        n_r : int
            Number of points in axisymmetric control volume
        """
        if inspect.isclass(viscosity_model):
            viscosity_model = viscosity_model(TI=1)
        assert (isinstance(viscosity_model, EddyViscosityModel.EddyViscosityModel))
        assert issubclass(boundaryConditionModel, BoundaryCondition.BoundaryCondition)
        vm_kwargs = {k: getattr(viscosity_model, k, 'missing')
                     for k in inspect.getfullargspec(viscosity_model.__init__).args[2:]}
        self.viscosity_model = lambda TI, kwargs =vm_kwargs, cls=viscosity_model.__class__: cls(TI=TI, **kwargs)
        self.solver = solver
        self.boundaryConditionModel = boundaryConditionModel()
        self.scale_with_freestream = scale_with_freestream
        self.r_max = r_max
        self.n_r = n_r
        dr = r_max / (n_r - 1)
        if isinstance(solver, implicit) and (dr > .2 or dx < dr**2 * 25):
            warnings.warn(
                "The implicit solver is probably unstable for the current dr and dx. See https://doi.org/10.5194/wes-8-1387-2023 figure 2",
                RuntimeWarning)

        self.dx = dx

    def get_kwargs_func(self):
        def kwargs_func(windTurbine):
            if self.scale_with_freestream:
                u_scale = windTurbine.rotor_avg_freestream[0]
            else:
                u_scale = windTurbine.rotor_avg_windspeed[0]
            r = np.linspace(0, self.r_max, self.n_r)
            return {'u_scale': u_scale,
                    'a': windTurbine.axisymetric_induction(r)[:, 0],
                    'TI': windTurbine.rotor_avg_ti()[0],
                    'D': windTurbine.diameter(),
                    'CT': windTurbine.ct()}
        return kwargs_func

    def new_particle_deficit(self, particle_position, ip, u_scale, a, TI, D, CT):
        r = np.linspace(0, self.r_max, self.n_r)
        U, V = self.boundaryConditionModel(r, a)
        kwargs = dict(D_src_il=np.reshape(D, (1, 1)),
                      # WS_ilk=np.reshape(u_freestream / u_scale, (1, 1, 1)),
                      # WS_eff_ilk=np.reshape(1, (1, 1, 1)),
                      ct_ilk=np.reshape(CT, (1, 1, 1)),
                      # TI_ilk=np.reshape(TI, (1, 1, 1)),
                      # TI_eff_ilk=np.reshape(TI, (1, 1, 1)),
                      # h_ilk=np.reshape(h, (1, 1, 1)),
                      # yaw_ilk=np.reshape(yaw, (1, 1, 1)),
                      # tilt_ilk=np.reshape(tilt, (1, 1, 1))
                      )
        return AinslieDeficitProfile(generator=self, particle_position=particle_position, ip=ip,
                                     u_scale=u_scale,
                                     diameter=D, r=r, U=U, V=V, TI=TI, kwargs=kwargs)


class AinslieDeficitProfile(ParticleDeficitProfile):
    def __init__(self, generator, particle_position, ip, diameter, u_scale, r, U, V, TI, kwargs):
        ParticleDeficitProfile.__init__(
            self,
            generator=generator,
            initial_position=particle_position,
            u_scale=u_scale,
            ip=ip)
        self.R = diameter / 2
        self.r = r
        self.dr = r[1] - r[0]
        self.U = U
        self.V = V
        self.dx = self.generator.dx or 25. * self.dr**2
        self.evolved_x_R = 0
        self.dUdr = U * 0
        self.viscosity_model = self.generator.viscosity_model(TI=TI)
        self.kwargs = kwargs

    def evolve_profile(self, dx):
        self.evolved_x_R += dx
        visc = self.viscosity_model(self.evolved_x_R, self.r, self.U)
        self.U, self.V = self.generator.solver.evolve(self.r, self.U, self.V, visc, dx, self.dr)
        self.dUdr = np.gradient(self.U, self.dr)

    def _get_profile_norm(self, rel_x, rel_y, rel_z):
        rel_x, rel_y, rel_z = [v / self.R for v in [rel_x, rel_y, rel_z]]
        deficit = np.zeros(rel_x.shape)
        for x in np.sort(np.unique(rel_x)):
            m = rel_x == x
            while (self.evolved_x_R + self.dx) < x:
                self.evolve_profile(self.dx)
            if x > self.evolved_x_R:
                self.evolve_profile(x - self.evolved_x_R)

            deficit[m] = np.interp(rel_y[m]**2 + rel_z[m]**2, self.r**2, 1 - self.U)
        return deficit

    def _get_profile_norm_gradient(self, rel_x, rel_y, rel_z):
        rel_x, rel_y, rel_z = [v / self.R for v in [rel_x, rel_y, rel_z]]
        assert len(np.unique(rel_x)) == 1
        assert np.unique(rel_x) <= self.evolved_x_R, (self.ip,
                                                      np.unique(rel_x), self.evolved_x_R)  # Call get_profile first
        deficit_gradient_norm = np.interp(rel_y**2 + rel_z**2, self.r**2, self.dUdr)
        return deficit_gradient_norm / self.R

    def deficit_norm_magnitude(self, rel_x):
        if rel_x > self.evolved_x_R * self.R:
            self._get_profile_norm(rel_x, rel_y=0, rel_z=0)
        return self.u_scale(0, 0) * (1 - self.U.min())

    def _get_wake_radius(self, rel_x):
        dU = 1 - self.U
        lim = dU.max() * np.exp(-2)  # 2*sigma assuming gaussian profile
        return np.reshape(self.r[np.argmin(dU > lim)] * self.R, rel_x.shape)
