from numpy import newaxis as na
from dynamiks.dwm.added_turbulence_models import SynchronizedAutoScalingIsotropicMannTurbulence
from dynamiks.views import Points, View
import numpy as np
import xarray as xr
from py_wake.utils.model_utils import check_model
from dynamiks.dwm.particle_motion_models import ParticleMotionModel
from py_wake.superposition_models import LinearSum
from dynamiks.dwm.particles_model import WindTurbinesParticles
from dynamiks.flow_simulation import FlowSimulation
from dynamiks.dwm.particle_deficit_profiles.particle_deficit_profile import ParticleNoWakeProfile
from scipy.interpolate._cubic import pchip_interpolate
from multiclass_interface import mpi_interface

"""
suffixes:
u: uvw component
x: xyz component
i: wt
g: 1,2 or 3 grid dimensions
p: particle
"""
step_handler_time = {}


class DWMFlowSimulation(FlowSimulation):
    """Dynamic wake meandering flow simulation.

    In this model, a row of particles are assosciated with each wind turbine.
    The particles holds information needed to calculate the deficit and added turbulence at down stream positions

    In each time step, all particles, except the first, are moved (meandered) with the wind (mostly downstream,
    but also in the crosswind and vertical direction.

    The first particle is fixed to the rotor center and updated with rotor position, wind speed, induction/ct etc.
    When the second particles has moved <d_particle> diameters downstream, the first particle is released, and
    the last particle is reset and fixed to the rotor center.


    """

    def __init__(self, site, windTurbines, particleDeficitGenerator, dt,
                 particleMotionModel=ParticleMotionModel(), d_particle=0.2, n_particles=None,
                 wind_direction=270,
                 step_handlers=None,
                 superpositionModel=LinearSum(),
                 addedTurbulenceModel=SynchronizedAutoScalingIsotropicMannTurbulence(),
                 windTurbinesParticles=WindTurbinesParticles
                 ):
        """
        Parameters
        ----------
        site : Site
            site object, e.g. dynamiks.sites.TurbulenceFieldSite
            see https://dynamiks.pages.windenergy.dtu.dk/dynamiks/notebooks/Site.html
        windTurbines : WindTurbines
            windTurbines object e.g. dynamiks.wind_turbines.PyWakeWindTurbines, dynamiks.wind_turbines.HAWC2WindTurbines
            see https://dynamiks.pages.windenergy.dtu.dk/dynamiks/notebooks/WindTurbines.html
        particleDeficitGenerator : ParticleDeficitGenerator
            Model to instantiate the wake deficit model of new particles, e.g.
            dynamiks.dwm.PyWakeDeficitGenerator, dynamiks.dwm.jDWMAinslieGenerator
            see https://dynamiks.pages.windenergy.dtu.dk/dynamiks/notebooks/DWMFlowSimulation.html#ParticleDeficitGenerator
        dt : float
            Time increment in dynamiks (sub models, e.g. HAWC2, are allowed to have smaller increments).
            Must be small enough to capture the interaction between flow model and wind turbines
        particleMotionModel : ParticleMotionModel, optional
            model to handle particles movement and velocity, e.g.
            dynamiks.particle_motion_models.ParticleMotionModel (default) or
            dynamiks.particle_motion_models.HillVortexParticleMotion
        d_particle : int or float, optional
            Distance between particles normalized with rotor diameter
            Default is 0.2D
            Must be small enough to capture non-linearities in wake and the particle meandering path
        n_particles : int or None
            Number of particles associated with each turbine.
            If None, default, the number of particles is set to cover 120% of the farm size (at least 10)
        wind_direction : int or float, optional
            Wind direction used internally to rotate the wind farm (x is always the turbulence box advection direction).
            Use the EastNorthView to see the wind from the right direction.
        step_handlers : list
            List of step handlers that is called in every time step.
            Step handlers can be:
            - step_handler, i.e. a function, f(flowSimulation)->None
            - (<time_start>, <time_step>, step_handler)
            - (<time_start>, <time_step>, [step_handler1, step_handler2, ...])
        superpositionModel : py_wake.SuperpositionModel, optional
            model used to sum up deficits from multiple wind turbines.
            Default model is LinearSum from py_wake.py_wake.superposition_models
        addedTurbulenceModel : AddedTurbulenceModel, optional
            AddedTurbulence model object
            Default is SynchronizedAutoScalingIsotropicMannTurbulence
        windTurbinesParticles : {WindTurbinesParticles, DistributedWindTurbinesParticles}, optional
            Default is WindTurbinesParticles
            Use DistributedWindTurbinesParticles to improve parallel performance
        """

        step_handlers = step_handlers or []
        FlowSimulation.__init__(self, site, windTurbines, dt=dt, wind_direction=wind_direction,
                                step_handlers=step_handlers)
        self.step_handlers.insert(0, self.update_particles)
        self.particleDeficitGenerator = particleDeficitGenerator
        self.superpositionModel = superpositionModel

        check_model(particleMotionModel, ParticleMotionModel, arg_name="particleMotionModel")
        self.particleMotionModel = particleMotionModel

        n_wt = self.windTurbines.N
        D = self.windTurbines.diameter()
        self.update_wind_turbine_sensors(self)
        if addedTurbulenceModel:
            addedTurbulenceModel.initialize(self)

        self.d_particle = d_particle * D
        if n_particles is None:
            # farm size in downwind direction
            x = self.windTurbines.rotor_positions_xyz[0]
            farm_size = x.max() - x.min()
            n_particles = np.maximum(int(np.ceil(farm_size * 1.2 / min(self.d_particle))), 10)

        # assert np.all(self.d_particle * self.n_particles > farm_size)
        self.n_particles = n_particles
        self.particle_position_xip = (((np.arange(self.n_particles)[na] *
                                        self.d_particle[:, na])[na] *
                                       np.array([1., 0, 0])[:, na, na]) +
                                      self.windTurbines.rotor_positions_xyz[:, :, na])

        self.particleMotionModel.initialize(self)
        self._last_particle_index = np.full(n_wt, -1, dtype=int)
        self.boundary_particle_index = np.zeros(n_wt, dtype=int)
        self._active_particles = np.full((n_wt, self.n_particles), False, dtype=bool)

        self.windTurbinesParticles = windTurbinesParticles(
            windTurbines, n_particles, particleDeficitGenerator, addedTurbulenceModel)
        self.kwargs_func = self.particleDeficitGenerator.get_kwargs_func()

        for i in range(n_wt):
            kwargs = self.kwargs_func(self.windTurbines[i])
            self.windTurbinesParticles[i].reset_particle(0, self.particle_position_xip[:, i, 0], kwargs)
        self._active_particles[:, 0] = True

        self.particle_velocity_uip = self.particleMotionModel.reset_particles(self.particle_position_xip)

    def update_wind_turbine_sensors(self, flowSimulation):
        if mpi_interface.main or mpi_interface.COLLECTIVE_MPI:
            wts = self.windTurbines
            if 'rotor_avg_windspeed' not in wts.sensors.dict:
                wts.add_sensor('rotor_avg_windspeed', expose=True, ext_lst=['u', 'v', 'w'])
                wts.add_sensor('rotor_avg_freestream', expose=True, ext_lst=['u', 'v', 'w'])
            wts.rotor_avg_windspeed = wts.get_rotor_avg_windspeed(include_wakes=flowSimulation.time > 0).T
            wts.rotor_avg_freestream = wts.get_rotor_avg_windspeed(include_wakes=False).T

    def update_particles(self, flowSimulation):
        windturbine_positions = self.windTurbines.positions_xyz
        rotor_positions = self.windTurbines.rotor_positions_xyz

        dx = (self.particle_position_xip[0, np.arange(self.n_wt), (self.boundary_particle_index + 1) % self.n_particles] -
              windturbine_positions[0])  # could also be rotor_position
        m = (dx >= self.d_particle)

        if any(m):
            yaw, tilt = self.windTurbines.yaw_tilt()
            yaw = self.wind_direction - yaw

            # emit old boundary particle
            for i, p in enumerate(self.boundary_particle_index):
                if m[i]:
                    self.windTurbinesParticles[i].emit_particle(p, rotor_positions[:, i], yaw[i], tilt[i])

            # update boundary_particle_index
            self.boundary_particle_index[m] = (self.boundary_particle_index[m] - 1) % self.n_particles
            self.particle_position_xip[:, m, self.boundary_particle_index[m]] = rotor_positions[:, m]
            self.update_wind_turbine_sensors(self)
            # reset new boundary particle
            for i, p in enumerate(self.boundary_particle_index):
                if m[i]:
                    kwargs = self.kwargs_func(self.windTurbines[i])
                    self.windTurbinesParticles[i].reset_particle(p, rotor_positions[:, i], kwargs)
                    self._active_particles[i, p] = True
                    # set new boundary particle speed
                    self.particle_velocity_uip[:, i, p] = self.particleMotionModel.reset_particle(
                        rotor_positions[:, i], [i])

            self.particle_position_xip[:, m, self.boundary_particle_index[m]] = rotor_positions[:, m]

        # propagate active particles
        self.particle_position_xip, self.particle_velocity_uip = self.particleMotionModel(
            self.particle_position_xip, self.particle_velocity_uip)

        self.windTurbinesParticles.set_position(list(np.moveaxis(self.particle_position_xip, 1, 0)))
        self.update_wind_turbine_sensors(self)

    def get_active_particles_idx(self, wt_idx, x):
        px = np.array([self.particle_position_xip[0, wt_idx], np.arange(self.n_particles)])
        px_ordered = np.roll(px, -self.boundary_particle_index[wt_idx], 1)
        active_ordered = np.roll(self._active_particles[wt_idx], -self.boundary_particle_index[wt_idx])
        px_active_ordered = px_ordered[:, active_ordered]
        idx_mono_inc = np.where(np.r_[1, np.diff(np.maximum.accumulate(px_active_ordered[0]))] > 0)[0]
        px_active_mono_inc = px_active_ordered[:, idx_mono_inc]

        idx = np.searchsorted(px_active_mono_inc[0], x)
        # we need 2 particles on both sides of x for pchip interpolation
        idx = (np.unique(idx) + np.arange(-2, 2)[:, na]).flatten()
        idx = np.sort(np.unique(np.clip(idx, 0, len(idx_mono_inc) - 1)))
        return px_active_mono_inc[1, idx].astype(int)

    def get_particle_path(self, wt_idx, x=None):
        if x is None:
            px = self.particle_position_xip[0, wt_idx, self._active_particles[wt_idx]]
            x = np.linspace(px.min(), px.max(), 1000)
        px, py, pz = self.particle_position_xip[:, wt_idx, self.get_active_particles_idx(wt_idx, x)]
        if len(px) < 2:
            return np.array([[], [], []])
        x = np.atleast_1d(x)
        x = x[(x >= px[0]) & (x <= px[-1])]

        return np.array([x, pchip_interpolate(px, py, x), pchip_interpolate(px, pz, x)])

    def _get_windspeed(self, view, include_wakes, exclude_wake_from=[], time=None):
        uvw = self.site.get_windspeed(view, time=time)

        if include_wakes:
            deficit = self.get_deficit(view, exclude_wake_from)
            uvw -= deficit
        return uvw

    def get_deficit(self, view, exclude_wake_from=[]):
        assert isinstance(view, View)
        x = view[0]
        src_wt_lst = np.array([i for i in np.arange(self.n_wt) if i not in exclude_wake_from])

        if len(src_wt_lst) == 0:
            return 0

        if isinstance(view, Points) and len(np.unique(x)) > 1:
            deficit_up = np.zeros((3, len(x)))
            for x_ in np.unique(x):
                m = x_ == x
                deficit_up[:, m] = self.get_deficit(Points(*[view[i][m] for i in [0, 1, 2]]), exclude_wake_from)
            return deficit_up
        else:
            mask_ip = [self.get_active_particles_idx(src_wt, x) for src_wt in src_wt_lst]

        deficit_iug = self.windTurbinesParticles[src_wt_lst].get_deficit(
            view, [self.particle_position_xip[:, i] for i in src_wt_lst], mask_ip,
            self.time, self.windTurbines.rotor_positions_xyz[:, src_wt_lst].T.tolist(), self.wind_direction)
        return self.superpositionModel(np.array(deficit_iug))

    def get_turbulence_intensity(self, xyz, include_wake_turbulence):
        if isinstance(xyz, (list, tuple, np.ndarray)):
            view = Points(*xyz)
        else:
            view = xyz
        return self.site.get_turbulence_intensity(view)
