from tqdm.auto import tqdm
import matplotlib.pyplot as plt
from dynamiks.views import XYView, View, Points
from dynamiks.visualizers.visualizer_utils import AnimationFigure
import numpy as np
import time
from dynamiks.utils.geometry import get_east_north_height, get_xyz
from abc import ABC, abstractmethod
from multiclass_interface import mpi_interface
from dynamiks.utils import is_notebook
import xarray as xr


"""
suffixes:
u: uvw component
x: xyz component
i: wt
g: 1,2 or 3 grid dimensions
"""
step_handler_time = {}


class FlowSimulation(ABC):

    def __init__(self, site, windTurbines, dt, wind_direction=270, step_handlers=None):
        self.time = 0
        self._wind_direction = wind_direction
        self.site = site
        site.initialize(self)
        self.step_handlers = step_handlers or []

        wt_pos = windTurbines.positions_east_north
        self.center_offset = (wt_pos.max(1) + wt_pos.min(1)) / 2
        self.n_wt = windTurbines.N
        windTurbines.initialize(self)
        self.windTurbines = windTurbines
        self.step_handlers = site.step_handlers + windTurbines.step_handlers + self.step_handlers

        self.dt = float(dt)
        # self.wt_diameters = self.windTurbines.diameter()

    @property
    def wind_direction(self):
        return self._wind_direction

    @wind_direction.setter
    def wind_direction(self, wd):
        p_pos_eip = get_east_north_height(self.particle_position_xip, self.wind_direction, self.center_offset)
        p_vel_eip = get_east_north_height(self.particle_velocity_uip, self.wind_direction, np.array([0, 0, 0]))
        self._wind_direction = wd
        self.particle_position_xip = get_xyz(p_pos_eip, self.wind_direction, self.center_offset)
        self.particle_velocity_uip = get_xyz(p_vel_eip, self.wind_direction, np.array([0, 0, 0]))
        self.update_wind_turbine_sensors(self)

    def run(self, time_stop, verbose=False):
        steps = int(np.ceil(np.round((time_stop - self.time) / self.dt, 6)))
        for _ in tqdm(range(steps), disable=not (verbose and mpi_interface.main)):
            self.step()

    def step(self):
        self.time = np.round(self.time + self.dt, 6)
        self.step_stephandlers()

    def step_stephandlers(self):
        for step_handler in self.step_handlers:

            if isinstance(step_handler, tuple):
                (t0, dt), step_handler = step_handler
                if self.time > t0 and np.round(self.time / dt, 6) % 1 == 0:
                    for sh in np.atleast_1d(step_handler):
                        sh(self)
            else:
                if hasattr(self, 'step_handler_time'):
                    t = time.time()
                    step_handler(self)
                    self.step_handler_time[step_handler] = time.time() - t + self.step_handler_time.get(step_handler, 0)
                else:
                    step_handler(self)

    def add_step_handlers(self, step_handlers, t0=0, dt=None):
        self.step_handlers.append(((t0, dt or self.dt), step_handlers))
        return self.step_handlers[-1]

    @abstractmethod
    def update_wind_turbine_sensors(self, flowSimulation):
        ""

    def get_windspeed(self, xyz, include_wakes=True, exclude_wake_from=[], time=None, xarray=False):
        if isinstance(xyz, View):
            view = xyz
        else:
            view = Points(*xyz)

        uvw = self._get_windspeed(view, include_wakes=include_wakes, exclude_wake_from=exclude_wake_from, time=time)

        if xarray:
            return xr.DataArray(uvw, dims=['uvw'] + list(view.plane),
                                coords={'uvw': ['u', 'v', 'w'], **view.get_coords(self)},
                                attrs={xyz: getattr(view, xyz) for xyz in 'xyz' if xyz not in view.plane})
        else:
            return uvw

    @abstractmethod
    def _get_windspeed(self, view, include_wakes=True, time=None):
        ""

    def get_wind_direction(self, xyz, include_wakes, xarray=False):
        uvw = self.get_windspeed(xyz, include_wakes=include_wakes, xarray=xarray)
        e, n, h = get_east_north_height(uvw, self.wind_direction, [0, 0, 0])
        wd = (270 - np.rad2deg(np.arctan2(n, e))) % 360
        if xarray:
            wd = uvw[0].drop_vars('uvw') * 0 + wd
        return wd

    def _set_view_defaults(self, view, **kwargs):
        if 'flowVisualizer' in kwargs:  # pragma: no cover
            raise Exception(f"""show, visualize and animate with arguments view, flowVisualizer etc is deprecated.
             Use show({view.__class__.__name__}(..., flow_visualizer={kwargs['flowVisualizer'].__class__.__name__}(...)) instead""")
        if view is None:
            view = XYView(z=None)
        view.initialize_visualizers(self)
        return view

    def show(self, view=None, block=True, **kwargs):
        assert 'show' not in kwargs, "show argument deprecated. Use block instead"
        view = self._set_view_defaults(view, **kwargs)

        view(self)
        if not is_notebook() and block:  # pragma: no cover
            plt.show(block=block)

    def visualize(self, time_stop, view=None, dt=None, id="", **kwargs):
        assert isinstance(dt, (int, float, type(None))), f"dt must be int, float or None, but is {dt}"
        view = self._set_view_defaults(view, **kwargs)

        with plt.ion():
            view.interactive = True

            sh = self.add_step_handlers(view, t0=0, dt=dt)

            try:
                self.run(time_stop)
            finally:
                self.step_handlers.remove(sh)

    def animate(self, time_stop, view=None, filename=None, dt=None, interval=None, verbose=True, **kwargs):
        """
        Parameters
        ----------
        dt : float or None, optional
            Time step [s] between capturing frames from the simulation.
            If None, default, the time step of the flow simulation is used
        interval : float or None, optional
            Time step [ms] between frames in animation. If None, default, interval is set to 1000*dt, i.e. real time.
            Use shorter interval to speed up animation and longer interval to make slow motion
        """
        view = self._set_view_defaults(view, **kwargs)

        with plt.ioff():
            fig = AnimationFigure(view.figure)

        dt = dt or self.dt
        time_steps = int(np.ceil((time_stop - self.time) / dt))
        sh = self.add_step_handlers(view, t0=0, dt=dt)
        try:
            fig.animation = fig.animate(self, frames=time_steps, dt=dt, interval=interval or dt * 1000,
                                        repeat=False, verbose=verbose)
            if filename:
                fig.save(filename)
            else:  # pragma: no cover
                try:
                    from IPython import get_ipython
                    assert 'IPKernelApp' in get_ipython().config
                    fig = fig.animation.to_jshtml()
                except (ModuleNotFoundError, AttributeError, AssertionError):
                    fig.show()
        finally:
            self.step_handlers.remove(sh)
        return fig
