import numpy as np
import matplotlib.pyplot as plt
from dynamiks.utils.geometry import get_east_north_height


class View():
    plane = ""

    def __init__(self, x=None, y=None, z=None, ax=None, visualizers=None, **ax_init_kwargs):
        self.x = x
        self.y = y
        self.z = z

        self.ax = ax
        self.visualizers = visualizers
        self.ax_init_kwargs = ax_init_kwargs
        self.interactive = False
        self.visualizers_initialized = False

    def initialize_visualizers(self, flowSimulation):
        from dynamiks.visualizers.visualizer_utils import AxesInitializer
        self.ax = self.ax or plt.gca()
        if isinstance(self, (XYView, EastNorthView, XView, YView)) and self.z is None:
            self.z = flowSimulation.windTurbines.hub_height(flowSimulation.windTurbines.types).mean()
        ax_init = AxesInitializer(**{'ax': self.ax, **self.get_ax_init_kwargs(), **self.ax_init_kwargs})
        visualizers = self.visualizers or []

        self.visualizers = [ax_init]

        for visualizer in visualizers:
            self.add_visualizer(visualizer)

        self.add_visualizer(ax_init.set_lim)  # set limits again in case a visualizer has plotted outside the limits
        self.visualizers_initialized = True
        self.figure = self.ax.figure

    def __getitem__(self, s):
        return [self.x, self.y, self.z][s]

    @property
    def view_plane(self):
        return self.plane

    def get_ax_init_kwargs(self,):
        return {}

    def __call__(self, flowSimulation):
        from dynamiks.visualizers.visualizer_utils import update_figure
        if not self.visualizers_initialized:
            self.initialize_visualizers(flowSimulation)
        for visualizer in self.visualizers:
            visualizer(flowSimulation)
        if self.interactive:
            update_figure(self.ax.figure)

    def add_visualizer(self, visualizer):
        if hasattr(visualizer, 'initialize'):
            visualizer.initialize(self.ax, self)
        self.visualizers.append(visualizer)


class Points(View):
    plane = 'i'

    def __init__(self, x, y, z):
        x, y, z = [np.atleast_1d(v) for v in [x, y, z]]
        assert len(x) == len(y) == len(z)
        View.__init__(self, x, y, z)

    def get_coords(self, flowSimulation):
        return {'i': np.arange(len(self[0]))}

    def __getitem__(self, s):
        return np.array([self.x, self.y, self.z])[s]

    @property
    def XYZ(self):
        return np.array([*self])


class Grid(View):

    def __init__(self, x, y, z, adaptive, visualizers, ax, **ax_init_kwargs):
        if any([v is None for v in [x, y, z]]) and not adaptive:
            raise TypeError(
                f"Grid undefined. Please specify the grid explicitly when instantiating {self.__class__.__name__} or set adaptive to True")

        View.__init__(self, x, y, z, visualizers=visualizers, ax=ax, **ax_init_kwargs)
        self.desired_x = x
        self.desired_y = y
        self.desired_z = z
        self.adaptive = adaptive

    @property
    def shape(self):
        return np.shape(self.x) + np.shape(self.y) + np.shape(self.z)

    @property
    def XYZ(self):
        return np.reshape(np.meshgrid(self.x, self.y, self.z, indexing='ij'), (3,) + self.shape)

    def get_coords(self, flowSimulation):
        return {n: self['xyz'.index(n)] for n in self.plane}

    def get_slices(self, x, y, z, mirror_xyz):
        def get_slice(actual, xyz, mirror):
            dactual = np.round(actual[1] - actual[0], 10)
            desired = getattr(self, f'desired_{xyz}')
            if desired is None:
                desired = actual
            ddesired = desired[1] - desired[0]
            idx = np.arange(int(np.floor((desired.min() - actual[0]) / dactual)),
                            int(np.floor((desired.max() - actual[0]) / dactual) + 1),
                            np.maximum(int(np.floor(ddesired / dactual)), 1))
            axis = np.round(dactual * idx + actual[0], 10)
            N = len(actual)
            if mirror:
                N1 = N - 1
                idx = N1 - np.abs(idx % (2 * N1) - N1)
            else:
                idx = idx % N
            return idx, axis

        slices, axes_lst = [], []
        for xyz, n, mirror in zip([x, y, z], 'xyz', mirror_xyz):
            if n in self.plane:
                s, axis = get_slice(xyz, n, mirror)
                slices.append(s)
            else:
                desired = getattr(self, n)
                dactual = np.round(xyz[1] - xyz[0], 10)
                idx = np.array(np.round((desired - xyz[0]) / dactual), dtype=int)
                slices.append(idx)
                axis = dactual * np.array(idx) + xyz[0]
            axes_lst.append(axis)

        self.x, self.y, self.z = axes_lst
        return slices


class GridSlice3D(Grid):
    def __init__(self, x_slice, y_slice, z_slice, axes):
        self.x_slice = x_slice
        self.y_slice = y_slice
        self.z_slice = z_slice
        self.slices = [x_slice, y_slice, z_slice]
        self.plane = 'xyz'

        def get(ax, s):
            if isinstance(s, slice):
                return ax[s]
            else:
                x0, dx = ax[0], ax[1] - ax[0]
                assert np.allclose(np.diff(ax), dx), "axes must be equidistant"
                return s * dx + x0
        Grid.__init__(self, *[get(a, s) for a, s in zip(axes, [x_slice, y_slice, z_slice])], adaptive=True,
                      visualizers=None, ax=None)


class View2D(Grid):

    def __init__(self, x, y, z, adaptive, flowVisualizer, visualizers, ax, **ax_init_kwargs):
        from dynamiks.visualizers import Flow2DVisualizer, WindTurbineVisualizer
        if flowVisualizer is None:
            flowVisualizer = Flow2DVisualizer()
        if flowVisualizer:
            visualizers = (visualizers or []) + [flowVisualizer, WindTurbineVisualizer()]
        xyz = dict(x=x, y=y, z=z)
        ax_init_kwargs['xlim'] = (ax_init_kwargs.get('xlim', None) or
                                  xyz[self.plane[0]] is not None and xyz[self.plane[0]][[0, -1]])
        ax_init_kwargs['ylim'] = (ax_init_kwargs.get('ylim', None,) or
                                  xyz[self.plane[1]] is not None and xyz[self.plane[1]][[0, -1]])
        Grid.__init__(self, x, y, z, adaptive=adaptive, visualizers=visualizers, ax=ax, **ax_init_kwargs)

    def XY(self, *_):
        return np.meshgrid(*[getattr(self, n) for n in self.plane])

    def get_ax_init_kwargs(self):
        return dict(ax=self.ax, xlabel=f'{self.plane[0]} [m]', ylabel=f'{self.plane[1]} [m]', axis='scaled')


class View1D(Grid):
    def __init__(self, x, y, z, adaptive, flowVisualizer, visualizers, ax, **ax_init_kwargs):
        x = self.desired_x = [np.asarray(x), None][x is None]
        y = self.desired_y = [np.asarray(y), None][y is None]
        z = self.desired_z = [np.asarray(z), None][z is None]
        if flowVisualizer is None:
            from dynamiks.visualizers import Flow1DVisualizer
            flowVisualizer = Flow1DVisualizer()
        if flowVisualizer:
            visualizers = (visualizers or []) + [flowVisualizer]
        xyz = dict(x=x, y=y, z=z)
        if self.plane == 'z':
            ax_init_kwargs['ylim'] = (ax_init_kwargs.get('ylim', None) or
                                      xyz[self.plane[0]] is not None and xyz[self.plane[0]][[0, -1]])
        else:
            ax_init_kwargs['xlim'] = (ax_init_kwargs.get('xlim', None) or
                                      xyz[self.plane[0]] is not None and xyz[self.plane[0]][[0, -1]])
        Grid.__init__(self, x, y, z, adaptive=adaptive, visualizers=visualizers, ax=ax, **ax_init_kwargs)

    @property
    def X(self):
        return getattr(self, self.plane)

    def get_ax_init_kwargs(self):
        return dict(ax=self.ax, xlabel=f'{self.plane[0]} [m]', axis='auto')


class XView(View1D):
    plane = 'x'

    def __init__(self, y, z, x=None, adaptive=True, flowVisualizer=None, visualizers=None, ax=None, **ax_init_kwargs):
        View1D.__init__(self, x, y, z, adaptive=adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)


class YView(View1D):
    plane = 'y'

    def __init__(self, x, z, y=None, adaptive=True, flowVisualizer=None, visualizers=None, ax=None, **ax_init_kwargs):
        View1D.__init__(self, x, y, z, adaptive=adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)


class ZView(View1D):
    plane = 'z'

    def __init__(self, x, y, z=None, adaptive=True, flowVisualizer=None, visualizers=None, ax=None, **ax_init_kwargs):
        View1D.__init__(self, x, y, z, adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)

    def get_ax_init_kwargs(self):
        return dict(ax=self.ax, ylabel=f'{self.plane[0]} [m]', axis='auto')


class XYView(View2D):
    plane = 'xy'

    def __init__(self, z=None, x=None, y=None, adaptive=True, flowVisualizer=None,
                 visualizers=None, ax=None, **ax_init_kwargs):
        View2D.__init__(self, x, y, z, adaptive=adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)


class EastNorthView(View2D):
    plane = 'xy'
    view_plane = 'en'

    def __init__(self, z=None, x=None, y=None, adaptive=True, flowVisualizer=None,
                 visualizers=None, ax=None, **ax_init_kwargs):
        View2D.__init__(self, x, y, z, adaptive=adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)

    def XY(self, wind_direction, center_offset):
        X, Y = np.meshgrid(*[getattr(self, n) for n in self.plane])
        return get_east_north_height([X, Y, X * 0], wind_direction, center_offset)[:2]

    def get_coords(self, flowSimulation):
        XY = self.XY(flowSimulation.wind_direction, flowSimulation.center_offset)
        return {**View2D.get_coords(self, flowSimulation), 'e': (('x', 'y'), XY[0].T), 'n': (('x', 'y'), XY[1].T)}

    def get_ax_init_kwargs(self):
        return {**View2D.get_ax_init_kwargs(self), 'xlabel': 'East [m]', 'ylabel': 'North [m]'}


class XZView(View2D):
    plane = 'xz'

    def __init__(self, y, x=None, z=None, adaptive=True, flowVisualizer=None, visualizers=None,
                 ax=None, **ax_init_kwargs):
        View2D.__init__(self, x, y, z, adaptive=adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)


class YZView(View2D):
    plane = 'yz'

    def __init__(self, x, y=None, z=None, adaptive=True, flowVisualizer=None, visualizers=None,
                 ax=None, **ax_init_kwargs):
        View2D.__init__(self, x, y, z, adaptive=adaptive, flowVisualizer=flowVisualizer, visualizers=visualizers,
                        ax=ax, **ax_init_kwargs)
        self.x = x

        self.adaptive = adaptive


class XYZView(Grid):
    plane = 'xyz'

    def __init__(self, x, y, z, adaptive=True):
        Grid.__init__(self, x, y, z, adaptive, visualizers=None, ax=None)


class MultiView(View):
    def __init__(self, view_lst):
        self.view_lst = view_lst
        self.interactive = False
        self._figures = None

    @property
    def figures(self):
        return {view.ax.figure for view in self.view_lst}

    def __call__(self, flowSimulation):
        from dynamiks.visualizers.visualizer_utils import update_figure
        figures = set([])
        for view in self.view_lst:
            view(flowSimulation)
            figures.add(view.ax.figure)
        for fig in figures:
            fig.tight_layout()
            if self.interactive:
                update_figure(fig)

    @property
    def figure(self):
        assert len(self.figures) == 1, "Multiview has more than one figure"
        return list(self.figures)[0]

    def initialize_visualizers(self, flowSimulation):
        for view in self.view_lst:
            view.initialize_visualizers(flowSimulation)
