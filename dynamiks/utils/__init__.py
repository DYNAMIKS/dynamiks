

def is_notebook():
    try:
        from IPython import get_ipython
        assert 'IPKernelApp' in get_ipython().config  # pragma: no cover
        return True  # pragma: no cover
    except (ModuleNotFoundError, AttributeError, AssertionError):
        return False
