import os

from dynamiks.dwm.dwm_flow_simulation import DWMFlowSimulation
import matplotlib.pyplot as plt

# In notebook
# from dynamiks.utils import doc_utils # use cached animations in sphinx documentation

# use from notebook
# import os
# os.environ['DYNAMIKS_VISUALIZE']=["visualize","animate","reanimate","short"][1]

# from linux bash
# export DYNAMIKS_VISUALIZE=animate
# Windows cmd
# set DYNAMIKS_VISUALIZE=animate


DWMFlowSimulation._visualize = DWMFlowSimulation.visualize
DWMFlowSimulation._run = DWMFlowSimulation.run


def visualize(fs, *args, id=None, **kwargs):  # pragma: no cover
    if os.environ.get('DYNAMIKS_VISUALIZE', 'visualize') in ['visualize', 'short'] or id is None:
        fs._visualize(*args, **kwargs)
    elif os.environ['DYNAMIKS_VISUALIZE'] in ['animate', 'reanimate']:

        name = f"cached_{id}.gif"
        if not os.path.isfile(name) or os.environ['DYNAMIKS_VISUALIZE'] == 'reanimate':
            fs.animate(*args, **kwargs, filename=name)
        plt.close()
        from IPython.display import HTML
        return HTML(f'<img src="{name}">')
    else:
        raise ValueError(
            f"os.environ['DYNAMIKS_VISUALIZE']={os.environ['DYNAMIKS_VISUALIZE']} must be one of ['visualize','animate','reanimate']")


def run(self, time_stop, verbose=False):  # pragma: no cover
    if os.environ.get('DYNAMIKS_VISUALIZE', 'visualize') == 'short':
        self._run(self.time + 2 * self.dt, verbose=verbose)
    else:
        self._run(time_stop, verbose=verbose)


DWMFlowSimulation.visualize = visualize
DWMFlowSimulation.run = run
