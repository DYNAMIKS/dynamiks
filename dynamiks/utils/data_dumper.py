import numpy as np
import xarray as xr
from numpy import newaxis as na
from dynamiks.visualizers._visualizers import DataDumperVisualizer
from dynamiks.views import View


class DataDumper:

    def __init__(self, data_dumper_function, coords={}, time_step_interval=None):
        self.data_dumper_function = data_dumper_function
        self.time_step_interval = time_step_interval
        self.coords = coords
        self.data = []
        self.time = []
        self.time_step = 0

    def __call__(self, flowSimulation):
        if (self.time_step_interval is None or self.time_step % self.time_step_interval == 0):
            self.data.append(self.data_dumper_function(flowSimulation))
            self.time.append(flowSimulation.time)
        self.time_step += 1

    def to_xarray(self):
        data = np.array(self.data)
        dims = ['time'] + [k for k, v in self.coords.items() if not isinstance(v, tuple)]
        dims.extend([f'dim_{i}' for i in range(len(dims), len(data.shape))])
        return xr.DataArray(data, dims=dims, coords={'time': self.time, **self.coords})

    def get_visualizer(self):
        return DataDumperVisualizer(self)

    def get_view(self, ax=None, **ax_init_kwargs):
        return View(visualizers=[self.get_visualizer()], ax=ax, **ax_init_kwargs)
