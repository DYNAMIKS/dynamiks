import os
import numpy
import dynamiks
from py_wake.deficit_models.gaussian import NiayifarGaussianDeficit
from py_wake.examples.data.hornsrev1 import V80
from dynamiks.dwm.dwm_flow_simulation import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.pywake_deficit_wrapper import PyWakeDeficitGenerator
from dynamiks.sites._site import TurbulenceFieldSite, UniformSite
from dynamiks.sites.turbulence_fields import RandomTurbulence, MannTurbulenceField
from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines
from dynamiks.dwm.particle_motion_models import ParticleMotionModel
from dynamiks.dwm.particles_model import WindTurbinesParticles
from dynamiks.sites.mean_wind import MeanWind
from dynamiks.flow_simulation import FlowSimulation
import numpy as np
from numpy import newaxis as na

tfp = os.path.abspath(os.path.dirname(dynamiks.__file__).replace("\\", "/") + '/../tests/test_files') + "/"
if not os.path.exists(tfp):  # pragma: no cover
    # on binder, the tests folder is some levels up relative to the notebook
    for i in range(4):
        f = "../" * i + "tests/test_files/"
        if os.path.exists(f + "mann_turb"):
            tfp = f
            break
npt = numpy.testing


def DemoWindTurbines(x=[0], y=[0], z=0, windTurbine=V80(), rotorAvgModel=None):
    return PyWakeWindTurbines(x=x, y=y, z=z, windTurbine=windTurbine, rotorAvgModel=rotorAvgModel)


def DemoSite(ws, ti):
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_3.200x3.20x3.20_s0001.nc",
    )
    if isinstance(ws, MeanWind):
        _ws = ws.ws
    else:
        _ws = ws
    turbfield.scale_TI(TI=ti, U=_ws)
    return TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))


class DefaultDWMFlowSimulation(DWMFlowSimulation):
    def __init__(self, x=[0], y=[0], z=0, ws=10, ti=.1, wind_direction=270, site='mann', windTurbines=None,
                 wakeDeficitModel=None, addedTurbulenceModel=None, dt=1, fc=.1, d_particle=2, n_particles=10, step_handlers=[],
                 particleMotionModel=None, windTurbineParticles_cls=WindTurbinesParticles):

        if site == 'random':
            site = TurbulenceFieldSite(ws=10, turbulenceField=RandomTurbulence(ti=ti, ws=ws))
        elif site == 'mann':
            site = DemoSite(ws, ti)
        elif site == 'uniform':
            site = UniformSite(ws=ws, ti=ti)

        windTurbines = windTurbines or DemoWindTurbines(x=x, y=y, z=z)
        wakeDeficitModel = wakeDeficitModel or PyWakeDeficitGenerator(
            deficitModel=NiayifarGaussianDeficit(use_effective_ws=True), scale_with_freestream=False)
        particleMotionModel = particleMotionModel or ParticleMotionModel(temporal_filter=fc)

        DWMFlowSimulation.__init__(self, site=site, windTurbines=windTurbines, dt=dt, wind_direction=wind_direction,
                                   step_handlers=step_handlers,
                                   n_particles=n_particles, d_particle=d_particle,
                                   particleDeficitGenerator=wakeDeficitModel,
                                   particleMotionModel=particleMotionModel,
                                   addedTurbulenceModel=addedTurbulenceModel,
                                   windTurbinesParticles=windTurbineParticles_cls,
                                   )


class EllipsysMockFlowSimulation(FlowSimulation):
    def __init__(self, site, windTurbines, dt, wind_direction=270, step_handlers=None, update_flowfield=True):
        FlowSimulation.__init__(self, site, windTurbines, dt, wind_direction=wind_direction,
                                step_handlers=step_handlers)
        self.step_handlers.insert(0, self.step_ellipsys)
        # ws, ti = 10, .1
        # turbfield = MannTurbulenceField.from_netcdf(
        #     tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_9.600x9.60x9.60_s0001.nc")
        # turbfield.scale_TI(TI=ti, U=ws)
        # self.el = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))
        self.el = site
        if hasattr(self.el, 'turbulenceField'):
            self.el.turbulenceField.time_offset = 0, np.array(self.el.turbulence_offset, dtype=float)
            self.el.turbulenceField.site = self.el
        self.update_flowfield = update_flowfield

    def update_wind_turbine_sensors(self, flowSimulation):
        ""

    def set_aerosection_forces(self, rotor_pos_xi, sec_rel_pos_xibs, f_xibs):
        sec_pos_xibs = sec_rel_pos_xibs + rotor_pos_xi[:, :, na, na]
        uvw_shape = self.el.turbulenceField.uvw.shape[1:]

        def get(s, i):
            if isinstance(s, slice):
                return s
            # else:
            #     return s % uvw_shape[i]
        for p_x, f_x in zip((sec_pos_xibs).reshape((3, -1)).T, f_xibs.reshape((3, -1)).T):
            s1, s2, s3 = [get(self.el.turbulenceField.get_slice(p, i, self.time), i) for i, p in enumerate(p_x)]
            self.el.turbulenceField.uvw[0, s1, s2, s3] -= (f_x[0] / 500000)

    def _get_windspeed(self, view, include_wakes=True, exclude_wake_from=[], time=None):
        assert include_wakes, f"{self.__class__.__name__} cannot extract wind speed without wakes"
        return self.el.get_windspeed(view)

    def step_ellipsys(self, _):
        try:
            rotor_pos_xi, sec_rel_pos_xibs = self.windTurbines.get_aerosection_positions()
            f_xibs = self.windTurbines.get_aerosection_forces()
            if self.update_flowfield:
                self.set_aerosection_forces(rotor_pos_xi, sec_rel_pos_xibs, f_xibs)
        except ChildProcessError:  # pragma: no cover
            pass
