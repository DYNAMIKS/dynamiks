from py_wake.deficit_models.gaussian import NiayifarGaussianDeficit
from py_wake.examples.data.hornsrev1 import V80
from dynamiks.dwm.particle_deficit_profiles.pywake_deficit_wrapper import PyWakeDeficitGenerator
from dynamiks.sites._site import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField
from dynamiks.utils.test_utils import tfp, DefaultDWMFlowSimulation, npt
from dynamiks.views import XYView, XZView, YView, YZView, XView, Points, EastNorthView, MultiView
from dynamiks.visualizers.flow_visualizers import Flow2DVisualizer, Flow, Flow1DVisualizer
from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines
import matplotlib.pyplot as plt
import numpy as np
import pytest
from pathlib import Path
import matplotlib
from dynamiks.visualizers import ParticleVisualizer, WindDirectionVisualizer


def test_flow_visualizer():
    if matplotlib.get_backend() == 'agg':
        pytest.xfail("agg backend")

    ws = 10
    ti = .06
    x, y = [0, 400], [0, 0]

    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_3.200x3.20x3.20_s0001.nc")
    turbfield.scale_TI(TI=ti, U=ws)
    turbfield.uvw[:, :3] = 5
    turbfield.uvw[:, :, :3] = 5
    turbfield.uvw[:, :, :, :3] = 5
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))

    wt = PyWakeWindTurbines(x, y, V80())
    deficit_model = NiayifarGaussianDeficit()

    fig = plt.figure(figsize=(10, 8))

    ax1, ax2, ax3, ax4, ax5 = fig.subplots(5)

    view_lst = [EastNorthView(z=70, ax=ax1,
                              flowVisualizer=Flow2DVisualizer(uvw='uv', include_wakes=True,
                                                              levels=np.linspace(0, 16, 22)),
                              visualizers=[lambda _: ax1.axvline(200, color='k', ls='--'),
                                           ParticleVisualizer(True, velocity=True),
                                           WindDirectionVisualizer(-1000, 0, 70, 10)]),
                XZView(y=0, ax=ax2, flowVisualizer=Flow2DVisualizer(uvw='u', include_wakes=True),
                       xlim=[None, 1000], ylim=[0, 150],
                       visualizers=[lambda _: ax2.axvline(200, color='k', ls='--')]),
                YZView(x=200, ax=ax3, flowVisualizer=Flow2DVisualizer(uvw='u', include_wakes=True),
                       title='x=200', xlabel='My xlabel', ylabel='My ylabel'),
                XView(y=0, z=70, ax=ax4, flowVisualizer=Flow1DVisualizer(uvw='u', include_wakes=True)),
                YView(x=200, z=70, ax=ax5, flowVisualizer=Flow1DVisualizer(uvw='u', include_wakes=True))
                ]

    fs = DefaultDWMFlowSimulation(x, y, ws=ws, ti=ti, site=site, windTurbines=wt,
                                  wakeDeficitModel=PyWakeDeficitGenerator(deficitModel=deficit_model),
                                  dt=1, fc=0.1, d_particle=2, n_particles=10)
    # fs.add_step_handlers(step_handlers, t0=50, dt=None)
    fs.run(50)
    for view in view_lst:
        fs.show(view, block=False)
    fs.show(MultiView(view_lst), block=False)

    if 0:
        #fs.visualize(153, MultiView(view_lst))
        plt.show()
    plt.close('all')


def test_flow():
    fs = DefaultDWMFlowSimulation(site='uniform')

    def mean_wsp(xyz, uvw, time):
        uvw[0] = 3
        uvw[1] = 4
        return uvw

    fs.site.add_mean_windspeed = mean_wsp
    for uvw, ref in [('u', 3), ('v', 4), ('uv', 5)]:
        npt.assert_array_equal(Flow(uvw=uvw, include_wakes=False)(fs, Points([0], [0], [0])), ref)


def test_flowsimulation_show_visualize_animate():
    if matplotlib.get_backend() == 'agg':
        pytest.xfail("agg backend")
    x, y = [0, 400], [0, 0]
    fs = DefaultDWMFlowSimulation(x=x, y=y, wind_direction=0)

    fs.run(10)
    debug = False
    for ext in ['.gif', '.avi']:
        f = Path(tfp + f'tmp{ext}')
        f.unlink(missing_ok=True)
        fs.animate(fs.time + 12, dt=3, interval=1000, filename=f, verbose=False)
        assert f.exists()
    if debug:
        plt.show()


def test_WindDirectionVisualizer():
    if matplotlib.get_backend() == 'agg':
        pytest.xfail("agg backend")
    x, y = [0, 400], [0, 0]
    fs = DefaultDWMFlowSimulation(x=x, y=y, wind_direction=0, ws=1)
    debug = 0
    lims = {'xlim': [-400, 600], 'ylim': [-300, 300]}

    axes = plt.subplots(3, 2)[1]
    args = [0, 100, 350], [0, 100, 150], [70, 70, 70], 80
    for wd, ax_row in zip([240, 270, 300], axes):
        fs.wind_direction = wd
        fs.show(MultiView([XYView(70, visualizers=[WindDirectionVisualizer(*args), ParticleVisualizer()], **lims, ax=ax_row[0]),
                          EastNorthView(70, visualizers=[WindDirectionVisualizer(*args), ParticleVisualizer()], **lims, ax=ax_row[1])]), block=0)
    if debug:
        plt.show()
