import pytest

from dynamiks.sites._site import TurbulenceFieldSite, Site, UniformSite, MetmastSite, NoTurbulenceField
from dynamiks.sites.turbulence_fields import RandomTurbulence, MannTurbulenceField
from dynamiks.utils.test_utils import npt, tfp, DefaultDWMFlowSimulation, DemoSite
from dynamiks.views import XYView, YZView, GridSlice3D, Points, XZView, ZView
from dynamiks.visualizers.flow_visualizers import Flow2DVisualizer
import matplotlib.pyplot as plt
import numpy as np
from py_wake.site.shear import PowerShear, LogShear
from dynamiks.sites.mean_wind import ConstantWindSpeed
from dynamiks.dwm.particle_motion_models import ParticleMotionModel
from dynamiks.utils.geometry import get_east_north_height
from dynamiks.utils.data_dumper import DataDumper
from py_wake.utils.plotting import setup_plot


def test_site():
    ti = 0.08
    site = TurbulenceFieldSite(ws=10, turbulenceField=RandomTurbulence(ws=10, ti=ti))
    uvw = site.get_windspeed(Points([0, 0, 0, 0], [0, 0, 0, 0], [0, 10, 20, 30]))

    npt.assert_array_almost_equal(uvw, [[8.86094, 11.010983, 9.303471, 9.792661],
                                        [-0.04822, -0.474166, -0.875387, 0.415291],
                                        [0.144423, -0.781145, 0.938964, 0.387399]])
    x = np.zeros(10000)
    uvw = site.get_windspeed(Points(x, x, x))
    npt.assert_array_almost_equal(np.mean(uvw, 1), [10, 0, 0], 2)
    npt.assert_array_almost_equal(np.std(uvw, 1), [.8, .64, .4], 2)
    with pytest.raises(AssertionError):
        site.get_windspeed([0, 0, 70])


def test_uniformSite():
    site = UniformSite(8, .06)
    xyz = Points([0, 100], [0, 200], [0, 50])
    npt.assert_array_equal(site.get_turbulence_intensity(xyz), 0.06)
    u, v, w = site.get_windspeed(xyz)
    npt.assert_array_equal(u, 8)
    npt.assert_array_equal(v, 0)
    npt.assert_array_equal(w, 0)

    with pytest.raises(TypeError, match='Grid undefined. Please specify the grid explicitly when instantiating XYView'):
        site.get_windspeed(XYView(z=100))


def test_turbulenceFieldSite():
    ws = 8
    tf = MannTurbulenceField.from_netcdf(
        tfp + 'mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_256x8x8_1.000x1.00x1.00_s0001.nc')
    site = TurbulenceFieldSite(ws=ws, turbulenceField=tf)
    site.turbulence_transport_speed = ws

    # time = -np.arange(0, 25, .1)
    # u = np.array([site.get_windspeed(Points(0, 5, 3.5), t)[0] for t in time]).squeeze()
    #
    # x = -time * ws
    # u_ref = tf.to_xarray().sel(uvw='u').interp(x=x, y=5, z=3.5) + ws
    # if 1:
    #     plt.plot(time, u)
    #     plt.plot(time, u_ref, ':')
    #     plt.show()
    # npt.assert_array_almost_equal(u, u_ref)
    da = tf.to_xarray()
    u, v, w = site.get_windspeed(YZView(x=0, y=da.y.values, z=da.z.values, adaptive=False))
    npt.assert_array_almost_equal(da.sel(x=0, uvw='u').values + ws, u.squeeze())
    npt.assert_array_almost_equal(da.sel(x=0, uvw='v').values, v.squeeze())
    npt.assert_array_almost_equal(da.sel(x=0, uvw='w').values, w.squeeze())

    u, v, w = site.get_windspeed(YZView(x=0, adaptive=True))
    npt.assert_array_almost_equal(da.sel(x=0, uvw='u').values + ws, u.squeeze())
    npt.assert_array_almost_equal(da.sel(x=0, uvw='v').values, v.squeeze())
    npt.assert_array_almost_equal(da.sel(x=0, uvw='w').values, w.squeeze())


def test_adaptive_incompatibility():
    fs = DefaultDWMFlowSimulation(site='random')

    with pytest.raises(TypeError, match='Grid undefined. Please specify the grid explicitly when instantiating XYView'):
        fs.show(view=XYView(z=70))

    with pytest.raises(TypeError, match='Grid undefined. Please specify the grid explicitly when instantiating XYView or set adaptive to True'):
        XYView(z=70, adaptive=False)

    fs.site.turbulenceField.x = np.arange(10)
    with pytest.raises(TypeError, match='Grid undefined. Please specify the grid explicitly or set adaptive to True when instantiating XYView'):
        fs.show(view=XYView(z=70))

    fs.site.turbulenceField.y = np.arange(10)
    fs.site.turbulenceField.z = np.arange(10)
    with pytest.raises(TypeError, match='Grid undefined. Please specify the grid explicitly when instantiating XYView or set adaptive to True'):
        XYView(z=70, adaptive=False)


@pytest.mark.parametrize('shear_cls,f', [  # (PowerShear, lambda z, U:(z / 100)**0.1 * U),
    (LogShear, lambda z, U: np.log(z / .03) / np.log(100 / .03) * U)
])
def test_shear(shear_cls, f):
    U = 10

    mannturbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_3.200x3.20x3.20_s0001.nc")
    mannturbfield.scale_TI(TI=0, U=U)
    mannturbfield.time_offset = (0, [-2500, -100, 20])
    for turbfield in [mannturbfield, RandomTurbulence(ti=0, ws=10)]:
        site = TurbulenceFieldSite(ws=ConstantWindSpeed(U, shear=shear_cls()), turbulenceField=turbfield,
                                   turbulence_offset=(-2500, -100, 20))
        if hasattr(turbfield, 'get_axes'):
            z = turbfield.get_axes(0)[2]
        else:
            z = np.linspace(0, 100)[1:]
        for grid in [ZView(0, 0, z, adaptive=False),
                     Points(x=z * 0, y=z * 0, z=z),
                     GridSlice3D(slice(1), slice(1), slice(None), axes=[[0], [0], z])]:
            uvw = site.get_windspeed(grid)
            if 0:
                plt.plot(uvw[0], z)
                plt.show()
            npt.assert_array_almost_equal(uvw[0].squeeze(), f(z, U))


def test_shear_int():
    site = DemoSite(ws=ConstantWindSpeed(ws=10, shear=PowerShear()), ti=.1)
    fs = DefaultDWMFlowSimulation(site=site)
    npt.assert_allclose(fs.get_windspeed(XYView(z=70), False)[0].mean(),
                        (70 / 100)**.1 * 10 + 0.12, atol=.03)  # 0.12 offset due to non-zero mean turb at level


def test_MetmastSite():
    from py_wake.examples.data import example_data_path

    d = np.load(example_data_path + "/time_series.npz")
    dt = 600  # seconds between samples
    start = int(3600 / dt * 24 * 23)  # start with day number 21.5
    wd, ws, ws_std = [d[k][start:start + 6 * 12 + 1] for k in ['wd', 'ws', 'ws_std']]
    #ti = np.minimum(ws_std/ws,.5)
    t = np.arange(len(wd)) * dt

    site = MetmastSite(ws=10, turbulenceField=NoTurbulenceField(ti=.05), wd_lst=wd, dt=dt,
                       max_wd_step=0.02, update_interval=5)

    def get_wd(flowSimulation):
        e, n, h = get_east_north_height(fs.get_windspeed([0, 0, 0], include_wakes=False)[
                                        :, 0], fs.wind_direction, [0, 0, 0])
        return (270 - np.rad2deg(np.arctan2(n, e))) % 360

    wd_dumper = DataDumper(get_wd)  # Instant wind direction relative to the North
    wd_slow_dumper = DataDumper(lambda fs: fs.wind_direction)  # Turbulence transport direction
    # Instant wind direction relative to turbulence transport direction
    wd_small_dumper = DataDumper(lambda fs: get_wd(fs) - fs.wind_direction)

    fs = DefaultDWMFlowSimulation(x=[-200, 200], y=[0, 0], site=site,
                                  particleMotionModel=ParticleMotionModel(temporal_filter=None),
                                  d_particle=0.5, n_particles=20,
                                  dt=300,  # very high dt, only for demo purposes
                                  step_handlers=[wd_dumper, wd_slow_dumper, wd_small_dumper])
    wd_dumper(fs)
    fs.run(3600 * 12, verbose=1)
    if 0:
        plt.plot(t, wd, label='metmast wd')
        wd_dumper.to_xarray().plot(label='simulated wd')
        wd_slow_dumper.to_xarray().plot(label='transport direction')
        plt.plot(wd_small_dumper.time, np.unwrap(wd_small_dumper.data, period=360), label='relative wd')
        setup_plot()
        plt.show()
    npt.assert_array_almost_equal(wd, np.interp(t, wd_dumper.time, wd_dumper.data))  # compare wd with prescribed
    assert np.abs(np.diff(wd_slow_dumper.data)).max() / 300 * 5 < 0.02 + 1e-10
    assert np.abs(np.unwrap(wd_small_dumper.data, period=360)).max() < 42
