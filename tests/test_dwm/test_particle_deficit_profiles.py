from py_wake.tests.test_files import tfp
from dynamiks.utils.data_dumper import DataDumper
from dynamiks.dwm.dwm_flow_simulation import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.pywake_deficit_wrapper import PyWakeDeficitGenerator
from dynamiks.sites._site import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import RandomTurbulence
from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines
import matplotlib.pyplot as plt
import numpy as np
from py_wake.deficit_models.fuga import FugaDeficit
from py_wake.deficit_models.gaussian import BastankhahGaussianDeficit, NiayifarGaussianDeficit
from py_wake.examples.data.hornsrev1 import V80
from dynamiks.views import XYView, YView, Points, MultiView, View
from dynamiks.utils.test_utils import npt, DefaultDWMFlowSimulation
from dynamiks.dwm.added_turbulence_models import AutoScalingIsotropicMannTurbulence
import pytest
from dynamiks.dwm.particle_motion_models import ParticleMotionModel
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator
from dynamiks.visualizers._visualizers import ParticleVisualizer
from py_wake.deficit_models.noj import NOJDeficit
from jDWM import EddyViscosityModel
import warnings


def test_pywake_wrapper():

    ws = 10
    x, y = [0, 0, 0, 100], [0, 300, 200, 50]
    turbfield = RandomTurbulence(ti=0, ws=10)
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield)

    wt = PyWakeWindTurbines(x, y, V80(), rotorAvgModel=None)
    deficit_model = FugaDeficit()

    ref = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.937, 0.937, 2.467, 2.479, 2.428, 2.37, 2.345, 2.335, 2.323,
           2.334, 2.336, 2.325]
    tsd = DataDumper(data_dumper_function=lambda fs: fs.get_deficit(Points(500, 0, 70))[0], coords={})

    fs = DWMFlowSimulation(site, windTurbines=wt, dt=1,
                           particleDeficitGenerator=PyWakeDeficitGenerator(deficitModel=deficit_model),
                           d_particle=2,
                           n_particles=10,
                           addedTurbulenceModel=None,
                           particleMotionModel=ParticleMotionModel(temporal_filter=0.1),
                           step_handlers=[tsd]
                           )

    if 0:
        py = np.linspace(-200, 400, 1000)

        ax1, ax2, ax3 = plt.subplots(1, 3, figsize=(12, 4))[1]

        y_lst = np.arange(-200, 400, 10)

        def draw1(fs):
            ax1.plot(y_lst * 0 + 500, y_lst, '--k')
            ax1.plot(500, 0, '.k')

        def draw3(fs):
            ax3.plot(tsd.time, tsd.data)
            ax3.plot(np.arange(1, 97, 5), ref, 'xk')

        view = MultiView([
            XYView(x=np.linspace(-200, 800), y=np.linspace(-100, 400), z=None, ax=ax1,
                   xlim=[-100, 800], visualizers=[ParticleVisualizer(True), draw1]),
            YView(x=500, z=70, y=y_lst, ax=ax2, ylim=[5, 11]),
            View(ax=ax3, visualizers=[draw3])
        ])
        fs.visualize(100, view=view, verbose=False)
    fs.run(100, verbose=False)
    print(list(np.round(np.squeeze(tsd.data[::5]), 3)))

    npt.assert_array_almost_equal(np.squeeze(tsd.data[::5]), ref, 3)


@pytest.mark.parametrize("l,deficit_generator,wake_width_ref,ref", [
    ('Fuga', PyWakeDeficitGenerator(deficitModel=FugaDeficit(tfp + 'fuga/2MW/Z0=0.03000000Zi=00401Zeta0=0.00E+00.nc')), 190,
     [-0.05, -0.04, -0.02, 0.06, 0.35, 0.96, 1.82, 2.51, 2.55, 1.9, 1.1, 0.81, 1.15, 1.6, 1.75, 1.73, 1.75, 1.58, 1.05, 0.46]),
    ('Bastankhah', PyWakeDeficitGenerator(deficitModel=BastankhahGaussianDeficit()), 148,
     [0.0, 0.0, 0.0, 0.03, 0.26, 1.14, 2.84, 4.49, 5.01, 3.45, 1.28, 0.65, 1.58, 2.78, 2.68, 2.19, 2.7, 2.76, 1.52, 0.43]),
    ('Niayifar', PyWakeDeficitGenerator(deficitModel=NiayifarGaussianDeficit()), 184,
     [0.0, 0.0, 0.01, 0.09, 0.36, 1.03, 2.06, 3.08, 3.4, 2.55, 1.32, 0.84, 1.24, 1.79, 1.92, 1.84, 1.92, 1.77, 1.16, 0.5]),
    ('ainslie', jDWMAinslieGenerator(), 120,
     [0.0, 0.0, 0.0, 0.0, 0.06, 1.19, 3.89, 6.24, 6.23, 3.91, 1.03, 0.24, 2.07, 4.28, 3.79, 2.35, 3.84, 4.25, 1.98, 0.2]),
    ('ainslie', jDWMAinslieGenerator(scale_with_freestream=True), 120,
     [0.0, 0.0, 0.0, 0.0, 0.06, 1.18, 3.91, 6.57, 6.92, 4.52, 1.19, 0.25, 2.07, 4.28, 3.79, 2.34, 3.84, 4.25, 1.98, 0.2]),
    ('noj', PyWakeDeficitGenerator(deficitModel=NOJDeficit(rotorAvgModel=None)), 184,
     [0.0, 0.0, 0.0, 0.0, 0.91, 1.1, 2.55, 2.55, 2.55, 2.55, 1.44, 1.37, 1.1, 1.1, 2.02, 2.21, 2.02, 1.1, 1.1, 1.1])])
def test_profiles(l, deficit_generator, wake_width_ref, ref):
    ws = 10
    ti = .12
    x, y = [0, 0, 0, 100], [0, 300, 200, 50]

    step_handlers = []
    py = np.linspace(-200, 400, 1000)
    px = py * 0 + 500
    pz = py * 0 + 70

    fs = DefaultDWMFlowSimulation(x, y, ws=ws, ti=ti, site='uniform', wakeDeficitModel=deficit_generator,
                                  step_handlers=step_handlers)
    fs.run(100)

    deficit = fs.get_deficit(Points(px, py, pz))[0]
    wake_width = np.array([fs.windTurbinesParticles[i].get_wake_radius(fs.particle_position_xip[0, i])[0] * 2
                           for i in range(4)])
    ax = plt.gca()
    if 0:
        fs.show(view=XYView(x=np.linspace(-100, 800, 500), y=np.linspace(-200, 500, 500), adaptive=False, z=70,
                            visualizers=[ParticleVisualizer()]), block=False)
        plt.sca(ax)
        c = plt.plot(deficit * 50 + px, py, label=l)[0].get_color()
        plt.plot(deficit[::50] * 50 + px[0], py[::50], '.', color=c)
        plt.plot(np.array(ref) * 50 + px[0], py[::50], 'x', label='ref')
        plt.axvline(px[0], color='k', ls=':')
        for i, c in enumerate(['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']):
            plt.plot(fs.particle_position_xip[0, i], fs.particle_position_xip[1, i] - wake_width[i] / 2, '_', color=c)
            plt.plot(fs.particle_position_xip[0, i], fs.particle_position_xip[1, i] + wake_width[i] / 2, '_', color=c)

        x, y = fs.particle_position_xip[:2, 1, 7]
        plt.plot(x, y - wake_width[1, 7] / 2, '_k')
        plt.plot(x, y + wake_width[1, 7] / 2, '_k')

        plt.legend()
        plt.show()
        # print(list(np.round(deficit[::50], 2)))
        print(wake_width[1, 7])

    npt.assert_array_almost_equal(ref, deficit[::50], 2)
    npt.assert_allclose(wake_width_ref, wake_width[1, 7], atol=0.5)


def test_ainslie():
    fs = DefaultDWMFlowSimulation(
        x=[0, 400], y=[0, 0],
        dt=0.01,
        wakeDeficitModel=jDWMAinslieGenerator(),
        addedTurbulenceModel=AutoScalingIsotropicMannTurbulence(),

    )
    # new boundary particle 1m downstream of wt1
    fs.windTurbines.rotor_positions_east_north[0, 1] = 401
    fs.d_particle = 0
    fs.step()
    fs.windTurbines.rotor_positions_east_north[0, 1] = 400
    fs.d_particle = 160
    # failed previously when trying to calculate deficit profile upstream of boundary particle initial position
    fs.run(20)


def test_ainslie_viscosity_model():
    for evm, w300_ref, l in [
        (EddyViscosityModel.madsen, 63.3, 'madsen(k1=.07,k2=0.008'),
        (EddyViscosityModel.madsen(TI=1, k1=.1), 55.3, 'madsen(k1=.1)'),
        (EddyViscosityModel.madsen(TI=1, k2=.016), 61.5, 'madsen(k2=.016)'),
        (EddyViscosityModel.larsen, 68.7, 'larsen(kamb=.1,k2=0.008'),
        (EddyViscosityModel.larsen(TI=1, kamb=.2), 54.3, 'larsen(kamb=.2)'),
        (EddyViscosityModel.larsen(TI=1, k2=.016), 66.7, 'larsen(k2=0.016)'),
        (EddyViscosityModel.keck, 49.2, 'keck(k1=0.0914,k2=0.0216)'),
        (EddyViscosityModel.keck(TI=1, k1=.15), 32.0, 'keck(k1=.15)'),
        (EddyViscosityModel.keck(TI=1, k2=.04), 40.6, 'keck(k2=0.04)'),
    ]:
        fs = DefaultDWMFlowSimulation(
            x=[0], y=[0],
            dt=0.1,
            site='uniform',
            wakeDeficitModel=jDWMAinslieGenerator(viscosity_model=evm),
        )
        fs.run(50)
        if 0:
            ax = plt.gca()
            fs.show(MultiView([YView(x=50, z=70, y=np.linspace(-100, 100), ax=ax),
                               YView(x=150, z=70, y=np.linspace(-100, 100), ax=ax, clear=False),
                               YView(x=300, z=70, y=np.linspace(-100, 100), ax=ax, clear=False),
                               ]))

        def get_width(x, ws=6):
            y_lst = np.linspace(-100, 0, 1000)
            u = fs.get_windspeed(YView(x=x, z=70, y=y_lst), include_wakes=True)[0]
            return -2 * y_lst[np.searchsorted(-u, -ws)]

        x_lst = np.linspace(10, 300)
        plt.plot(x_lst, [get_width(x) for x in x_lst], label=l)
        w300 = get_width(300)
        print(np.round(w300, 1))
        npt.assert_allclose(w300_ref, w300, atol=.1)
    if 0:
        plt.legend()
        plt.show()
    else:
        plt.close('all')


def test_unstable():
    with pytest.warns(RuntimeWarning, match='The implicit solver is probably unstable for the current dr and dx'):
        gen = jDWMAinslieGenerator(dx=0.03)
        np.random.seed(0)

        a = np.random.random(size=51)
        pf = gen.new_particle_deficit(particle_position=[0, 0, 70], ip=(0, 0), u_scale=1, a=a, TI=.1, D=80, CT=None)
        y_lst = np.linspace(-200, 200)
        z_lst = y_lst * 0

        if 0:
            for x in np.linspace(0, 1000, 5):
                try:
                    plt.plot(y_lst, pf._get_profile_norm(z_lst + x, y_lst, z_lst), label=x)
                except BaseException:
                    pass

            plt.legend()
            plt.show()
        deficit = pf._get_profile_norm(z_lst + 1000, y_lst, z_lst)
        assert np.abs(deficit).max() > 1


def test_stable():
    with warnings.catch_warnings():
        warnings.simplefilter("error")
        dr = 3 / 50
        dx = dr**2 * 50  # https://doi.org/10.5194/wes-8-1387-2023 recommends dx>dr^2*25, and dr^2*25 cause error for some seeds
        gen = jDWMAinslieGenerator(dx=dx)
    y_lst = np.linspace(-200, 200)
    z_lst = y_lst * 0
    for s in range(50):
        np.random.seed(s)
        a = np.random.random(size=51)
        pf = gen.new_particle_deficit(particle_position=[0, 0, 70], ip=(0, 0), u_scale=1, a=a, TI=.1, D=80, CT=None)

        if 0:
            for x in np.linspace(0, 1000, 5):
                plt.plot(y_lst, pf._get_profile_norm(z_lst + x, y_lst, z_lst), label=x)
            plt.legend()
            plt.show()
        deficit = pf._get_profile_norm(z_lst + 1000, y_lst, z_lst)
        assert np.abs(deficit).max() <= 1


if __name__ == '__main__':
    test_profiles()
