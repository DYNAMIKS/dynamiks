from dynamiks.dwm.particle_deficit_profiles.pywake_deficit_wrapper import PyWakeDeficitGenerator
from dynamiks.dwm.particle_motion_models import HillVortexParticleMotion, CutOffFrqLarsen2008, ParticleMotionModel,\
    XSpeed, FirstOrderLowPass, SOSFilter, PyWakeDeflectionModel
from dynamiks.utils.test_utils import DefaultDWMFlowSimulation, npt, DemoWindTurbines
from dynamiks.views import XYView, XZView, YZView, XView, ZView, Points, MultiView
import matplotlib.pyplot as plt
import numpy as np
from py_wake.deficit_models.gaussian import BastankhahGaussianDeficit
from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator
import pytest
from dynamiks.utils.data_dumper import DataDumper
from scipy import signal
import re
from py_wake.rotor_avg_models.rotor_avg_model import CGIRotorAvg, EqGridRotorAvg
from py_wake.utils.plotting import setup_plot
from dynamiks.visualizers._visualizers import ParticleVisualizer
from dynamiks.sites._site import UniformSite
from py_wake.wind_farm_models.engineering_models import PropagateDownwind
from py_wake.deflection_models.jimenez import JimenezWakeDeflection
from py_wake.deflection_models.gcl_hill_vortex import GCLHillDeflection
from dynamiks.visualizers.flow_visualizers import Flow2DVisualizer


def get_damping(x, y, fs):
    n = int(len(x) / 100)
    _, Pxx_den = signal.welch(x, fs, nperseg=n)
    f, Pyy_den = signal.welch(y, fs, nperseg=n)
    return f, np.log10(Pyy_den / Pxx_den) * 10


def plot_filter_damping(order, cutoff_frq, fs):
    sos = signal.butter(order, cutoff_frq, 'low', fs=fs, output='sos')
    f, h = signal.sosfreqz(sos, fs=fs, worN=4096)
    plt.semilogx(f, 20 * np.log10(abs(h)))
    plt.semilogx([cutoff_frq, cutoff_frq * 100], [0, -40 * order], label='20*order dB/decade')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Damping [dB]')
    plt.grid(which='both')
    plt.axvline(cutoff_frq, color='green')
    plt.plot(cutoff_frq, -3, '.r', label='-3db at cutoff')
    plt.ylim([-50, 5])
    plt.legend()


def get_filter_damping(sos, fs):
    f, h = signal.sosfreqz(sos, fs=fs, worN=4096)
    return f, 20 * np.log10(abs(h))


@pytest.mark.parametrize('Filter', [FirstOrderLowPass, SOSFilter])
def test_first_order_filter(Filter):

    cutoff_frq = 2
    order = 1

    # make signal
    fs = 500
    t = np.arange(50000) / fs
    sig = np.random.normal(scale=1, size=len(t))

    if 0:
        # add two hamonics
        add_frq = np.array([.25, 4]) * cutoff_frq
        for f in add_frq:
            sig += np.sin(2 * np.pi * f * t)

        n = int(np.round(t.size / 100))
        f, Pxx_den = signal.welch(sig, fs, nperseg=n)

        if 0:
            # plot signal
            plt.semilogy(f, Pxx_den)
            plt.xlabel('frequency [Hz]')
            plt.ylabel('PSD [V**2/Hz]')
            plt.xlim([0, cutoff_frq * 20])
            for f in add_frq:
                plt.axvline(f, color='k')
            plt.grid()
            plt.show()

        npt.assert_array_equal(f[np.where(Pxx_den > .9 * Pxx_den.max())], add_frq)

    sos = signal.butter(order, cutoff_frq, 'low', fs=fs, output='sos')
    butter_y = signal.sosfilt(sos, sig)

    filter_y = sig.copy()
    if Filter is FirstOrderLowPass:
        lp = Filter(cut_off_frequency=cutoff_frq, dt=1 / fs)
    else:
        lp = Filter(sos, (1,))

    for i in range(1, len(sig)):
        filter_y[i] = np.atleast_1d(lp(sig[i], filter_y[i - 1]))[0]

    if 0:
        plt.semilogx(*get_damping(sig, butter_y, fs), '-', label='butter')
        plt.semilogx(*get_damping(sig, filter_y, fs), '-', label='simple')
        plt.semilogx(*get_filter_damping(sos, fs), '-k', label='filter')

        plt.legend()
        plt.semilogx([cutoff_frq, cutoff_frq * 100], [0, -40 * order], label='20*order dB/decade')
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Damping [dB]')
        plt.grid(which='both')
        plt.axvline(cutoff_frq, color='green')
        plt.plot(cutoff_frq, -3, '.r', label='-3db at cutoff')
        plt.show()

    f, d = get_filter_damping(sos, fs)
    f_ref, d_ref = f[64:512], d[64:512]

    f, d = get_damping(sig, butter_y, fs)
    npt.assert_allclose(np.interp(f_ref, f, d), d_ref, atol=0.5)
    f, d = get_damping(sig, filter_y, fs)
    npt.assert_allclose(np.interp(f_ref, f, d), d_ref, atol=0.5)


def test_sos_filter():
    """Compare the SOS filter of DYNAMIKS against the scipy one."""

    # Define the time array.
    nyquist_frequency = 5.0  # [Hz]
    frequency_resolution = 0.2  # [Hz]
    sampling_frequency = 2 * nyquist_frequency  # [Hz]
    dt = 1.0 / sampling_frequency  # [s]
    time_end = 1 / frequency_resolution  # [s]
    time = np.arange(0.0, time_end + dt / 2, dt)  # [s]

    # Create a signal with shape (n_time, uvw, n_wt, n_particles).
    uvw = 3  # Must be 3.
    n_wt = 2
    n_particles = 8
    rng = np.random.default_rng(123)
    # Time must be the first dimension to get a C-contiguous array after reshape to column inside SOSFilter.
    signal_original = rng.uniform(-1.0, +1.0, size=(time.size, uvw, n_wt, n_particles))

    # Generate low-pass filter.
    sos = signal.butter(6, 2.0, btype="low", output="sos", fs=sampling_frequency)

    # Apply filter from scipy.
    signal_filtered_scipy = signal.sosfilt(sos, signal_original, axis=0)

    # Apply filter from DYNAMIKS.
    filt = SOSFilter(sos, (uvw, n_wt, n_particles))
    signal_filtered_dynamiks = np.zeros_like(signal_original)
    for k in range(time.size):
        signal_filtered_dynamiks[k, ...] = filt(signal_original[k, ...])

    # Test.
    npt.assert_almost_equal(signal_filtered_dynamiks, signal_filtered_scipy)


def test_first_order_alpha():
    if 0:
        for dt in [0.1, 1, 2]:
            f_lst = np.linspace(0, 1 / dt / 2, 100)
            alpha_lst = [FirstOrderLowPass(cut_off_frequency=f, dt=dt).alpha for f in f_lst]
            plt.plot(f_lst, alpha_lst, label=f'dt: {dt}')
        plt.legend()
        plt.show()

    with pytest.raises(ValueError, match=re.escape(
            """The cut-off frequency (6Hz) is too high for the time step (0.1s).\nPlease reduce to at most 5.0Hz or discard the temporal filter.""")):
        FirstOrderLowPass(6, .1)


@pytest.mark.parametrize('sos_filter', [0, 1])
@pytest.mark.parametrize('cutoff_frq', [.1, .2])
def test_ParticleMotionModel_temporal_filter(cutoff_frq, sos_filter):
    dt = .1
    if sos_filter:
        filter = signal.butter(1, cutoff_frq, 'low', fs=1 / dt, output='sos')
    else:
        filter = cutoff_frq

    pmm = ParticleMotionModel(x_speed=XSpeed.Particle, include_wakes=False, temporal_filter=filter)

    fs = DefaultDWMFlowSimulation(x=[0, -200], y=[0, 0], ws=8, ti=.2,
                                  particleMotionModel=pmm, n_particles=1,
                                  dt=dt, d_particle=100,
                                  site='random')

    ws_dumper = DataDumper(lambda fs: fs.get_windspeed(fs.particle_position_xip[:, 0, 0], include_wakes=pmm.include_wakes, exclude_wake_from=[0])[:, 0],
                           coords={'uvw': ['u', 'v', 'w']})
    pm_dumper = DataDumper(lambda fs: [fs.particle_position_xip[:, 0, 0], fs.particle_velocity_uip[:, 0, 0]],
                           coords={'type': ['pos', 'vel'], 'uvw': ['u', 'v', 'w']})
    fs.step_handlers.extend([ws_dumper, pm_dumper])
    fs.run(1000)

    pm_data = pm_dumper.to_xarray()  # [:200:10]
    ws_data = ws_dumper.to_xarray()

    if 0:
        plot_filter_damping(1, cutoff_frq, 1 / dt)
        for uvw in 'uvw':
            f, d = get_damping(ws_data.sel(uvw=uvw), pm_data.sel(type='vel', uvw=uvw), 1 / dt)
            plt.semilogx(f, d, label=uvw)
        plt.plot(cutoff_frq * np.array([5, 10]), -20 * np.log10([5, 10]), '.r')
        plt.legend()
        plt.show()

    for uvw in 'uvw':
        f, d = get_damping(ws_data.sel(uvw=uvw), pm_data.sel(type='vel', uvw=uvw), 1 / dt)
        npt.assert_allclose(np.interp(cutoff_frq * np.array([1, 5, 10]), f, d),
                            np.r_[-3, -20 * np.log10([5, 10])], atol=2.5)


@pytest.mark.parametrize('sos_filter', [0, 1])
def test_ParticleMotionModel_temporal_filter_reset(sos_filter):
    dt = .1
    cutoff_frq = .1
    if sos_filter:
        filter = signal.butter(1, cutoff_frq, 'low', fs=1 / dt, output='sos')
    else:
        filter = cutoff_frq

    pmm = ParticleMotionModel(x_speed=XSpeed.Particle, include_wakes=True, temporal_filter=filter)

    fs = DefaultDWMFlowSimulation(x=[0, -200], y=[0, 0], ws=8, ti=.05,
                                  particleMotionModel=pmm,
                                  dt=dt, d_particle=.5, n_particles=8,
                                  site='mann')
    i = 0
    ws_dumper = DataDumper(lambda fs, i=i: fs.get_windspeed(fs.particle_position_xip[:, 0, i], include_wakes=pmm.include_wakes, exclude_wake_from=[i]),
                           coords={'uvw': ['u', 'v', 'w']})
    pm_dumper = DataDumper(lambda fs, i=i: [fs.particle_position_xip[:, 0, i], fs.particle_velocity_uip[:, 0, i]],
                           coords={'type': ['pos', 'vel'], 'uvw': ['u', 'v', 'w']})
    fs.step_handlers.extend([ws_dumper, pm_dumper])

    # fs.visualize(200, dt=1, view=XYView(x=np.linspace(-300, 1000), y=np.linspace(-500, 500), z=70))
    fs.run(65)

    pm_data = pm_dumper.to_xarray()  # [:200:10]
    # ws_data = ws_dumper.to_xarray()
    if 0:
        plt.figure()
        pm_data.sel(type='pos', uvw='u').plot()
        plt.figure()
        pm_data.sel(type='vel', uvw='u').plot()
        plt.show()

    # when particle becomes the new particle, the velocity low pass filter must be reset
    # and the velocity instantaneously adapt to the velocity at the rotor (which is in wake of upstream turbine)
    i = np.where(np.diff(pm_data.sel(type='pos', uvw='u')) < 0)  # step where particle moves back to rotor
    assert np.diff(pm_data.sel(type='vel', uvw='u'))[i] < -5


# @pytest.mark.parametrize('pmm_cls', [ParticleMotionModel, HillVortexParticleMotion])
# def test_temporal_filter(pmm_cls):
#     dt = .1
#     axes = plt.subplots(2, 1)[1]
#     for temporal_filter in [None, 5, 1, .1]:
#         pmm = pmm_cls(x_speed=XSpeed.Global, include_wakes=False, temporal_filter=temporal_filter,
#                       spatial_filter=CGIRotorAvg(7))
#         fs = DefaultDWMFlowSimulation(x=[0], y=[0],
#                                       particleMotionModel=pmm,
#                                       dt=dt, d_particle=5, n_particles=20,
#                                       )
#
#         p_i = -1
#         fs.run(40)
#         ws_dumper = DataDumper(lambda fs: fs.get_windspeed(fs.particle_position_xip[:, 0, p_i], include_wakes=pmm.include_wakes, exclude_wake_from=[0]),
#                                coords={'uvw': ['u', 'v', 'w']})
#         pm_dumper = DataDumper(lambda fs: [fs.particle_position_xip[:, 0, p_i], fs.particle_velocity_uip[:, 0, p_i]],
#                                coords={'type': ['pos', 'vel'], 'uvw': ['u', 'v', 'w']})
#         fs.step_handlers.extend([ws_dumper, pm_dumper])
#
#         if 0:
#             axes = plt.subplots(2, 1)[1]
#             fs.visualize(500, dt=10, view=MultiView([
#                 XYView(x=np.linspace(-300, 4000, 500), y=np.linspace(-200, 200, 500), ax=axes[0],
#                        flowVisualizer=Flow2DVisualizer(uvw='v', include_wakes=False),
#                        visualizers=[ParticleVisualizer(),
#                                     lambda fs:axes[0].plot(*fs.particle_position_xip[[0, 1], 0, p_i], '.r', zorder=32)]),
#                 XZView(x=np.linspace(-300, 4000, 500), z=np.linspace(0, 200, 500), y=0, ax=axes[1],
#                        flowVisualizer=Flow2DVisualizer(uvw='w', include_wakes=False),
#                        visualizers=[ParticleVisualizer(),
#                                     lambda fs:axes[1].plot(*fs.particle_position_xip[[0, 2], 0, p_i], '.r', zorder=32)]),
#             ]))
#         fs.run(500, verbose=0)
#         pm_data = pm_dumper.to_xarray()
#
#         pm_data.sel(type='pos', uvw='v').plot(label=str(temporal_filter), ax=axes[0])
#         pm_data.sel(type='pos', uvw='w').plot(label=str(temporal_filter), ax=axes[1])
#     plt.legend()
#     plt.show()


@pytest.mark.parametrize('pmm_cls', [ParticleMotionModel, HillVortexParticleMotion])
@pytest.mark.parametrize('spatial_filter,wakes,ref', [(CGIRotorAvg(7), False, 7),
                                                      (CGIRotorAvg(7), True, 5.49),
                                                      (EqGridRotorAvg(), False, 8),
                                                      (EqGridRotorAvg(3), False, (4 + 8 * 8) / 9)])
def test_spatial_filter(pmm_cls, spatial_filter, wakes, ref):
    dt = 1
    pmm = pmm_cls(x_speed=XSpeed.Particle, include_wakes=wakes, temporal_filter=None,
                  spatial_filter=spatial_filter)

    def mean_ws(xyz, uvw, time=None):
        # 4m/s from 0 to 5m from rotor center, 8m/s elsewhere
        uvw[0] += 4. + 4 * ((xyz[1]**2 + (xyz[2] - 70)**2) > 25)
        return uvw
    site = UniformSite(ws=8., ti=.1)
    site.add_mean_windspeed = mean_ws
    fs = DefaultDWMFlowSimulation(x=[0, -200], y=[0, 0], ws=8,
                                  particleMotionModel=pmm,
                                  dt=dt, d_particle=.5, n_particles=8,
                                  site=site)

    i = 2
    ws_dumper = DataDumper(lambda fs: fs.get_windspeed(fs.particle_position_xip[:, 0, i], include_wakes=pmm.include_wakes, exclude_wake_from=[i]),
                           coords={'uvw': ['u', 'v', 'w']})
    pm_dumper = DataDumper(lambda fs: [fs.particle_position_xip[:, 0, i], fs.particle_velocity_uip[:, 0, i]],
                           coords={'type': ['pos', 'vel'], 'uvw': ['u', 'v', 'w']})
    fs.step_handlers.extend([ws_dumper, pm_dumper])

    if 0:
        fs.visualize(100, dt=1, view=XZView(x=np.linspace(-300, 300, 500), z=np.linspace(0, 200, 500), y=0,
                                            visualizers=[ParticleVisualizer(), lambda fs:plt.plot(*fs.particle_position_xip[[0, 2], 0, i], '.r', zorder=32)]))

    fs.run(60)

    pm_data = pm_dumper.to_xarray()  # [:200:10]
    npt.assert_allclose(pm_data.sel(type='vel', uvw='u')[pm_data.sel(type='pos', uvw='u') == 0][1:], ref, atol=0.1)


def test_invalid_spatial_filter():
    pmm = ParticleMotionModel(x_speed=XSpeed.Particle, include_wakes=False, temporal_filter=None,
                              spatial_filter=10)
    with pytest.raises(NotImplementedError):
        DefaultDWMFlowSimulation(particleMotionModel=pmm)


@pytest.mark.parametrize('x_speed,wake,ref', [
    (XSpeed.Global, False, [81.6, 4.06, 60.46]),
    (XSpeed.Global, True, [81.6, 4.06, 60.46]),
    (XSpeed.Rotor, False, [98.32, 8.57, 59.44]),
    (XSpeed.Rotor, True, [46.94, 0.99, 62.23]),
    (XSpeed.Particle, False, [103.96, 10.09, 57.82]),
    (XSpeed.Particle, True, [54.02, 0.42, 62.47]),
])
def test_ParticleMotionModel(x_speed, wake, ref):

    pmm = ParticleMotionModel(x_speed=x_speed, include_wakes=wake)

    fs = DefaultDWMFlowSimulation(x=[0, -100], y=[0, 0], ws=8, ti=.2,
                                  particleMotionModel=pmm,
                                  dt=.2, d_particle=.5,
                                  site='mann')

    fs.run(12)  # run until wake hits downstream turbine, 0
    while True:
        # run until wt0 emits its next particle, p_i
        p_i = fs.boundary_particle_index[0]  # boundary particle of downstream turbine
        fs.step()
        if fs.boundary_particle_index[0] != p_i:
            break

    ws_dumper = DataDumper(lambda fs: fs.get_windspeed(fs.particle_position_xip[:, 0, p_i], include_wakes=pmm.include_wakes, exclude_wake_from=[0]),
                           coords={'uvw': ['u', 'v', 'w']})
    pm_dumper = DataDumper(lambda fs: [fs.particle_position_xip[:, 0, p_i], fs.particle_velocity_uip[:, 0, p_i]],
                           coords={'type': ['pos', 'vel'], 'uvw': ['u', 'v', 'w']})
    fs.step_handlers.extend([ws_dumper, pm_dumper])
    T = 10
    # fs.visualize(fs.time + T, XYView(visualizers=[ParticleVisualizer()]))
    fs.run(fs.time + T)
    axes = plt.subplots(3, 1)[1]
    pm_data = pm_dumper.to_xarray()  # [:200:10]
    ws_data = ws_dumper.to_xarray()
    print(f'x_speed: {x_speed.name}, wake: {wake}', np.round(fs.particle_position_xip[:, 0, p_i], 2).tolist())
    if 0:
        n = 5
        pm_data.sel(type='pos', uvw='u').plot(ax=axes[0])
        setup_plot(ax=axes[0], xlabel='time', ylabel='particle position, x')
        axes[1].plot(pm_data.sel(type='pos', uvw='u')[::n], pm_data.sel(type='pos', uvw='v')[::n], '.-')
        setup_plot(ax=axes[1], xlabel='particle position, x', ylabel='particle position, y')
        axes[2].plot(pm_data.sel(type='pos', uvw='u')[::n], pm_data.sel(type='pos', uvw='w')[::n], '.-')
        setup_plot(ax=axes[2], xlabel='particle position, x', ylabel='particle position, z')
        axes = plt.subplots(3, 1)[1]
        for ax, uvw in zip(axes, 'uvw'):
            pm_data.sel(type='vel', uvw=uvw).plot(ax=ax, label=f'particle velocity, {uvw}')
            ax.plot(ws_data.time + 1, ws_data.sel(uvw=uvw), label=f'wind speed, {uvw}')
            ax.legend()
        plt.suptitle(f'x_speed: {x_speed.name}, wake: {wake}')
        plt.show()
    if pmm.x_speed != XSpeed.Particle:
        npt.assert_allclose(pm_data.isel(time=0).sel(uvw='u', type='vel') * T, ref[0], atol=2.1)
    npt.assert_allclose(pm_data.sel(type='vel').sum('time') * fs.dt + [0, 0, 70], ref, atol=2.1)
    npt.assert_array_almost_equal(fs.particle_position_xip[:, 0, p_i], ref, 2)


@pytest.mark.parametrize('deficit_generator,T,ref_y,ref_z',
                         [(PyWakeDeficitGenerator(deficitModel=BastankhahGaussianDeficit()), 65,
                           [-0.0, 2.72, 12.25, 21.76, 30.09, 36.82, 42.3, 46.82, 50.6, 53.8],
                           [70.0, 70.84, 73.76, 76.69, 79.25, 81.32, 83.0, 84.39, 85.55, 86.53]),
                             (jDWMAinslieGenerator(), 50,
                              [-0.0, 2.88, 6.49, 10.07, 13.61, 17.06, 20.43, 23.78, 27.1, 30.37],
                              [70.0, 70.89, 71.99, 73.09, 74.18, 75.24, 76.28, 77.31, 78.33, 79.33])
                          ])
def test_deflection_pywake_deficit(deficit_generator, T, ref_y, ref_z):
    fs = DefaultDWMFlowSimulation(x=[0, 0], y=[0, 200],
                                  particleMotionModel=HillVortexParticleMotion(CutOffFrqLarsen2008),
                                  dt=1, d_particle=.5,
                                  wakeDeficitModel=deficit_generator,
                                  site='uniform')
    fs.windTurbines.yaw = 20, -20
    fs.windTurbines.tilt = 6, -6
    fs.run(T)
    x, y, z = fs.particle_position_xip

    if 0:
        print(np.round(-y[0], 2).tolist())
        print(np.round(z[0], 2).tolist())
        for i, yaw in enumerate(fs.windTurbines.yaw):
            plt.plot(x[i], y[i], '.-', label=f'particles (x,y), yaw={yaw}')
        plt.plot(x[1], -y[1] + 200, 'x', label=f'symmetry')
        for i, tilt in enumerate(fs.windTurbines.sensors.tilt):
            plt.plot(x[i], z[i], '.-', label=f'particles (x,z), tilt={tilt}')
        plt.legend()
        plt.show()
        fs.visualize(100, dt=1,  # xlim=[0, 100], ylim=[-100, 100],
                     view=XYView(z=70, x=np.linspace(-50, 800, 100), y=np.linspace(-100, 300)))

    npt.assert_array_almost_equal(y[0], -y[1] + 200)
    npt.assert_array_almost_equal(-y[0], ref_y, 2)
    npt.assert_array_almost_equal(z[0] - 70, 70 - z[1])
    npt.assert_array_almost_equal(z[0], ref_z, 2)


def test_PyWakeDeflectionModel_cls_or_wrong():
    with pytest.raises(ValueError, match="Did you forget the brackets: JimenezWakeDeflection()"):
        PyWakeDeflectionModel(JimenezWakeDeflection)

    with pytest.raises(ValueError, match="must be a DeflectionIntegrator instance"):
        PyWakeDeflectionModel(HillVortexParticleMotion())


@pytest.mark.parametrize('deflectionModel,dt', [(JimenezWakeDeflection(beta=.1), 1),
                                                (GCLHillDeflection(N=100), .1)])
def test_PyWakeDeflectionModel(deflectionModel, dt):
    k = .1
    deficitModel = BastankhahGaussianDeficit(k=k)
    deflectionModel._wake_deficitModel = deficitModel
    windTurbines = DemoWindTurbines(x=[0, 0], y=[0, 200], z=0)
    windTurbines.yaw = 20, -20
    windTurbines.tilt = 6, -6
    fs = DefaultDWMFlowSimulation(x=[0, 0], y=[0, 200],
                                  windTurbines=windTurbines,
                                  particleMotionModel=PyWakeDeflectionModel(
                                      deflectionModel, CutOffFrqLarsen2008),
                                  n_particles=30,
                                  dt=dt, d_particle=.5,
                                  wakeDeficitModel=PyWakeDeficitGenerator(deficitModel),
                                  site='uniform')
    fs.run(100, verbose=0)

    from py_wake.examples.data.hornsrev1 import V80
    from py_wake.site import UniformSite as UniformSitePW

    from py_wake.flow_map import XYGrid
    for i in [0, 1]:
        p_xp = fs.particle_position_xip[:, i, fs.get_active_particles_idx(i, np.linspace(0, 600))]

        k = .1
        wfm = PropagateDownwind(UniformSitePW(ws=10), V80(), deficitModel, deflectionModel=deflectionModel)
        sim_res = wfm([0, 0], [0, 200], yaw=windTurbines.yaw, tilt=windTurbines.tilt, wd=[270])
        sim_res.flow_map(XYGrid(x=p_xp[0], y=[0]))
        hcw = -wfm.deflectionModel.hcw_ijlk[i, :, 0, 0]
        dh = -wfm.deflectionModel.dh_ijlk[i, :, 0, 0]
        if 0:
            axes = plt.subplots(2, 1)[1]
            fs.show(MultiView([XYView(x=np.linspace(-200, 1000), y=np.linspace(-200, 600),
                                      visualizers=[ParticleVisualizer()], ax=axes[0]),
                              XZView(x=np.linspace(-200, 1000), y=0, z=np.linspace(0, 200),
                                     visualizers=[ParticleVisualizer()], ax=axes[1])]))
            plt.figure()
            plt.plot(p_xp[0], p_xp[1], '.-', label='dynamiks')
            plt.plot(p_xp[0], hcw, '.-', label='pywake')
            plt.legend()
            plt.show()
        npt.assert_allclose(p_xp[1], hcw, rtol=.03)  # decrease dt to get more accurate results
        npt.assert_allclose(p_xp[2], dh + windTurbines.hub_height(i), rtol=.003)


def test_PyWakeDeflectionModel_Ainslie():
    deflectionModel = JimenezWakeDeflection(beta=.1)
    windTurbines = DemoWindTurbines(x=[0, 0], y=[0, 200], z=0)
    windTurbines.yaw = 20, -20
    windTurbines.tilt = 6, -6
    fs = DefaultDWMFlowSimulation(x=[0, 0], y=[0, 200],
                                  windTurbines=windTurbines,
                                  particleMotionModel=PyWakeDeflectionModel(
                                      deflectionModel, CutOffFrqLarsen2008),
                                  n_particles=30,
                                  dt=1, d_particle=.5,
                                  wakeDeficitModel=jDWMAinslieGenerator(),
                                  site='uniform')
    fs.run(100, verbose=0)

    from py_wake.examples.data.hornsrev1 import V80
    from py_wake.site import UniformSite as UniformSitePW

    from py_wake.flow_map import XYGrid
    for i in [0, 1]:
        p_xp = fs.particle_position_xip[:, i, fs.get_active_particles_idx(i, np.linspace(0, 600))]

        k = .1
        deficitModel = BastankhahGaussianDeficit(k=k)
        wfm = PropagateDownwind(UniformSitePW(ws=10), V80(), deficitModel, deflectionModel=deflectionModel)
        sim_res = wfm([0, 0], [0, 200], yaw=windTurbines.yaw, tilt=windTurbines.tilt, wd=[270])
        sim_res.flow_map(XYGrid(x=p_xp[0], y=[0]))
        hcw = -wfm.deflectionModel.hcw_ijlk[i, :, 0, 0]
        dh = -wfm.deflectionModel.dh_ijlk[i, :, 0, 0]
        if 0:
            axes = plt.subplots(2, 1)[1]
            fs.show(MultiView([XYView(x=np.linspace(-200, 1000), y=np.linspace(-200, 600),
                                      visualizers=[ParticleVisualizer()], ax=axes[0]),
                              XZView(x=np.linspace(-200, 1000), y=0, z=np.linspace(0, 200),
                                     visualizers=[ParticleVisualizer()], ax=axes[1])]))
            plt.figure()
            plt.plot(p_xp[0], p_xp[1], '.-', label='dynamiks')
            plt.plot(p_xp[0], hcw, '.-', label='pywake')
            plt.legend()
            plt.show()
        npt.assert_allclose(p_xp[1], hcw, rtol=.03)  # decrease dt to get more accurate results
        npt.assert_allclose(p_xp[2], dh + windTurbines.hub_height(i), rtol=.003)


def test_deflection_deficit_direction():
    deficit_generator = PyWakeDeficitGenerator(deficitModel=BastankhahGaussianDeficit())
    fs = DefaultDWMFlowSimulation(x=[0, 0, 0], y=[-200, 0, 200],
                                  particleMotionModel=HillVortexParticleMotion(CutOffFrqLarsen2008),
                                  dt=1, d_particle=.5,
                                  wakeDeficitModel=deficit_generator,
                                  site='uniform')
    fs.windTurbines.yaw = 20, 0, -20
    fs.windTurbines.tilt = 6, 0, -6
    fs.run(100)
    fs.step()

    wake_center_pos_xi = np.array([fs.get_particle_path(i, 100)[:, 0] for i in range(3)]).T

    uvw_ui = np.array([fs.get_windspeed(p, include_wakes=True)[:, 0] for p in wake_center_pos_xi.T]).T
    c_lst = ['#1f77b4', '#ff7f0e', '#2ca02c']  # [plt.plot([])[0].get_color() for _ in range(3)]
    if 0:
        axes = plt.subplots(4, 3, figsize=(18, 10))[1]
        fs.show(MultiView(
            [XYView(z=70, x=np.linspace(-50, 800, 100), y=np.linspace(-300, 300), ax=ax,
                    visualizers=[lambda fs, ax=ax:[ax.plot(100, py, '.', color=c)
                                                   for c, py in zip(c_lst, wake_center_pos_xi[1])]],
                    flowVisualizer=Flow2DVisualizer(uvw=uvw))
             for ax, uvw in zip(axes[0], 'uvw')] +

            [XZView(z=np.linspace(0, 150), x=np.linspace(-50, 800, 100), y=y,
                    ax=ax, flowVisualizer=Flow2DVisualizer(uvw=uvw),
                    visualizers=[lambda fs, ax=ax, z=z, c=c: ax.plot(100, z, '.', color=c)])
             for axes, y, c, z in zip(axes[1:], wake_center_pos_xi[1], c_lst, wake_center_pos_xi[2])
             for ax, uvw in zip(axes, 'uvw')]
        ))
    print(np.round(uvw_ui, 2).tolist())
    ref = [[1.21, 0.18, 1.21],  # u yawed has same u, while perpendicular has higher deficit
           [-3.2, 0.0, 3.2],  # wt0 pushes wake to the south resulting in negative y, opposite for wt2
           [0.98, 0.0, -0.98]]  # wt pushes wake up resulting in positive w, opposite for wt2
    # total deficit of yawed turbines, sqrt((10-1.21)**2+3.2**2+.98**2) is less than deficit of wt1, 10-.18, because
    # the ct of the yawed turbines are calculated from the perpendicular projection of the wind speed.
    npt.assert_array_almost_equal(uvw_ui, ref, 2)
