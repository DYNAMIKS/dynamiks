from dynamiks.dwm.added_turbulence_models import IsotropicMannTurbulence, AutoScalingIsotropicMannTurbulence,\
    SynchronizedAutoScalingIsotropicMannTurbulence
from dynamiks.utils.test_utils import DefaultDWMFlowSimulation, npt, tfp
from dynamiks.views import XYView, YView, GridSlice3D, MultiView
import numpy as np
from dynamiks.visualizers.visualizer_utils import AxesInitializer
from dynamiks.visualizers.flow_visualizers import Flow1DVisualizer, Flow2DVisualizer, Flow
import pytest
import matplotlib.pyplot as plt
from pathlib import Path
from dynamiks.sites._site import TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField


@pytest.fixture(scope="session")
def delete_cache():
    Path('Hipersim_mann_l5.0_ae1.0000_g0.0_h0_128x128x128_1.562x0.62x0.62_s0001.nc').unlink(missing_ok=True)


@pytest.mark.parametrize("atm", [SynchronizedAutoScalingIsotropicMannTurbulence(),
                                 AutoScalingIsotropicMannTurbulence(),
                                 AutoScalingIsotropicMannTurbulence(),  # use saved file
                                 AutoScalingIsotropicMannTurbulence(cache_field=False),
                                 IsotropicMannTurbulence.generate(D=80),
                                 IsotropicMannTurbulence])
def test_IsotropicMannTurbulence(atm, delete_cache):
    D = 80
    U = 10
    ti = 0.1
    if atm == IsotropicMannTurbulence:
        atm = atm.from_netcdf(filename='Hipersim_mann_l5.0_ae1.0000_g0.0_h0_128x128x128_1.562x0.62x0.62_s0001.nc')
    if atm.__class__.__name__ == "IsotropicMannTurbulence":
        atm.scale()

    step_handlers = []
    turbfield = MannTurbulenceField.generate(dxyz=[1.5625, 0.625, 0.625], Nxyz=(32, 32, 32))
    turbfield.scale_TI(TI=0, U=U)
    turbfield.get_turbulence_intensity = lambda xyz, U: ti
    site = TurbulenceFieldSite(ws=U, turbulenceField=turbfield, turbulence_offset=(0, 0, 0))

    fs = DefaultDWMFlowSimulation(x=[-5 * D, 0, D * 5, D * 10], y=[0, 0, 0, 0], ws=U, ti=ti, site=site,
                                  addedTurbulenceModel=atm,
                                  step_handlers=step_handlers)

    fs.run(22)  # wake from wt1 has just hit x=200
    assert fs.get_windspeed(YView(x=200, z=70, y=np.linspace(-200, 200, 500), adaptive=True), True)[0].min() < 5
    fs.run(100)
    if 0:
        ax1, ax2 = plt.subplots(2, 1)[1]
        fs.show(MultiView([
            YView(x=200, z=70, y=np.linspace(-200, 200, 500), adaptive=True, ax=ax2),
            XYView(z=70, x=np.linspace(-500, 500, 500), y=np.linspace(-200, 200), adaptive=False, ax=ax1,
                   visualizers=[lambda fs:ax1.axvline(200)])

        ]))

    view1d = YView(x=200, z=70, y=np.linspace(0, 10, 10), adaptive=False)
    # print(np.round(fs.get_windspeed_grid(grid=view1d, include_wakes=True)[0].squeeze(), 2).tolist())
    if isinstance(atm, SynchronizedAutoScalingIsotropicMannTurbulence):
        # SynchronizedAutoScalingIsotropicMannTurbulence has a random wt dependent offset
        ref = [4.76, 4.78, 4.95, 4.43, 4.02, 4.37, 4.47, 4.5, 4.46, 4.46]
    else:
        ref = [4.53, 4.77, 4.98, 5.31, 5.15, 4.9, 5.02, 4.84, 4.77, 4.81]
    if 0:
        plt.plot(view1d.y, fs.get_windspeed(view1d, include_wakes=True)[0].squeeze())
        plt.plot(view1d.y, ref, label='ref')
        plt.legend()
        plt.show()

    npt.assert_array_almost_equal(fs.get_windspeed(view1d, include_wakes=True)[0].squeeze(), ref, 2)

    npt.assert_array_almost_equal([fs.get_windspeed([view1d.x, y_, view1d.z], include_wakes=True)[0, 0]
                                   for y_ in view1d.y],
                                  ref, 2)

    axes = turbfield.get_axes(fs.time)
    gridSlice3D = GridSlice3D(x_slice=slice(5, 6), y_slice=slice(0, 17), z_slice=slice(10, 11), axes=axes)
    grid1d = YView(x=gridSlice3D.x, z=gridSlice3D.z, y=gridSlice3D.y, adaptive=False)
    npt.assert_array_almost_equal(fs.get_windspeed(gridSlice3D, include_wakes=True)[0].squeeze(),
                                  fs.get_windspeed(grid1d, include_wakes=True)[0].squeeze())


def test_IsotropicMannTurbulence_ainslie():
    from dynamiks.dwm.particle_deficit_profiles.ainslie import jDWMAinslieGenerator

    D = 80
    U = 10
    ti = .1

    atm = IsotropicMannTurbulence.generate(D)
    step_handlers = []
    fs = DefaultDWMFlowSimulation(x=[-5 * 80, 0, 80 * 5], y=[0, 0, 0], ws=U, ti=ti, site='uniform',
                                  wakeDeficitModel=jDWMAinslieGenerator(),
                                  addedTurbulenceModel=atm,
                                  step_handlers=step_handlers)
    if 0:
        fig = InteractiveFigure(figsize=(10, 8))
        ax1, ax2 = fig.subplots(2)

        view2d = XYView(z=70, x=np.linspace(-500, 500), y=np.linspace(-200, 200), adaptive=False, ax=ax1)
        view1d = YView(x=200, z=70, y=np.linspace(-200, 200, 500), adaptive=True, ax=ax2)
        flow_visualizer = Flow1DVisualizer(view=view1d)
        step_handlers = (fs._visualization_step_handlers(view=view2d, flowVisualizer=Flow2DVisualizer(), particleVisualizer=False) +
                         [AxesInitializer(ax=ax2),
                          flow_visualizer, fig])

        fs.step_handlers.extend(step_handlers)

    fs.run(100)
    view1d = YView(x=200, z=70, y=np.linspace(0, 10, 10), adaptive=True)
    # print(np.round(fs.get_windspeed_grid(grid=view1d, include_wakes=True)[0].squeeze(), 2).tolist())
    ref = [3.05, 3.51, 3.9, 4.47, 4.14, 3.65, 3.81, 3.44, 3.26, 3.26]
    if 0:
        plt.plot(view1d.y, fs.get_windspeed_grid(grid=view1d, include_wakes=True)[0].squeeze())
        plt.plot(view1d.y, ref, label='ref')
        plt.legend()
        plt.show()
    npt.assert_array_almost_equal(fs.get_windspeed(view1d, include_wakes=True)[0].squeeze(), ref, 2)
    npt.assert_array_almost_equal([fs.get_windspeed([200, y, 70], include_wakes=True)[0, 0] for y in view1d.y], ref, 2)


def test_deficit():
    U = 10
    ti = 0.1
    D = 80
    fs = DefaultDWMFlowSimulation(x=[-5 * 80, 0, 80 * 5], y=[0, 0, 0], ws=U, ti=ti, site='uniform',
                                  addedTurbulenceModel=IsotropicMannTurbulence.generate(D),
                                  )

    fs.run(100)
    if 0:
        fs.visualize(time_stop=200, dt=1,
                     view=XYView(z=70, x=np.linspace(-200, 500), y=np.linspace(-200, 200), adaptive=False),
                     flowVisualizer=Flow2DVisualizer(flow2d=Flow(uvw='v')))
    grid = YView(x=-100, z=70, y=np.linspace(-100, 100), adaptive=False)
    uvw = fs.get_windspeed(grid, include_wakes=True).squeeze()
    if 0:
        plt.plot(grid.y, uvw[1])
        plt.show()
    npt.assert_allclose(uvw.mean(1), [8.54, 0, 0], atol=0.06)


@pytest.mark.parametrize('folder', ["", 'added_turb'])
def test_folder_arg(folder):
    n = 'Hipersim_mann_l5.0_ae1.0000_g0.0_h0_128x128x128_1.562x0.62x0.62_s0001.nc'
    p = Path(n)
    if folder:
        Path(folder).unlink(missing_ok=True)
        p = folder / p
    p.unlink(missing_ok=True)
    addedTurbulenceModel = AutoScalingIsotropicMannTurbulence(folder=folder)
    DefaultDWMFlowSimulation(x=[-5 * 80, 0, 80 * 5], y=[0, 0, 0], ws=10, ti=0.1, site='uniform',
                             addedTurbulenceModel=addedTurbulenceModel)
    assert p.exists()


if __name__ == '__main__':
    test_IsotropicMannTurbulence_ainslie()
