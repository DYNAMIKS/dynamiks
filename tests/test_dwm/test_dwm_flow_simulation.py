import time

import pytest

from dynamiks.dwm.added_turbulence_models import AutoScalingIsotropicMannTurbulence,\
    SynchronizedAutoScalingIsotropicMannTurbulence
from dynamiks.utils.data_dumper import DataDumper
from dynamiks.utils.test_utils import npt, DefaultDWMFlowSimulation, tfp
from dynamiks.views import GridSlice3D, YView, EastNorthView, XYView, MultiView, XZView, XView
from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines
import matplotlib.pyplot as plt
import numpy as np
from py_wake.utils.plotting import setup_plot
import xarray as xr
from py_wake.examples.data.hornsrev1 import V80
from py_wake.wind_turbines.generic_wind_turbines import GenericWindTurbine
from dynamiks.wind_turbines.hawc2_windturbine import HAWC2WindTurbinesDummy
from dynamiks.dwm.particles_model import DistributedWindTurbinesParticles, WindTurbinesParticles
from dynamiks.sites.turbulence_fields import MannTurbulenceField
from dynamiks.sites._site import TurbulenceFieldSite
from dynamiks.visualizers._visualizers import ParticleVisualizer


def test_windspeed_and_production():
    ws_dumper = DataDumper(data_dumper_function=lambda fs:
                           np.array([fs.windTurbines.rotor_avg_freestream[:, 0],
                                     fs.windTurbines.rotor_avg_windspeed[:, 0]]).T,
                           coords={'wt': [0, 1, 2], 'wakes': [False, True]})

    power_dumper = DataDumper(data_dumper_function=lambda fs: [[fs.windTurbines.power(wakes, wt)
                                                                for wakes in [False, True]] for wt in [0, 1, 2]],
                              coords={'wt': [0, 1, 2], 'wakes': [False, True]})

    def sleep_step_handler(fs):
        time.sleep(0.1)

    step_handlers = [ws_dumper, power_dumper, sleep_step_handler]

    fs = DefaultDWMFlowSimulation(x=[-5 * 80, 0, 80 * 5], y=[0, 0, 0], site='mann', step_handlers=step_handlers,
                                  addedTurbulenceModel=None)

    fs.step_handler_time = {}
    fs.step()
    da = power_dumper.to_xarray()  # test to_xarray with only one time stamp
    assert da.shape == (1, 3, 2)
    # fs.visualize(100)

    fs.run(10)
    npt.assert_allclose(fs.step_handler_time[sleep_step_handler], fs.time * 0.1, atol=0.1)
    fs.step_handlers.remove(sleep_step_handler)

    fs.run(101)
    fs.step()
    ws = ws_dumper.to_xarray()
    power = power_dumper.to_xarray() / 1000
    if 0:
        for t, da in [('ws', ws), ('Power', power)]:
            plt.figure()
            for wt in da.wt:
                c = plt.plot(da.sel(wt=wt, wakes=False), label=f'wt {wt.item()}')[0].get_color()
                plt.plot(da.sel(wt=wt, wakes=True), '--', color=c)
            setup_plot(title=t)
            plt.show()
    eff = power.sel(wakes=True) / power.sel(wakes=False)

    try:
        npt.assert_allclose((ws.sel(wakes=True) / ws.sel(wakes=False)).min('time'),
                            [1., 0.66393881, 0.62711343], atol=0.02)
        npt.assert_allclose((ws.sel(wakes=True) / ws.sel(wakes=False))[60:].max('time'),
                            [1.0, 0.8294646920198068, 0.855144263012068], atol=0.02)

        # wake_loss (max, min and mean)
        npt.assert_allclose(1 - eff.min('time'), [0.0, 0.72, 0.74], atol=0.02)
        npt.assert_allclose(1 - eff[60:].max('time'), [0.0, 0.27, 0.28], atol=0.02)
        npt.assert_allclose(1 - eff[60:].mean('time'), [0.0, 0.51, 0.54], atol=0.02)
    except BaseException:
        for v in [(ws.sel(wakes=True) / ws.sel(wakes=False)).min('time'),
                  (ws.sel(wakes=True) / ws.sel(wakes=False))[60:].max('time'),
                  (1 - eff.min('time')),
                  (1 - eff[60:].max('time')),
                  (1 - eff[60:].mean('time'))]:
            print(np.round(v.values, 2).tolist())
        raise


def test_no_upstream_deficit():
    fs = DefaultDWMFlowSimulation(x=[0, 80 * 5], y=[0, 0], site='mann', addedTurbulenceModel=None)
    fs.run(10)
    ref_uvw = fs.get_windspeed([0, 0, 70], include_wakes=False, exclude_wake_from=[])
    npt.assert_array_equal(ref_uvw, fs.get_windspeed([0, 0, 70], include_wakes=True, exclude_wake_from=[0]))

    grid = GridSlice3D(x_slice=slice(749, 752), y_slice=slice(31, 33), z_slice=slice(15, 17),
                       axes=fs.site.turbulenceField.get_axes(fs.time))
    uvw = fs.get_windspeed(grid, include_wakes=True, exclude_wake_from=[0])
    da = xr.DataArray(uvw, dims=['uvw', 'x', 'y', 'z'],
                      coords={'uvw': ['u', 'v', 'w'], 'x': grid.x, 'y': grid.y, 'z': grid.z})
    npt.assert_array_almost_equal(ref_uvw[:, 0], da.interp(x=0, y=0, z=70))


def test_far_downstream_deficit():
    fs = DefaultDWMFlowSimulation(x=[0], y=[0], d_particle=0.1, site='mann', addedTurbulenceModel=None)
    fs.run(10)

    #fs.show(XYView(z=None, x=np.linspace(-100, 500), visualizers=[ParticleVisualizer()]))

    ref_uvw = fs.get_windspeed([200, 0, 70], include_wakes=False, exclude_wake_from=[])

    npt.assert_array_equal(ref_uvw, fs.get_windspeed([200, 0, 70], include_wakes=True))

    grid = GridSlice3D(x_slice=slice(812, 814), y_slice=slice(None), z_slice=slice(15, 17),
                       axes=fs.site.turbulenceField.get_axes(fs.time))
    uvw = fs.get_windspeed(grid, include_wakes=True)
    da = xr.DataArray(uvw, dims=['uvw', 'x', 'y', 'z'],
                      coords={'uvw': ['u', 'v', 'w'], 'x': grid.x, 'y': grid.y, 'z': grid.z})
    npt.assert_array_almost_equal(ref_uvw[:, 0], da.interp(x=200, y=0, z=70))


@pytest.mark.parametrize('addedTurbulenceModel', [None, AutoScalingIsotropicMannTurbulence()])
def test_windspeed_grid(addedTurbulenceModel):
    fs = DefaultDWMFlowSimulation(x=[0, 80 * 5], y=[0, 0], ti=0.05, site='mann',
                                  addedTurbulenceModel=addedTurbulenceModel)
    fs.run(100)
    for exclude_wake_from in [[], [0], [1], [0, 1]]:
        for x in [-200, 200, 700]:
            x_idx = np.searchsorted(fs.site.turbulenceField.get_axes(fs.time)[0], x)
            grid = GridSlice3D(x_slice=slice(x_idx - 2, x_idx + 2), y_slice=slice(30, 33), z_slice=slice(14, 17),
                               axes=fs.site.turbulenceField.get_axes(fs.time))
            uvw = fs.get_windspeed(grid, include_wakes=True, exclude_wake_from=exclude_wake_from, xarray=True)
            xp, yp, zp = grid.x[0], grid.y[0], grid.z[0]
            ref_uvw = fs.get_windspeed([xp, yp, zp], include_wakes=True, exclude_wake_from=exclude_wake_from)
            npt.assert_array_almost_equal(ref_uvw[:, 0], uvw.interp(x=xp, y=yp, z=zp))


@pytest.mark.parametrize('wd,ref_xy', [(270, [(0, 200, 0, -200), (0, 0, 100, -100)]),
                                       (0, [(0, 0, -100, 100), (0, 200, 0, -200)]),
                                       (240, [(0, 2 * 86.60, 50, -223.21), (0, -2 * 50, 86.60, 13.4)])])
def test_wind_direction(wd, ref_xy):
    fs = DefaultDWMFlowSimulation(x=[0, 200, 0, -200], y=[0, 0, 100, -100], wind_direction=wd)
    npt.assert_array_almost_equal(fs.windTurbines.rotor_positions_xyz[:2, :], ref_xy, 2)


def test_get_wind_direction():
    wd = 300
    fs = DefaultDWMFlowSimulation(wind_direction=wd, site='uniform')

    def add30deg(xyz, uvw, time):
        theta = np.deg2rad(30)
        uvw[0] += np.cos(theta) * 10
        uvw[1] -= np.sin(theta) * 10
        return uvw
    fs.site.add_mean_windspeed = add30deg
    assert fs.get_wind_direction([0, 0, 0], include_wakes=True, xarray=False) == 330
    npt.assert_array_equal(fs.get_wind_direction(XView(y=0, z=70, x=[0, 100]), include_wakes=True), 330)
    npt.assert_array_equal(fs.get_wind_direction(XView(y=0, z=70, x=[0, 100]), include_wakes=True, xarray=True), 330)
    npt.assert_array_equal(fs.get_wind_direction(
        XView(y=0, z=70, x=[0, 100]), include_wakes=True, xarray=True).sel(x=[0, 100]), 330)


@pytest.mark.parametrize('windTurbines', [
    lambda: PyWakeWindTurbines(x=[0, 0, 0], y=[0, 500, 1000],
                               windTurbine=[V80(),
                                            GenericWindTurbine(name='G100', diameter=100, hub_height=80, power_norm=2300)]),
    lambda: HAWC2WindTurbinesDummy(x=[0, 0, 0], y=[0, 500, 1000], htc_lst=[''], types=[0], suppress_output=True)])
def test_combinations(windTurbines):
    fs = DefaultDWMFlowSimulation(windTurbines=windTurbines())
    atm_lst = [SynchronizedAutoScalingIsotropicMannTurbulence(),
               AutoScalingIsotropicMannTurbulence()]
    for atm in atm_lst:
        fs.addedTurbulenceModel = atm
        fs.run(6)


def test_changing_winddirection():
    def wd_changer(fs):
        fs.wind_direction += 1

    power_dumper = DataDumper(lambda fs: np.round(fs.windTurbines.power() / 1000), coords={'wt': [0, 1]})
    fs = DefaultDWMFlowSimulation(x=[0, 400], y=[0, 0], site='uniform',
                                  step_handlers=[((50, 1), wd_changer), power_dumper])
    ref = [1341.0, 1341.0, 1341.0, 1341.0, 522.0, 511.0, 527.0, 794.0, 1293.0, 1341.0, 1341.0]
    if 0:
        fs.visualize(100, view=EastNorthView(z=70, x=np.linspace(-800, 800), y=np.linspace(-800, 800)))
    fs.run(110)
    da = power_dumper.to_xarray()
    if 0:
        da.sel(wt=0).plot(label='wt0')
        da.sel(wt=1).plot(label='wt1')
        plt.plot(da.time[::10], ref, 'x')
        print(da.sel(wt=1)[::10].values.tolist())
        setup_plot()
        plt.show()
    npt.assert_array_almost_equal(da.sel(wt=1)[::10], ref)


def test_turbulence_intensity():
    fs = DefaultDWMFlowSimulation()

    ti = fs.get_turbulence_intensity(YView(x=500, z=70, y=[0, 10], adaptive=False), include_wake_turbulence=False)
    npt.assert_array_equal(ti, [0.1, 0.1])


@pytest.mark.parametrize('windTurbineParticles_cls', [
    # WindTurbinesParticles,  # only needed to ensure eq when recalculating ref
    DistributedWindTurbinesParticles
])
@pytest.mark.parametrize('windTurbines_cls', [PyWakeWindTurbines, HAWC2WindTurbinesDummy])
def test_distributed_wt_particles(windTurbineParticles_cls, windTurbines_cls):
    ws = 10
    ti = 0.05
    x, y = [0, 0, 50], [0, 400, 400]
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_3.200x3.20x3.20_s0001.nc",
    )

    turbfield.scale_TI(TI=ti, U=ws)
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))
    if windTurbines_cls == PyWakeWindTurbines:
        windTurbines = windTurbines_cls(x, y, windTurbine=V80())
        ref = [10.7, 9.8, 7.6, 0.8, 3.0, 9.2, 9.9, 8.8, 10.0, 11.7, 11.3,
               10.2, 10.9, 10.6, 10.4, 10.0, 5.5, -7.6, -3.6, 8.1, 9.8]
    elif windTurbines_cls == HAWC2WindTurbinesDummy:
        from h2lib_tests import tfp as h2_tfp
        fn = [h2_tfp + 'DTU_10_MW/htc/DTU_10MW_RWT.htc']
        windTurbines = windTurbines_cls(x=x, y=y, htc_lst=fn, types=[0], suppress_output=True)

        ref = [9.7, 7.5, 5.4, 3.4, 3.5, 5.5, 7.3, 7.7, 9.7, 11.6, 11.2,
               10.2, 10.7, 9.9, 8.1, 4.7, 0.3, -2.6, -2.3, 1.7, 5.6]
    windTurbines.rotorAvgModel = None

    try:
        fs = DefaultDWMFlowSimulation(
            site=site,
            windTurbines=windTurbines,
            windTurbineParticles_cls=windTurbineParticles_cls)
        fs.run(10)

        view = YView(x=100, z=70, y=np.linspace(-100, 500, 20))
        u = fs.get_windspeed(view, include_wakes=True)[0]
        if 0:
            if windTurbineParticles_cls == WindTurbinesParticles:
                print(np.round(u, 1).tolist())
            plt.plot(view.y, np.round(u, 1),)
            plt.plot(view.y, ref, '.')
            plt.figure()
            fs.show(view=XYView(z=None, x=np.linspace(-200, 1000), y=np.linspace(-200, 1000)), block=False)
            plt.plot(view.y * 0 + view.x, view.y, '.-')
            plt.show()
        npt.assert_array_almost_equal(u, ref, 1)
    finally:
        if windTurbines_cls == HAWC2WindTurbinesDummy:
            windTurbines.h2.close()


@pytest.mark.parametrize('wd', [270, 300, 330, 360])
def test_n_particles(wd):
    fs = DefaultDWMFlowSimulation(x=[0, 80 * 5], y=[0, 0], d_particle=0.1, n_particles=None, wind_direction=wd)
    assert fs.n_particles == np.maximum(np.ceil(np.cos(np.deg2rad(270 - wd)) * 400 * 1.2 / (.1 * 80)), 10)


def test_particle_path():
    fs = DefaultDWMFlowSimulation(x=[0, 100], y=[0, 200])
    x = np.linspace(-100, 500)
    pxyz = fs.get_particle_path(0, x)
    npt.assert_array_equal(pxyz, [[], [], []])
    fs.run(100)
    if 0:
        axes = plt.subplots(2, 1)[1]
        fs.show(MultiView([
            XYView(x=x, y=np.linspace(-100, 100), visualizers=[ParticleVisualizer()], ax=axes[0]),
            XZView(x=x, y=0, z=np.linspace(0, 150), visualizers=[ParticleVisualizer()], ax=axes[1]),
        ]), block=False)
        for i, c in enumerate('ry'):
            px, py, pz = fs.get_particle_path(i, x)
            axes[0].plot(px, py, '--', color=c, zorder=32)
            axes[1].plot(px, pz, '--', color=c, zorder=32)

        plt.show()
    ref = [[[22.45, 144.9, 267.35, 389.8], [-0.42, 16.61, -5.4, -47.51], [71.96, 66.27, 66.94, 75.68]],
           [[144.9, 267.35, 389.8], [200.81, 199.19, 205.01], [72.13, 79.11, 64.38]]]
    for wt_i in [0, 1]:
        pxyz = fs.get_particle_path(wt_i, x[::10])
        #print(np.round(pxyz, 2).tolist())
        npt.assert_array_almost_equal(ref[wt_i], pxyz, 2)
    npt.assert_array_equal(fs.get_particle_path(0, [100]), fs.get_particle_path(0, 100))
    pxyz2 = fs.get_particle_path(wt_i)
    npt.assert_array_almost_equal(np.interp(pxyz[0], pxyz2[0], pxyz2[1]), pxyz[1], 4)
