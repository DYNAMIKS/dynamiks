from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines
from py_wake.examples.data.hornsrev1 import V80
from dynamiks.utils.test_utils import npt, DefaultDWMFlowSimulation
from py_wake.wind_turbines._wind_turbines import WindTurbines, WindTurbine
from py_wake.wind_turbines.generic_wind_turbines import GenericWindTurbine
import matplotlib.pyplot as plt

from dynamiks import views
from dynamiks.views import *
import numpy as np
from py_wake.examples.data import hornsrev1
from py_wake.wind_turbines.power_ct_functions import PowerCtXr
import xarray as xr


def test_windturbines():
    wt_lst = WindTurbines.from_WindTurbine_lst([V80(), GenericWindTurbine(name='G100', diameter=100,
                                                                          hub_height=80, power_norm=2300)])
    wt = PyWakeWindTurbines(x=[50, 500, 1000], y=[100, 0, -100],
                            windTurbine=wt_lst, types=[0, 1, 0])
    npt.assert_array_equal(wt.rotor_positions_east_north, [[50, 500, 1000],
                                                           [100, 0, -100],
                                                           [70, 80, 70]])
    npt.assert_array_equal(wt[0].rotor_positions_east_north, [50, 100, 70])
    assert wt[0].inductionModel.__class__.__name__ == 'InductionMatch'

    npt.assert_array_equal(wt.diameter(), [80, 100, 80])
    assert wt[1].diameter() == 100
    npt.assert_array_equal(wt.hub_height(), [70, 80, 70])
    assert wt[2].hub_height() == 70
    fs = DefaultDWMFlowSimulation(windTurbines=wt)
    r = np.array([0, .25, .5, .75, 1])
    assert wt.axisymetric_induction(r).shape == (5, 3)
    assert wt[0].axisymetric_induction(r).shape == (5, 1)

    x, y, z = np.linspace(0, 1200), np.linspace(-200, 200), np.linspace(0, 200)
    fs.wind_direction = 300
    fs.windTurbines.yaw = 10
    fs.windTurbines.tilt = 5

    for view in [XYView(z=70, x=x, y=y, adaptive=False),
                 EastNorthView(z=70, x=x, y=y, adaptive=False),
                 XZView(y=0, x=x, z=z, adaptive=False),
                 YZView(x=0, y=y, z=z, adaptive=False),
                 # Points, Grid(), GridSlice3D, View2D', 'View1D', 'XView', 'YView', 'ZView',
                 ]:

        fs.show(view=view, block=False)
        view.ax.set_title(view.__class__.__name__)
        if 0:
            plt.show()


def test_yaw_tilt():
    wt = PyWakeWindTurbines(x=[50, 500, 1000], y=[100, 0, -100],
                            windTurbine=V80())
    wt.sensors.yaw = [10, 20, 30]
    wt.sensors.tilt = [1, 2, 3]
    npt.assert_array_equal(wt.yaw_tilt(), [[10., 20., 30.], [1., 2., 3.]])
    npt.assert_array_equal(wt[0].yaw_tilt(), [[10.], [1.]])
    npt.assert_array_equal(wt[1:].yaw_tilt(), [[20., 30.], [2., 3.]])

    DefaultDWMFlowSimulation(windTurbines=wt, site='uniform')
    npt.assert_array_equal(wt.power(False), V80().power([10, 10, 10], yaw=[10, 20, 30], tilt=[1, 2, 3]))
    fs = DefaultDWMFlowSimulation(windTurbines=wt, site='mann')
    #fs.show(view=XYView(x=np.linspace(-200, 1200, 50), y=np.linspace(-200, 200, 30), z=70))
    fs.show(block=False)
    fs.run(5)
    da = wt.sensors.to_xarray()
    assert da.shape == (5, 3, len(da.sensor))
    if 0:
        plt.show()


def test_custom_input():
    u_p, p_c, ct_c = hornsrev1.power_curve[:, 0], hornsrev1.power_curve[:, 1], hornsrev1.ct_curve[:, 1]

    # Dummy derating
    ds = xr.Dataset(
        data_vars={'ct': (['ws', 'derate'], np.array([ct_c * 0, ct_c]).T),
                   'power': (['ws', 'derate'], np.array([p_c * 0, p_c]).T)},
        coords={'derate': [1, 0], 'ws': u_p, }).transpose('derate', 'ws')

    pw_wt = WindTurbine(name='test', diameter=80, hub_height=70, powerCtFunction=PowerCtXr(ds, 'w'))
    wt = PyWakeWindTurbines(x=[0, 500], y=[0, 0], windTurbine=pw_wt, rotorAvgModel=None)
    wt.add_sensor('derate', lambda self: self._derate, lambda self, value: setattr(self, '_derate', value))

    wt.sensors.derate = [0, 0]
    fs = DefaultDWMFlowSimulation(windTurbines=wt, site='uniform')

    fs.run(100)
    wt.sensors.derate = [.2, 0]
    fs.run(300)
    da = wt.sensors.to_xarray()
    if 0:
        da.sel(sensor='power', wt=0).plot()
        da.sel(sensor='power', wt=1).plot()
        plt.show()

    m1 = (da.time > 100) & (da.time < 150)  # full wake, no derating
    m2 = (da.time > 200) & (da.time < 250)  # full wake, wt0 derated 20%

    # power of downstream wt increased by more than 10%
    assert da.sel(sensor='power', wt=1)[m2].mean().item() > da.sel(sensor='power', wt=1)[m1].mean().item() * 1.1


def get_process_id(self):
    import os
    return os.getpid()


def get_var(self, name):
    return getattr(self, name)


def test_parallel_windturbines():
    wt_lst = [V80(), GenericWindTurbine(name='G100', diameter=100, hub_height=80, power_norm=2300)]
    with PyWakeWindTurbines(x=[50, 500, 1000], y=[100, 0, -100],
                            windTurbine=wt_lst, types=[0, 1, 0]) as wt:
        npt.assert_array_equal(wt.rotor_positions_east_north, [[50, 500, 1000],
                                                               [100, 0, -100],
                                                               [70, 80, 70]])
        npt.assert_array_equal(wt[0].rotor_positions_east_north, [50, 100, 70])

        npt.assert_array_equal(wt.diameter(), [80, 100, 80])
        assert wt[1].diameter() == 100
        npt.assert_array_equal(wt.hub_height(), [70, 80, 70])
        assert wt[2].hub_height() == 70

        dist_wt = wt.dist_wt
        dist_wt.get_process_id = get_process_id
        dist_wt.test = [f"hej{i}" for i in range(3)]
        dist_wt.get_var = get_var
        assert len(np.unique(dist_wt.get_process_id())) == 3
        assert isinstance(dist_wt[0].get_process_id()[0], int)
        assert dist_wt[0].test[0] == 'hej0'
        npt.assert_array_equal(dist_wt.get_var('test'), ['hej0', 'hej1', 'hej2'])
