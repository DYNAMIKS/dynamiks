from h2lib_tests import tfp as h2_tfp
import pytest
from wetb.hawc2.htc_file import HTCFile

from dynamiks.dwm.particle_motion_models import HillVortexParticleMotion
from dynamiks.sites._site import UniformSite
from dynamiks.utils.test_utils import tfp, DefaultDWMFlowSimulation, npt
from dynamiks.views import XYView, EastNorthView, MultiView, XZView, YZView
from dynamiks.visualizers._visualizers import ParticleVisualizer
from dynamiks.visualizers.flow_visualizers import Flow2DVisualizer
from dynamiks.wind_turbines.hawc2_windturbine import HAWC2WindTurbines, HAWC2WindTurbinesDummy
from dynamiks.wind_turbines.pywake_windturbines import PyWakeWindTurbines
import matplotlib.pyplot as plt
import numpy as np
from py_wake.examples.data.dtu10mw._dtu10mw import DTU10MW
from py_wake.examples.data.hornsrev1 import V80
from py_wake.wind_turbines.generic_wind_turbines import GenericWindTurbine
from py_wake.rotor_avg_models.rotor_avg_model import CGIRotorAvg, GridRotorAvg


fn = [h2_tfp + 'DTU_10_MW/htc/DTU_10MW_RWT.htc']
x, y = [50, 500, 1000], [100, 0, -100]


@pytest.mark.parametrize('windTurbines', [
    lambda: PyWakeWindTurbines(x=x, y=y, windTurbine=[
        V80(), GenericWindTurbine(name='G100', diameter=100, hub_height=80, power_norm=2300)]),
    lambda:HAWC2WindTurbines(x=x, y=y, htc_filename_lst=fn, types=[0], suppress_output=True),
    lambda: HAWC2WindTurbinesDummy(x=x, y=y, htc_lst=fn, types=[0], suppress_output=True)])
def test_dims_and_plot(windTurbines):
    try:
        windTurbines = windTurbines()
        fs = DefaultDWMFlowSimulation(windTurbines=windTurbines)

        for _ in range(2):
            for N, wts in [(3, windTurbines), (1, windTurbines[:1]), (1, windTurbines[0])]:
                assert wts.diameter().shape == (N,)
                assert wts.hub_height().shape == (N,)
                assert wts.yaw_tilt().shape == (2, N)
                assert wts.ct().shape == (N,)
                assert wts.power().shape == (N,)
                assert wts.rotor_avg_induction().shape == (N,)
                assert np.shape(wts.axisymetric_induction(r=np.linspace(0, 1, 5))) == (5, N)
            fs.step()
        axes = plt.subplots(3, 1)[1]
        fs.show(view=MultiView([XYView(z=None, x=np.linspace(-100, 1100), y=np.linspace(-200, 200), ax=axes[0]),
                                XZView(z=np.linspace(0, 200), x=np.linspace(-100, 1100), y=0, ax=axes[1]),
                                YZView(z=np.linspace(0, 200), x=0, y=np.linspace(-200, 200), ax=axes[2]),
                                ]), block=False)
    finally:
        try:
            windTurbines.h2.close()
        except BaseException:
            pass


def test_sensor_setter():
    fs = DefaultDWMFlowSimulation(x=[0, 400], y=[0, 0])
    wt = fs.windTurbines
    wt.sensors.yaw = 30
    npt.assert_array_equal(wt.sensors.yaw, (30, 30))
    wt.sensors.yaw = 10, 20
    npt.assert_array_equal(wt.sensors.yaw, (10, 20))

    with pytest.raises(ValueError, match="'yaw' is already a sensor name. Please use another name"):
        wt.add_sensor('yaw', lambda wt: wt.yaw)

    with pytest.raises(ValueError, match=r"Space not allowed in sensor name \('sensor with space'\)"):
        wt.add_sensor('sensor with space', lambda wt: wt.yaw)


@pytest.mark.parametrize('wt_cls', [PyWakeWindTurbines, HAWC2WindTurbines, HAWC2WindTurbinesDummy][:1])
def test_plot(wt_cls):

    for axes, wd, yaw in zip(plt.subplots(2, 4, figsize=(18, 12))[1], [270, 240], [(30, 0), (0, -30)]):

        if wt_cls is PyWakeWindTurbines:
            wts = PyWakeWindTurbines(x=[0, 0], y=[-100, 100], windTurbine=[V80(), DTU10MW()], types=[0, 1])
            site = 'uniform'
            T = 50
            wts.yaw = yaw
        else:
            htc_lst = []
            htc = HTCFile(h2_tfp + 'DTU_10_MW/htc/DTU_10MW_RWT.htc')
            for y in yaw:
                htc.new_htc_structure.orientation.relative.body2_eulerang.values[2] = -y
                htc.set_name(f'DTU_10MW_RWT_yaw{y}')
                htc.save()
                htc_lst.append(htc.filename)

            wts = wt_cls(x=[0, 0], y=[-100, 100],
                         htc_filename_lst=htc_lst,
                         types=np.arange(len(htc_lst)),
                         suppress_output=False)
            site = 'mann'
            T = .1

        fs = DefaultDWMFlowSimulation(windTurbines=wts, ti=0.06, d_particle=.1, n_particles=100, site=site,

                                      particleMotionModel=HillVortexParticleMotion())
        fs.wind_direction = wd
        if 0:
            plt.figure()
            fs.visualize(100, XYView(z=70, x=np.linspace(-100, 1500, 500), adaptive=False,
                                     y=np.linspace(-500, 200), visualizers=[ParticleVisualizer()], ax=plt.gca()))
        fs.run(T)
        # plt.close()
        # fs.visualize(500, XYView(z=70, x=np.linspace(-100, 1500, 500), adaptive=False,
        #              y=np.linspace(-500, 200), visualizers=[ParticleVisualizer()]))

        def kwargs():
            return dict(x=np.linspace(-100, 500), y=np.linspace(-200, 200), z=np.linspace(0, 250),
                        flowVisualizer=Flow2DVisualizer(color_bar=False),
                        visualizers=[ParticleVisualizer()])

        t = f' wd:{wd}'
        fs.show(MultiView([
            EastNorthView(**{**kwargs(), 'z': 70}, ax=axes[0], title='EastNorth' + t),
            XYView(**{**kwargs(), 'z': 70}, ax=axes[1], title='XY' + t),
            XZView(**{**kwargs(), 'y': 0}, ax=axes[2], title='XZ' + t),
            YZView(**{**kwargs(), 'x': 200, 'y': np.linspace(200, -200)}, ax=axes[3], title='YZ' + t)
        ]), block=False)
    if 0:
        plt.show()


@pytest.mark.parametrize('wt_cls', [PyWakeWindTurbines, HAWC2WindTurbines, HAWC2WindTurbinesDummy][:1])
@pytest.mark.parametrize('rotorAvgModel,ref', [(CGIRotorAvg(7), [[7, 0, 0]]),
                                               (GridRotorAvg(nodes_x=[-.5, 0, .5], nodes_y=[0, 0, 0]), [[(8 + 8 + 4) / 3, 0, 0]])])
def test_rotor_average_windspeed(wt_cls, rotorAvgModel, ref):
    if wt_cls == PyWakeWindTurbines:
        wts = PyWakeWindTurbines(x=[0], y=[0], windTurbine=[V80()], rotorAvgModel=rotorAvgModel)
    else:
        htc_lst = [h2_tfp + 'DTU_10_MW/htc/DTU_10MW_RWT.htc']
        wts = wt_cls(x=[0], y=[0],
                     htc_filename_lst=htc_lst,
                     suppress_output=True)

    def mean_ws(xyz, uvw, time=None):
        # 4m/s from 0 to 5m from rotor center, 8m/s elsewhere
        uvw[0] += 4. + 4 * ((xyz[1]**2 + (xyz[2] - wts.hub_height(0))**2) > 25)
        return uvw
    site = UniformSite(ws=8., ti=.1)
    site.add_mean_windspeed = mean_ws
    fs = DefaultDWMFlowSimulation(windTurbines=wts, ws=8,
                                  dt=1, d_particle=.5, n_particles=8,
                                  site=site)
    fs.step()
    npt.assert_array_equal(fs.windTurbines.rotor_avg_windspeed, ref)
