import os
from pathlib import Path

from h2lib_tests.test_files import tfp as h2lib_tfp
from numpy import newaxis as na
import pytest
from wetb.hawc2.at_time_file import AtTimeFile
from wetb.hawc2.htc_file import HTCFile

from dynamiks.dwm.added_turbulence_models import SynchronizedAutoScalingIsotropicMannTurbulence
from dynamiks.dwm.dwm_flow_simulation import DWMFlowSimulation
from dynamiks.dwm.particle_deficit_profiles.pywake_deficit_wrapper import PyWakeDeficitGenerator
from dynamiks.dwm.particle_motion_models import ParticleMotionModel
from dynamiks.flow_simulation import FlowSimulation
from dynamiks.sites._site import UniformSite, TurbulenceFieldSite
from dynamiks.sites.turbulence_fields import MannTurbulenceField
from dynamiks.utils.test_utils import npt, tfp, DefaultDWMFlowSimulation, EllipsysMockFlowSimulation, DemoSite
from dynamiks.views import XZView, XYView, EastNorthView, YZView, MultiView, YView
from dynamiks.visualizers.flow_visualizers import Flow2DVisualizer
from dynamiks.wind_turbines.hawc2_windturbine import HAWC2WindTurbines, AeroSectionCoupling
from dynamiks.wind_turbines.hawc2_windturbine import hawc2gl_to_uvw, HAWC2WindTurbinesDummy
import matplotlib.pyplot as plt
from multiclass_interface import mpi_interface
import numpy as np
from py_wake.deficit_models.no_wake import NoWakeDeficit
from tests.test_windturbines.test_pywake_windturbines import get_process_id, get_var


def test_gl2uvw():
    xyz_wt = np.array([[100, -100],
                       [50, 500],
                       [-118, -150.]])

    npt.assert_array_equal(hawc2gl_to_uvw(xyz_wt), [[50., 500.],
                                                    [100., -100.],
                                                    [118., 150.]])

    npt.assert_array_equal(hawc2gl_to_uvw(xyz_wt.T, 1), [[50., 500.],
                                                         [100., -100.],
                                                         [118., 150.]])

    npt.assert_array_equal(hawc2gl_to_uvw([100, 50, -118]), [50, 100, 118])


def test_windturbine():
    htc = HTCFile(h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT.htc")
    htc.simulation.visualization = 'visu.hdf5', 0, 0.01
    visualization_path = Path(h2lib_tfp + "DTU_10_MW/visualization/tmp_wt0.hdf5")
    visualization_path.unlink(missing_ok=True)

    htc.save(h2lib_tfp + "DTU_10_MW/htc/tmp.htc")

    htc_lst = [h2lib_tfp + "DTU_10_MW/htc/tmp.htc"]
    with pytest.warns(DeprecationWarning, match='htc_filename_lst argument is deprecated'):
        wt = HAWC2WindTurbines(x=[50], y=[100], htc_filename_lst=htc_lst, types=0, suppress_output=True)
    assert wt.htc_lst[0].output.buffer[0] == 10000
    assert wt.htc_lst[0].simulation.log_deltat[0] == 10000
    npt.assert_array_almost_equal(wt.rotor_positions_east_north.squeeze(), [42.927018, 100, 118.99880577])

    npt.assert_array_almost_equal(wt.diameter() / 2, 88.95765294)
    npt.assert_array_almost_equal(wt.hub_height(), 118.99880577)

    fs = DefaultDWMFlowSimulation(windTurbines=wt, dt=0.01)
    r = np.array([0, .25, .5, .75, 1])
    assert wt[0].axisymetric_induction(r).shape == (5, 1)
    assert wt.axisymetric_induction(r).shape == (5, 1)
    assert wt[:1].axisymetric_induction(r).shape == (5, 1)
    fs.step()
    npt.assert_array_almost_equal(wt.axisymetric_induction([0, .5, 1])[:, 0],
                                  [1.42546e-01, 3.66765e-02, 6.78727e-12], 4)

    assert visualization_path.exists()
    wt.h2.close()
    visualization_path.unlink(missing_ok=True)


def test_windturbines():
    htc_lst = [h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT.htc",
               h2lib_tfp + "IEA-15-240-RWT-Onshore/htc/IEA_15MW_RWT_Onshore.htc"]
    wt = HAWC2WindTurbines(x=[50, 500, 1000], y=[100, 0, -100], htc_lst=htc_lst, types=[0, 1, 0],
                           suppress_output=True)
    npt.assert_array_almost_equal(wt.rotor_positions_east_north, [[42.927018, 487.9687, 992.927018],
                                                                  [100., 0., -100.],
                                                                  [118.998806, 150., 118.998806]])
    npt.assert_array_almost_equal(wt[0].rotor_positions_east_north.squeeze(), [42.927018, 100, 118.99880577])

    npt.assert_array_almost_equal(wt.diameter() / 2, [88.95765294, 120.40297962, 88.95765294])
    npt.assert_array_almost_equal(wt[1].diameter() / 2, 120.40297962)
    npt.assert_array_almost_equal(wt.hub_height(), [118.99880577, 150., 118.99880577])
    npt.assert_array_almost_equal(wt[2].hub_height(), 118.99880577)

    fs = DefaultDWMFlowSimulation(windTurbines=wt, dt=0.01)
    r = np.array([0, .25, .5, .75, 1])
    assert wt[0].axisymetric_induction(r).shape == (5, 1)
    fs.step()
    npt.assert_array_almost_equal(wt[0].axisymetric_induction([0, .5, 1])[:, 0],
                                  [1.42546e-01, 3.66765e-02, 6.78727e-12], 4)

    dist_wt = wt.dist_wt
    dist_wt.get_process_id = get_process_id
    dist_wt.test = [f"hej{i}" for i in range(3)]
    dist_wt.get_var = get_var
    assert len(np.unique(dist_wt.get_process_id())) == 3
    assert isinstance(dist_wt[0].get_process_id()[0], int)
    npt.assert_array_equal(dist_wt.get_var('test'), ['hej0', 'hej1', 'hej2'])
    wt.h2.close()


@pytest.mark.parametrize('offset', [-500, -2500, -10000])
def test_set_windfield_1wt(offset):
    ws = 20
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/Hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_9.600x9.60x9.60_s0001.nc")
    turbfield.uvw[0] = np.arange(1024)[:, na, na]
    turbfield.uvw[1] = np.arange(128)[na, :, na]
    turbfield.uvw[2] = np.arange(32)[na, na, :]
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(offset, -100, 20))

    wt = HAWC2WindTurbines(x=[0], y=[50], htc_lst=[h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT.htc"],
                           types=[0], suppress_output=True)

    with pytest.raises(ValueError, match='Space not allowed in sensor name'):
        wt.add_sensor('aero thrust', 'aero thrust')

    wt.add_sensor('rotor_speed', 'constraint bearing1 shaft_rot 2')
    wt.add_sensor('yaw', lambda wt: np.rad2deg(wt.yaw_tilt()[0]),
                  lambda wt, value: wt.h2.set_variable_sensor_value(1, np.deg2rad(value)))

    fs = DWMFlowSimulation(site, windTurbines=wt, dt=.2,
                           d_particle=2, n_particles=100,
                           particleDeficitGenerator=PyWakeDeficitGenerator(deficitModel=NoWakeDeficit()),
                           particleMotionModel=ParticleMotionModel(temporal_filter=0.1))
    wt.coupling.windfield_update_interval = 0.4

    for _ in range(5):
        fs.step()
        npt.assert_array_almost_equal(fs.get_windspeed([0, 50, 100], include_wakes=False)[:, 0],
                                      wt.h2.get_uvw([0, 0, -100])[0])
        npt.assert_array_almost_equal(fs.get_windspeed([0, 50, 100], include_wakes=False),
                                      wt.get_windspeed(0, 50, 100))
    npt.assert_array_equal(wt.sensors.to_xarray().sensor[2:4], ['rotor_speed_0', 'rotor_speed_1'])
    wt.h2.close()


def test_aerosection_windturbine():
    from h2lib_tests.test_files import tfp as h2lib_tfp

    wt = HAWC2WindTurbines(x=[0], y=[50], htc_lst=[h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT_no_aerodrag.htc"],
                           types=[0], suppress_output=1)

    fs = DefaultDWMFlowSimulation(windTurbines=wt, dt=.5,
                                  wakeDeficitModel=PyWakeDeficitGenerator(deficitModel=NoWakeDeficit()))

    rotor_pos_xi, sec_rel_pos_xibs = wt.get_aerosection_positions()
    pos_before = rotor_pos_xi[:, :, na, na] + sec_rel_pos_xibs
    uvw_uibs = np.zeros_like(pos_before)
    uvw_uibs[0] = 6
    wt.set_aerosection_velocities(uvw_uibs)

    x, y, z = np.linspace(-200, 200), np.linspace(-200, 200), np.linspace(0, 200)
    fs.wind_direction = 300
    fs.windTurbines.yaw = 10
    fs.windTurbines.tilt = 5

    for view in [XYView(z=70, x=x, y=y, adaptive=False),
                 EastNorthView(z=70, x=x, y=y, adaptive=False),
                 XZView(y=0, x=x, z=z, adaptive=False),
                 YZView(x=0, y=y, z=z, adaptive=False),
                 # Points, Grid(), GridSlice3D, View2D', 'View1D', 'XView', 'YView', 'ZView',
                 ]:

        fs.show(view=view, block=False)
        view.ax.set_title(view.__class__.__name__)
        if 0:
            plt.show()

    fs.run(5, verbose=1)

    rotor_pos_xi, sec_rel_pos_xibs = wt.get_aerosection_positions()
    pos_before = rotor_pos_xi[:, :, na, na] + sec_rel_pos_xibs
    f_xibs_before = wt.get_aerosection_forces()
    uvw_uibs[0] = 8
    wt.set_aerosection_velocities(uvw_uibs)
    wt.h2.step()
    rotor_pos_xi, sec_rel_pos_xibs = wt.get_aerosection_positions()
    pos_after = rotor_pos_xi[:, :, na, na] + sec_rel_pos_xibs
    f_xibs_after = wt.get_aerosection_forces()

    if 0:
        ax = plt.figure().add_subplot(projection='3d')
        for i in range(3):
            c = ax.plot(*pos_before[:, 0, i], label=f'blade before {i}')[0].get_color()
            ax.quiver(*pos_before[:, 0, i], *f_xibs_before[:, 0, i] / 100, color=c)
            c = ax.plot(*pos_after[:, 0, i], label=f'blade after{i}')[0].get_color()
            ax.quiver(*pos_after[:, 0, i], *f_xibs_after[:, 0, i] / 100, color=c)
        ax.set_xlim3d([-50, 50])
        plt.legend()
        plt.show()

    # total force at all sections except tip section is increased
    np.all((np.sum(f_xibs_after**2, 0) > np.sum(f_xibs_before**2, 0))[:, :, :-1])
    wt.h2.close()


def test_set_windfield_3wt():
    ws = 20
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/Hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_9.600x9.60x9.60_s0001.nc")
    turbfield.uvw[0] = np.arange(1024)[:, na, na]
    turbfield.uvw[1] = np.arange(128)[na, :, na]
    turbfield.uvw[2] = np.arange(32)[na, na, :]
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 10))
    htc = HTCFile(h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT.htc")
    oat = htc.add_section('output_at_time aero 0.6', {'filename': './output_at_time'})
    oat.add_sensor('windspeed', "", [1, 1, 3])
    oat.add_sensor('windspeed', "", [1, 2, 3])
    oat.add_sensor('windspeed', "", [1, 3, 3])
    htc.save(h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT_output_at_time.htc")
    htc_lst = [htc,
               h2lib_tfp + "IEA-15-240-RWT-Onshore/htc/IEA_15MW_RWT_Onshore_simple.htc"]
    wt = HAWC2WindTurbines(x=[-2500, 500, 7330.4], y=[50, 100, 150], htc_lst=htc_lst,
                           types=[0, 1, 0], suppress_output=1)
    assert ([os.path.basename(htc.filename) for htc in wt.htc_lst] ==
            ['DTU_10MW_RWT_output_at_time_wt0.htc',
             'IEA_15MW_RWT_Onshore_simple_wt1.htc',
             'DTU_10MW_RWT_output_at_time_wt2.htc'])
    fs = DWMFlowSimulation(site, windTurbines=wt, dt=.2, d_particle=2, n_particles=100,
                           particleDeficitGenerator=PyWakeDeficitGenerator(deficitModel=NoWakeDeficit()),
                           particleMotionModel=ParticleMotionModel(temporal_filter=0.1))

    fs.windTurbines.coupling.windfield_update_interval = 0.4
    wt.add_sensor('rotor', 'constraint bearing1 shaft_rot 2', ext_lst=['angle', 'speed'])
    wt.add_sensor('yaw', 'general variable 1 0', 1)

    if os.path.isfile(h2lib_tfp + "DTU_10_MW/output_at_time_wt0.dat"):
        os.remove(h2lib_tfp + "DTU_10_MW/output_at_time_wt0.dat")
    for i in range(5):
        wt.sensors.yaw = 10 + i
        fs.step()
        xyz = wt.rotor_positions_east_north
        npt.assert_array_almost_equal(fs.get_windspeed(xyz, include_wakes=False),
                                      wt.get_windspeed(*xyz))

    npt.assert_array_almost_equal(np.deg2rad(wt.yaw_tilt()),
                                  [[0.000480616303979246, 3.320634660331569e-05, 0.000480616303979246],
                                   [0.086104662375323, 0.09662980122800646, 0.086104662375323]])

    da = wt.sensors.to_xarray()
    assert da.shape == (5, 3, len(da.sensor))  # 5 timestep, 3 wt
    npt.assert_array_equal(da.sel(sensor='yaw', wt=0), [10, 11, 12, 13, 14])

    for i, gx, gy, gz in [(0, 14.989978262899836, 1042.1529387500973, -17.86137005992684),
                          (2, 25.406678262899838, 1042.1529387500973, -17.86137005992684)]:
        # gx same due to repeated turbulence
        # gz same due to same height
        atf = AtTimeFile(h2lib_tfp + f"DTU_10_MW/output_at_time_wt{i}.dat")
        assert atf.attribute_names == ['radius_s', 'wsp_gx', 'wsp_gy', 'wsp_gz']
        npt.assert_allclose(atf.wsp_gx(60, curved_length=True), gx)
        npt.assert_allclose(atf.wsp_gy(60, curved_length=True), gy)
        npt.assert_allclose(atf.wsp_gz(60, curved_length=True), gz)

    if 0:
        axes = plt.subplots(3, 1)[1]
        fs.show(MultiView([XYView(y=np.linspace(-400, 600), flowVisualizer=Flow2DVisualizer(uvw='u'), ax=axes[0]),
                           XYView(y=np.linspace(-400, 600), flowVisualizer=Flow2DVisualizer(uvw='v'), ax=axes[1]),
                           XZView(y=0, flowVisualizer=Flow2DVisualizer(uvw='w'), ax=axes[2])]), block=False)
        fs.wind_direction = 275

        axes = plt.subplots(3, 1)[1]
        fs.show(MultiView([XYView(y=np.linspace(-400, 600), flowVisualizer=Flow2DVisualizer(uvw='u'), ax=axes[0]),
                           XYView(y=np.linspace(-400, 600), flowVisualizer=Flow2DVisualizer(uvw='v'), ax=axes[1]),
                           XZView(y=0, flowVisualizer=Flow2DVisualizer(uvw='w'), ax=axes[2])]))
    else:
        fs.wind_direction = 275
    fs.step()
    xyz = wt.rotor_positions_xyz
    npt.assert_array_almost_equal(fs.get_windspeed(xyz, include_wakes=False),
                                  wt.get_windspeed(*xyz))
    fs.site.turbulence_transport_speed = 10
    fs.step()
    xyz = wt.rotor_positions_xyz
    npt.assert_array_almost_equal(fs.get_windspeed(xyz, include_wakes=False),
                                  wt.get_windspeed(*xyz))
    wt.h2.close()


def test_HAWC2WindTurbinesDummy():
    x, y = [0, 500], [0, 0]
    ws = 10
    ti = 0.1
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_3.200x3.20x3.20_s0001.nc")
    turbfield.scale_TI(TI=ti, U=ws)
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))

    from h2lib_tests import tfp as h2_tfp
    fn = [h2_tfp + 'DTU_10_MW/htc/DTU_10MW_RWT.htc']
    windTurbines = HAWC2WindTurbinesDummy(x=x, y=y, htc_lst=fn, types=[0],
                                          suppress_output=True)

    fs = DefaultDWMFlowSimulation(x, y, ws=ws, ti=ti, site=site, windTurbines=windTurbines)
    fs.run(1)

    windTurbines.h2.close()


def test_hawc2_windturbine_distributed_mpi():
    mpi_interface.COLLECTIVE_MPI = False
    mpi_interface.mpi = True
    mpi_interface.size = 2
    wt = HAWC2WindTurbines(x=[0], y=[50], htc_lst=[h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT.htc"],
                           types=[0], suppress_output=True)

    wt.h2.step()

    class DummyCoupling():
        def step(self, fs):
            pass

    mpi_interface.rank = 1
    wt = HAWC2WindTurbines(x=[0], y=[50], htc_lst=[h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT.htc"],
                           types=[0], suppress_output=True)
    wt.coupling = DummyCoupling()
    wt.step(None)

    wt.h2.close()
    mpi_interface.mpi = False
    mpi_interface.rank = 0
    mpi_interface.size = 1


def test_raise_nan_error():
    x, y = [0, 500], [0, 0]
    ws = 10
    ti = 0.1
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_3.200x3.20x3.20_s0001.nc")

    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))

    from h2lib_tests import tfp as h2_tfp
    fn = [h2_tfp + 'DTU_10_MW/htc/DTU_10MW_RWT.htc']
    windTurbines = HAWC2WindTurbinesDummy(x=x, y=y, htc_lst=fn, types=[0],
                                          suppress_output=True)

    class NAN_AddedTurbulence(SynchronizedAutoScalingIsotropicMannTurbulence):
        def scale(self, target_std=1):
            self.mannTurbulenceField.uvw[:] = np.nan

    fs = DefaultDWMFlowSimulation(x, y, ws=ws, ti=ti, site=site, windTurbines=windTurbines,
                                  addedTurbulenceModel=NAN_AddedTurbulence())
    with pytest.raises(AssertionError, match="uvw passed to h2lib.set_windfield contains nan"):
        fs.run(1)


def test_AeroSectionCoupling():

    htc = HTCFile(h2lib_tfp + "DTU_10_MW/htc/DTU_10MW_RWT_output_at_time.htc")
    htc.aerodrag.delete()
    wts = HAWC2WindTurbines(x=[0], y=[0], htc_lst=[htc], suppress_output=1, coupling=AeroSectionCoupling())
    ws, ti = 10, .1
    turbfield = MannTurbulenceField.from_netcdf(
        tfp + "mann_turb/Hipersim_mann_l29.4_ae1.0000_g3.9_h0_1024x128x32_9.600x9.60x9.60_s0001.nc")
    turbfield.scale_TI(TI=ti, U=ws)
    site = TurbulenceFieldSite(ws=ws, turbulenceField=turbfield, turbulence_offset=(-2500, -100, 20))
    fs = EllipsysMockFlowSimulation(site=site, windTurbines=wts, dt=.01)

    if 0:
        axes = plt.subplots(2, 1)[1]
        fs.visualize(10, MultiView([
            XYView(x=np.arange(-300, 1000, 5), y=np.arange(-300, 300, 5), ax=axes[0]),
            YZView(x=20, y=np.arange(-300, 300, 5), z=np.arange(0, 290, 5), ax=axes[1])]), dt=1)
    else:
        fs.run(10)

    y_lst = np.linspace(-100, 100, 20)
    da = fs.get_windspeed(YView(x=20, y=y_lst, z=125), xarray=True)
    # print(np.round(da.sel(uvw='u').values, 3).tolist())
    ref = [9.814, 8.059, 6.775, 6.791, 6.624, 6.711, 7.824, 7.828, 8.459, 9.081, 8.921, 8.595, 8.469, 8.529,
           8.505, 9.416, 10.155, 10.489, 11.291, 11.682, 10.036]

    if 0:
        plt.plot(da.y, ref, label='ref')
        da.sel(uvw='u').plot(label='actual')
        plt.legend()
        plt.show()
    npt.assert_array_almost_equal(ref, da.sel(uvw='u'), 3)
