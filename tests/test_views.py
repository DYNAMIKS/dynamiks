from dynamiks.utils.test_utils import DefaultDWMFlowSimulation, tfp, npt
from dynamiks.views import XYView, XZView, YZView, EastNorthView, Points, XView, YView, ZView, XYZView, MultiView, View
import matplotlib.pyplot as plt
import numpy as np
from io import BytesIO
import pytest
from dynamiks.visualizers.flow_visualizers import Flow2DVisualizer
from dynamiks.visualizers._visualizers import Visualizer
from pathlib import Path
from dynamiks.utils.data_dumper import DataDumper


def test_Points():
    fs = DefaultDWMFlowSimulation(x=[25], y=[0], site='uniform', d_particle=.2)
    fs.run(100)
    view = Points(y=[0] * 10, z=[70] * 10, x=np.linspace(0, 200, 10))
    ref = [10.0, 10.0, 0.0, 0.0, 0.05, 0.58, 1.7, 2.63, 3.4, 4.05]

    # print(np.round(fs.get_windspeed(view, include_wakes=True)[0], 2).tolist())
    npt.assert_array_almost_equal(fs.get_windspeed(view, include_wakes=True, xarray=True).sel(uvw='u'), ref, 2)
    fs.show(view, block=False)


@pytest.mark.parametrize("view,ref", [
    (XView(y=0, z=70, x=np.linspace(0, 200, 10)), [10.0, 10.0, 0.0, 0.0, 0.05, 0.58, 1.7, 2.63, 3.4, 4.05]),
    (YView(x=100, z=70, y=np.linspace(-100, 100, 10)),
     [10.0, 9.96, 9.42, 6.45, 1.24, 1.24, 6.45, 9.42, 9.96, 10.0]),
    (ZView(x=100, y=0, z=np.linspace(0, 150, 10)), [9.89, 9.27, 7.13, 3.2, 0.29, 1.66, 5.7, 8.66, 9.75, 9.97])
])
def test_1d_views(view, ref):
    fs = DefaultDWMFlowSimulation(x=[25], y=[0], site='uniform', d_particle=.2)
    fs.run(100)

    fs.show(view, block=False)
    if 0:
        print(np.round(fs.get_windspeed(view, include_wakes=True)[0], 2).tolist())
        plt.show()
    npt.assert_array_almost_equal(fs.get_windspeed(view, include_wakes=True, xarray=True).sel(uvw='u'), ref, 2)


def test_initialize_visualizers():
    fs = DefaultDWMFlowSimulation(x=[25], y=[0])
    fs.add_step_handlers(XYView(z=70))
    fs.step()
    # plt.show()


def test_2d_views():

    for wd in [270, 300, 0]:
        x_lst = np.linspace(-200, 500, 20)
        y_lst = np.linspace(-200, 500, 20)
        z_lst = np.linspace(0, 200, 20)
        view_lst = [XYView(z=70, x=x_lst, y=y_lst),
                    XZView(y=0, x=x_lst, z=z_lst),
                    YZView(x=100, y=y_lst, z=z_lst),
                    EastNorthView(z=70, x=x_lst, y=y_lst)]
        fs = DefaultDWMFlowSimulation(x=[200, 0], y=[0, 100], wind_direction=wd, site='uniform')
        fs.run(10)

        axes = plt.subplots(2, 2)[1].flatten()
        for view, ax in zip(view_lst, axes):
            view.ax = ax
            fs.visualize(fs.time + 1, dt=1, view=view)
            ax.set_title(view.__class__.__name__)

        plt.suptitle(f'{wd}deg')

        if 0:
            # check result (installation dependent, fails on test machine)
            # save ref fig
            # plt.savefig(tfp + f"ref_figs/test_views_{wd}deg.png")
            bio = BytesIO()
            plt.savefig(bio, format='png')
            bio.seek(0)
            img = plt.imread(bio)

            ref = plt.imread(tfp + f"ref_figs/test_views_{wd}deg.png")
            try:
                npt.assert_array_equal(img, ref)

            except BaseException:
                plt.figure()
                plt.imshow(img)
                plt.title('img')
                plt.figure()
                plt.imshow(ref)
                plt.title('ref')
                plt.figure()
                plt.imshow((ref - img)[:, :, :3])
                plt.title('diff')
                # plt.show()
                raise

    if 0:
        plt.show()
    plt.close('all')


def test_3d_views():
    fs = DefaultDWMFlowSimulation(x=[25], y=[0], site='uniform', d_particle=.2)
    fs.run(100)

    view = XYZView(x=np.linspace(0, 200, 10), y=[0, 50], z=[50, 70, 100])
    ref = [10.0, 10.0, 0.0, 0.0, 0.05, 0.58, 1.7, 2.63, 3.4, 4.05]

    npt.assert_array_almost_equal(fs.get_windspeed(view, include_wakes=True, xarray=True).sel(uvw='u', y=0, z=70),
                                  ref, 2)


def test_views_coords():

    for wd in [270, 300, 0]:
        x_lst = np.linspace(-200, 500, 20)
        y_lst = np.linspace(-200, 500, 20)
        z_lst = np.linspace(0, 200, 20)
        view_lst = [XYView(z=70, x=x_lst, y=y_lst),
                    XZView(y=0, x=x_lst, z=z_lst),
                    YZView(x=100, y=y_lst, z=z_lst),
                    EastNorthView(z=70, x=x_lst, y=y_lst)]
        fs = DefaultDWMFlowSimulation(x=[200, 0], y=[0, 100], wind_direction=wd, site='uniform')
        fs.run(10)

        axes = plt.subplots(2, 2)[1].flatten()
        for view, ax in zip(view_lst, axes):

            da = fs.get_windspeed(view, include_wakes=True, xarray=True)
            ax.set_title(view.__class__.__name__)
            x, y = view.view_plane
            da.sel(uvw='u').plot(ax=ax, x=x, y=y)
        plt.suptitle(f'{wd}deg')

    if 0:
        plt.show()
    plt.close('all')


def test_MultiView():

    x, y = [0, 400], [0, 0]
    fs = DefaultDWMFlowSimulation(x=x, y=y)
    axes = plt.subplots(2, 2)[1].flatten()
    view1 = XYView(z=None, x=np.linspace(-200, 1000, 10), y=np.linspace(-200, 200, 10), ax=axes[0])
    view2 = XZView(y=0, x=np.linspace(-200, 1000, 10), z=np.linspace(0, 200, 10), ax=axes[1])
    fs.run(100)

    def my_draw(fs):
        power = fs.windTurbines.sensors.to_xarray(True).power
        for wt in power.wt.values:
            power.sel(wt=wt).plot(label=f'wt{wt}', ax=axes[2])
        axes[2].legend()

    class MyDraw(Visualizer):
        def __call__(self, fs):
            ws = fs.windTurbines.sensors.to_xarray(True).rotor_avg_windspeed_u
            for wt in ws.wt.values:
                ws.sel(wt=wt).plot(label=f'wt{wt}', ax=self.ax)
            self.ax.legend()

    multiView = MultiView([view1, view2,
                           View(visualizers=[my_draw], ax=axes[2]),
                           View(visualizers=[MyDraw()], ax=axes[3])])
    fs.show(view=multiView, block=False)
    fs.visualize(103, view=multiView)
    f = Path(tfp + f'tmp.gif')
    f.unlink(missing_ok=True)
    fs.animate(106, view=multiView, filename=f)
    assert f.exists()


def test_data_dumper():
    fs = DefaultDWMFlowSimulation()
    fs.visualize(10, view=fs.windTurbines.sensors.get_view())
