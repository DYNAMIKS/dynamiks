def test_version():
    from dynamiks import version
    assert version.__version__ == version.version
    assert version.__version_tuple__ == version.version_tuple
